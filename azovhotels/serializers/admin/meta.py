# -*- coding: utf-8 -*-

from rest_framework import serializers as s

import django_filters as df

from azovhotels.utils.base import ListSerializer, DetailSerializer, JSONField
import azovhotels.models as m



class MetaListSerializer(ListSerializer):
    description = JSONField(source='description', required=False)

    class Meta:
        model = m.Meta



class MetaDetailSerializer(DetailSerializer):  
    description = JSONField(source='description', required=False)

    class Meta:
        model = m.Meta

