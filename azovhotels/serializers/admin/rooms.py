# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .hotels import HotelListSerializer
from .room_types import RoomTypeListSerializer

from azovhotels.utils.base import DetailSerializer, ListSerializer

import azovhotels.models as m



class RoomListSerializer(ListSerializer):
    # hotel = s.RelatedField()
    # room_type = s.RelatedField()

    class Meta:
        model = m.Room
        exclude = ('hotel', 'room_type')

    # def get_hotel(self, obj):
    #     return model_to_dict(obj.hotel.values())


    # def get_room_type(self, obj):
    #     return model_to_dict(obj.room_types)


class RoomDetailSerializer(DetailSerializer):
    hotel = s.PrimaryKeyRelatedField(source='hotel', required=False)
    room_type = s.PrimaryKeyRelatedField(source='room_type', required=False)


    class Meta:
        model = m.Room








