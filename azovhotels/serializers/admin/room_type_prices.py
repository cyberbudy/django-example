# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .room_types import RoomTypeListSerializer
from .room_type_rates import RoomTypeRateListSerializer

from azovhotels.utils.base import DetailSerializer, ListSerializer

import azovhotels.models as m



class RoomTypePriceListSerializer(ListSerializer):
    room_type = s.Field(source='room_type_id')


    class Meta:
        model = m.RoomTypePrice
        # exclude = ('room_type', 'room_type_rates')

    # def get_room_type(self, obj):
    #     return model_to_dict(obj.room_type)


    # def get_rates(self, obj):
    #     return model_to_dict(obj.rates)


class RoomTypePriceDetailSerializer(DetailSerializer):
    # room_type = s.PrimaryKeyRelatedField(source='room_type')
    # rate = s.PrimaryKeyRelatedField(source='rate')
    price = s.DecimalField(source='price')
    available_rooms = s.IntegerField(source='available_rooms', required=True)
    date = s.DateField()

    class Meta:
        model = m.RoomTypePrice



    # def get_related_data(self, obj):
    #     return { 
    #         'room_type': RoomTypeListSerializer(obj.room_type).data,
    #         'room_type_rate': RoomTypeRateListSerializer(obj.rate).data
    #     }


    # def get_related_data_all(self, obj):
    #     return {
    #         # 'room_types': RoomTypeListSerializer(m.RoomType.objects.all(),
    #         #     many=True).data,  
    #         'room_type_rates': RoomTypeRateListSerializer(m.RoomTypeRate.objects.all(),
    #             many=True).data
    #     }