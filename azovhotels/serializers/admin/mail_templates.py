# -*- coding: utf-8 -*-

from django.contrib.auth.models import Group

from rest_framework import serializers as s

import azovhotels.models as m
from azovhotels.utils.base import ListSerializer, DetailSerializer, HTMLField
from azovhotels.serializers.admin import UserListSerializer, GroupListSerializer


class MailTemplateListSerializer(ListSerializer):
    class Meta:
        model = m.MailTemplate



class MailTemplateDetailSerializer(DetailSerializer):  
    class Meta:
        model = m.MailTemplate

    def get_related_data_all(self, obj):
        return {
            'groups': GroupListSerializer(Group.objects.all(),
                many=True).data,
            'users': UserListSerializer(m.User.objects.all(),
                many=True).data,
        }

