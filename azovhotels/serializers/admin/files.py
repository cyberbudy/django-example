# -*- coding: utf-8 -*-

from django.conf import settings
from rest_framework import serializers as s
from azovhotels.utils.base import ListSerializer, DetailSerializer


from azovhotels.models import File

def get_file_urls(value):
    urls = {}

    for alias in settings.IMAGE_SIZES:
        size = settings.IMAGE_SIZES[alias]
        url = value.file.url
        slug = value.slug

        urls[alias] = url.replace(slug,
            settings.IMAGE_SIZES_FORMAT.format(size[0], size[1], slug))

    return urls


class FileLinks(s.Field):
    def to_native(self, value):            
        return get_file_urls(value)


class FileListSerializer(ListSerializer):
    urls = FileLinks(source='*')
    url = s.CharField(source='file.url', read_only=True)
    
    class Meta:
        model = File



class FileDetailSerializer(DetailSerializer):
    urls = FileLinks(source='*')
    url = s.CharField(source='file.url', read_only=True)
    
    class Meta:
        model = File

