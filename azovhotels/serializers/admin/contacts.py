# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .hotels import HotelListSerializer

from azovhotels.utils.base import DetailSerializer, ListSerializer

import azovhotels.models as m



class ContactListSerializer(ListSerializer):
    hotel = s.Field('hotel_id')


    class Meta:
        model = m.Contact
        # exclude = ('hotel',)

    # def get_hotel(self, obj):
    #     return model_to_dict(obj.hotel)



class ContactDetailSerializer(DetailSerializer):
    hotel = s.PrimaryKeyRelatedField(source='hotel')


    class Meta:
        model = m.Contact

        
    def get_related_data(self, obj):
        return { 
            'hotel': HotelListSerializer(obj.hotel).data
        }


    def get_related_data_all(self, obj):
        return {
            'hotels': HotelListSerializer(m.Hotel.objects.all(), many=True).data
        }







