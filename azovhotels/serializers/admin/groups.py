# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.contrib.auth.models import Group, Permission

from .permissions import PermissionListSerializer

from azovhotels.utils.base import ListSerializer, DetailSerializer

import azovhotels.models as m



class GroupListSerializer(ListSerializer):
    # permissions = s.RelatedField(many=True)


    class Meta:
        model = Group
        exclude = ('permissions', )


class GroupDetailSerializer(DetailSerializer):
    permissions = s.PrimaryKeyRelatedField(source='permissions', many=True)


    class Meta:
        model = Group
        
    def get_related_data(self, obj):
        return { 
            'permissions': PermissionListSerializer(obj.permissions.all(), many=True).data,\
        }


    def get_related_data_all(self, obj):
        return {
            'permissions': PermissionListSerializer(Permission.objects.all(),
                many=True).data,\
        }







