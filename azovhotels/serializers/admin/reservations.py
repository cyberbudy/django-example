# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .hotels import HotelListSerializer
from .rooms import RoomTypeListSerializer
from .users import UserListSerializer

from azovhotels.utils.base import (DetailSerializer, ListSerializer,
    HTMLField, DateField)

import azovhotels.models as m


class DictListField(s.Field):
    def to_native(self, value):
        lst = []

        for i in value:
            lst.append(value[i])

        return lst


class ReservationListSerializer(ListSerializer):
    hotel = s.Field('hotel_id')
    room_type = s.Field('room_type_id')
    user = s.Field('user_id')
    manager = s.Field('manager_id')
    reservation_status_choices = s.Field(source="RESERVATION_STATUS_CHOICES")

    read_only_fields = ['reservation_status_choices']


    class Meta:
        model = m.Reservation
        # exclude = ('hotel', 'room_type', 'manager')
        depth = 1

    # def get_hotel(self, obj):
    #     return model_to_dict(obj.hotel)


    # def get_room_type(self, obj):
    #     return model_to_dict(obj.room_type)


    # def get_user(self, obj):
    #     return model_to_dict(obj.user)


    # def get_manager(self, obj):
    #     return model_to_dict(obj.manager)



class ReservationDetailSerializer(DetailSerializer):
    hotel = s.PrimaryKeyRelatedField(source='hotel')
    room_type = s.PrimaryKeyRelatedField(source='room_type')
    user = s.PrimaryKeyRelatedField(source='user', required=False)
    manager = s.PrimaryKeyRelatedField(source='manager', required=False)
    reservation_status_choices = DictListField(source="RESERVATION_STATUS_CHOICES")
    starting_at = DateField(source='starting_at', required=False)
    ending_at = DateField(source='ending_at', required=False)

    read_only_fields = ['reservation_status_choices']

    class Meta:
        model = m.Reservation

    def get_related_data(self, obj):
        return { 
            'hotel': HotelListSerializer(obj.hotel).data,
            'room_type': RoomTypeListSerializer(obj.room_type).data,
            'user': UserListSerializer(obj.user).data,

        }


    def get_related_data_all(self, obj):
        return {
            'hotel': HotelListSerializer(m.Hotel.objects.all(),
                many=True).data,
            'users': UserListSerializer(m.User.objects.all(),
                many=True).data,
        }







