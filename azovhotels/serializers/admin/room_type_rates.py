# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .room_types import RoomTypeListSerializer

from azovhotels.utils.base import DetailSerializer, ListSerializer

import azovhotels.models as m



class RoomTypeRateListSerializer(ListSerializer):
    room_type = s.Field(source='room_type_id')


    class Meta:
        model = m.RoomTypeRate

    # def get_room_type(self, obj):
    #     return model_to_dict(obj.room_type)


class RoomTypeRateDetailSerializer(DetailSerializer):
    room_type = s.PrimaryKeyRelatedField(source='room_type')
    price = s.DecimalField(required=True)

    class Meta:
        model = m.RoomTypeRate

    def get_related_data(self, obj):
        return { 
            'room_type': RoomTypeListSerializer(obj.room_type).data
        }


    def get_related_data_all(self, obj):
        return {
            'room_types': RoomTypeListSerializer(m.RoomType.objects.all(),
                many=True).data
        }







