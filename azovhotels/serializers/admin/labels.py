# -*- coding: utf-8 -*-

from rest_framework import serializers as s

from azovhotels.models import Label
from azovhotels.utils.base import ListSerializer, DetailSerializer, HTMLField


class LabelListSerializer(ListSerializer):
    description = HTMLField(source='description', required=False)

    class Meta:
        model = Label



class LabelDetailSerializer(DetailSerializer):  
    description = HTMLField(source='description', required=False)

    class Meta:
        model = Label

