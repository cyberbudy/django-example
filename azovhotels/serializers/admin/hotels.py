# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .labels import LabelListSerializer
from .cities import CityListSerializer
from .files import FileListSerializer, FileDetailSerializer
from .users import UserListSerializer
from .states import StateListSerializer

from azovhotels.utils.base import (DetailSerializer, ListSerializer,
    HTMLField, DateField)

import azovhotels.models as m



class HotelListSerializer(ListSerializer):
    city = s.Field('city_id')
    user = s.Field('user_id')
    view_file = s.Field('view_file_id')
    # labels = s.RelatedField(many=True)
    # files = s.RelatedField(many=True)

    about = HTMLField(source='about', required=False)


    class Meta:
        model = m.Hotel
        exclude = ('labels', 'files')


    # def get_city(self, obj):
    #     return model_to_dict(obj.city)


    # def get_user(self, obj):
    #     return model_to_dict(obj.user)


    # def get_labels(self, obj):
    #     return obj.labels.values()


    # def get_files(self, obj):
    #     return obj.files.values()


    # def get_file(self, obj):
    #     if obj.view_file:
    #         return model_to_dict(obj.view_file)
    #     else:
    #         return


    # def get_state(self, obj):
    #     if obj.state:
    #         return model_to_dict(obj.state)
    #     else:
    #         return 

        

class HotelDetailSerializer(DetailSerializer):
    city = s.PrimaryKeyRelatedField(source='city')
    user = s.PrimaryKeyRelatedField(source='user')
    labels = s.PrimaryKeyRelatedField(source='labels', many=True, required=False)
    files = s.PrimaryKeyRelatedField(source='files', many=True, required=False)
    view_file = s.PrimaryKeyRelatedField(source='view_file', required=False)
    working_from = DateField(source='working_from', required=False)
    working_till = DateField(source='working_till', required=False)

    about = HTMLField(source='about', required=False)


    class Meta:
        model = m.Hotel
        

    def get_related_data(self, obj):
        return { 
            'city': CityListSerializer(obj.city).data,\
            'labels': LabelListSerializer(obj.labels.all(), many=True).data,\
            'view_file': FileDetailSerializer(obj.view_file, many=False).data,\
            'files': FileListSerializer(obj.files.all(), many=True).data,\
            'user': UserListSerializer(obj.user).data,
        }


    def get_related_data_all(self, obj):
        return {
            'cities': CityListSerializer(m.City.objects.all(),
                many=True).data,\
            'types': LabelListSerializer(m.Label.objects\
                .filter(type='hotel_type').all(), many=True,
                ).data,\
            'conveniences': LabelListSerializer(m.Label.objects\
                .filter(type='hotel_convenience').all(), many=True,
                ).data,\
            'users': UserListSerializer(m.User.objects.all(),
                many=True).data,
        }







