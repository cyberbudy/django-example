# -*- coding: utf-8 -*-

from .users import UserListSerializer, UserDetailSerializer
from .actions import ActionListSerializer, ActionDetailSerializer
from .cities import CityListSerializer, CityDetailSerializer
from .comments import CommentListSerializer, CommentDetailSerializer
from .contacts import ContactListSerializer, ContactDetailSerializer
from .hotels import HotelListSerializer, HotelDetailSerializer
from .labels import LabelListSerializer, LabelDetailSerializer
from .pages import PageListSerializer, PageDetailSerializer
from .reservations import ReservationListSerializer, ReservationDetailSerializer
from .room_types import RoomTypeListSerializer, RoomTypeDetailSerializer
from .room_type_rates import RoomTypeRateListSerializer, RoomTypeRateDetailSerializer
from .room_type_prices import RoomTypePriceListSerializer, RoomTypePriceDetailSerializer
from .rooms import RoomListSerializer, RoomDetailSerializer
from .files import FileListSerializer, FileDetailSerializer
from .groups import GroupListSerializer, GroupDetailSerializer
from .permissions import PermissionListSerializer, PermissionDetailSerializer
from .states import StateListSerializer, StateDetailSerializer

from .meta import MetaListSerializer
from .mail_templates import MailTemplateListSerializer, MailTemplateDetailSerializer
# from .countries import CountrySerializer
# from .states import StateSerializer
# from .meta import MetaSerializer

