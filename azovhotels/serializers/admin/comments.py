# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .hotels import HotelListSerializer
from .users import UserListSerializer

from azovhotels.utils.base import (DetailSerializer,
    ListSerializer, HTMLField, JSONField)

import azovhotels.models as m



class CommentListSerializer(ListSerializer):
    hotel = s.Field('hotel_id')
    user = s.Field('user_id')
    
    content = HTMLField(source='content', required=False)
    vote_criteria = JSONField(source='vote_criteria', required=True)


    class Meta:
        model = m.Comment
        # exclude = ('hotel',)
        depth = 1

    # def get_hotel(self, obj):
    #     return model_to_dict(obj.hotel)


    # def get_user(self, obj):
    #     return model_to_dict(obj.user)
        


class CommentDetailSerializer(DetailSerializer):
    hotel = s.PrimaryKeyRelatedField(source='hotel')
    user = s.PrimaryKeyRelatedField(source='user')

    content = HTMLField(source='content', required=False)
    vote_criteria = JSONField(source='vote_criteria', required=True)


    class Meta:
        model = m.Comment

        
    def get_related_data(self, obj):
        return { 
            'hotel': HotelListSerializer(obj.hotel).data,
            'user': UserListSerializer(obj.user).data,
        }


    def get_related_data_all(self, obj):
        user_data = UserListSerializer(m.User.objects.all(),
            many=True).data

        return {
            'hotels': HotelListSerializer(m.Hotel.objects.all(),
                many=True).data,
            'users': user_data,
        }







