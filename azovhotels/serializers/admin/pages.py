# -*- coding: utf-8 -*-

from rest_framework import serializers as s

from .labels import LabelListSerializer

from azovhotels.utils.base import DetailSerializer, ListSerializer, HTMLField

import azovhotels.models as m



class PageListSerializer(ListSerializer):
    labels = s.PrimaryKeyRelatedField(many=True)
    
    content = HTMLField(source='content', required=False)


    class Meta:
        model = m.Page
        # exclude = ('labels',)

    # def get_labels(self, obj):
    #     return obj.labels.values()



class PageDetailSerializer(DetailSerializer):
    # labels = s.RelatedField(source='labels', many=True, required=False)

    content = HTMLField(source='content', required=False)


    class Meta:
        model = m.Page


    def get_related_data(self, obj):
        return { 
            'types': LabelListSerializer(obj.labels\
                .filter(type='page_type').all(), many=True).data,\
            'categories': LabelListSerializer(obj.labels\
                .filter(type='page_category').all(), many=True).data,\
        }


    def get_related_data_all(self, obj):
        return {
            'types': LabelListSerializer(m.Label.objects\
                .filter(type='page_type').all(), many=True,
                ).data,\
            'categories': LabelListSerializer(m.Label.objects\
                .filter(type='page_category').all(), many=True,
                ).data,\
        }






