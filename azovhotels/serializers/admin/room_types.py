# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .hotels import HotelListSerializer
from .labels import LabelListSerializer
from .files import FileListSerializer, FileDetailSerializer

from azovhotels.utils.base import DetailSerializer, ListSerializer

import azovhotels.models as m



class RoomTypeListSerializer(ListSerializer):
    hotel = s.Field('hotel_id')
    view_file = s.Field('view_file_id')

    class Meta:
        model = m.RoomType
        exclude = ('labels', 'files')




class RoomTypeDetailSerializer(DetailSerializer):
    hotel = s.PrimaryKeyRelatedField(source='hotel')
    labels = s.PrimaryKeyRelatedField(source='labels', required=False, many=True)
    files = s.PrimaryKeyRelatedField(source='files', required=False, many=True)
    view_file = s.PrimaryKeyRelatedField(source='view_file', required=False)

    class Meta:
        model = m.RoomType


    def get_related_data(self, obj):
        return { 
            'hotel': HotelListSerializer(obj.hotel).data,
            'labels': LabelListSerializer(obj.labels.all(), many=True).data,
            'files': FileListSerializer(obj.files.all(), many=True).data,
            'view_file': FileDetailSerializer(obj.view_file, many=False).data,
        }


    def get_related_data_all(self, obj):
        return {
            'hotels': HotelListSerializer(m.Hotel.objects.all(),
                many=True).data,
            'categories': LabelListSerializer(m.Label.objects\
                    .filter(type='roomtype_categories')
                    .all(),
                many=True).data,
        }