# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.forms.models import model_to_dict

from .hotels import HotelListSerializer
from .labels import LabelListSerializer

from azovhotels.utils.base import DetailSerializer, ListSerializer, HTMLField

import azovhotels.models as m



class ActionListSerializer(ListSerializer):
    hotel = s.Field('hotel_id')

    content = HTMLField(source='content', required=False)


    class Meta:
        model = m.Action
        exclude = ('labels')

    # def get_hotel(self, obj):
    #     return model_to_dict(obj.hotel)


    # def get_labels(self, obj):
    #     return obj.labels.values()


class ActionDetailSerializer(DetailSerializer):
    hotel = s.PrimaryKeyRelatedField(source='hotel')
    labels = s.PrimaryKeyRelatedField(source='labels', required=False, many=True)

    content = HTMLField(source='content', required=False)


    class Meta:
        model = m.Action

    def get_related_data(self, obj):
        return { 
            'hotel': HotelListSerializer(obj.hotel).data,
            'labels': LabelListSerializer(obj.labels.all()).data
        }


    def get_related_data_all(self, obj):
        return {
            'hotels': HotelListSerializer(m.Hotel.objects.all()
                ).data,
            'labels': LabelListSerializer(m.Label.objects.all()
                ).data
        }






