# -*- coding: utf-8 -*-

from rest_framework import serializers as s

from azovhotels.models import City, State
from .states import StateListSerializer
from azovhotels.utils.base import ListSerializer, DetailSerializer


class CityListSerializer(ListSerializer):

    class Meta:
        model = City
        depth = 1



class CityDetailSerializer(DetailSerializer):

    class Meta:
        model = City


    def get_related_data(self, obj):
        return {
            'state': StateListSerializer(obj.state).data
        }


    def get_related_data_all(self, obj):
        return {
            'states': StateListSerializer(State.objects.all()).data
        }