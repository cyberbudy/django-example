# -*- coding: utf-8 -*-

from rest_framework import serializers as s

from azovhotels.models import State
from azovhotels.utils.base import ListSerializer, DetailSerializer


class StateListSerializer(ListSerializer):

    class Meta:
        model = State


class StateDetailSerializer(DetailSerializer):

    class Meta:
        model = State