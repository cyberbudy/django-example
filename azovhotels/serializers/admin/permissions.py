# -*- coding: utf-8 -*-

from rest_framework import serializers as s

from django.contrib.auth.models import Permission

from azovhotels.utils.base import ListSerializer, DetailSerializer

import azovhotels.models as m



class PermissionListSerializer(ListSerializer):

    class Meta:
        model = Permission
        fields = ('id', 'name')


class PermissionDetailSerializer(DetailSerializer):

    class Meta:
        model = Permission






