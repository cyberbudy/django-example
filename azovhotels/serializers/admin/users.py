# -*- coding: utf-8 -*-

from rest_framework import serializers as s
from django.contrib.auth.models import Group, Permission

from azovhotels.models import User
from .groups import GroupListSerializer
from .permissions import PermissionListSerializer

from azovhotels.utils.base import DetailSerializer, ListSerializer

import azovhotels.models as m



class PasswordSerializer(s.Serializer):
    password = s.CharField()
    password_confirm = s.CharField()

    def validate(self, attrs):
        if attrs['password'] != attrs['password_confirm']:
            raise s.ValidationError("Passwords don't match.")
        return attrs



class UserListSerializer(ListSerializer):
    # groups = s.RelatedField()
    # user_permissions = s.RelatedField()

    email = s.CharField(source='email', required=True)
    password = s.CharField(source='password', required=False, write_only=True)

    first_name = s.CharField(source='first_name', required=False)
    middle_name = s.CharField(source='middle_name', required=False)
    last_name = s.CharField(source='last_name', required=False)

    phone = s.CharField(source='phone', required=False)

    passport_series = s.CharField(source='passport_series', required=False)
    passport_code = s.CharField(source='passport_code', required=False)
    passport_issued = s.CharField(source='passport_issued', required=False)

    send_sms = s.BooleanField(source='send_sms', required=False)
    send_email = s.BooleanField(source='send_email', required=False)
    is_active = s.BooleanField(source='is_active', default=True)

    last_login = s.DateTimeField(source='last_login', required=False)
    blocked_till = s.DateTimeField(source='blocked_till', required=False)
    updated_at = s.DateTimeField(source='updated_at', required=False)
    created_at = s.DateTimeField(source='created_at', required=False)


    class Meta:
        model = User
        exclude = ('password', 'permissions', 'user_permissions')



    def restore_object(self, attrs, instance=None):
        # call set_password on user object. Without this
        # the password will be stored in plain text.
        user = super(UserListSerializer, self).restore_object(attrs, instance)

        if 'password' in attrs:
            user.set_password(attrs['password'])

        return user



class UserDetailSerializer(DetailSerializer):
    groups = s.PrimaryKeyRelatedField(source='groups', many=True)
    user_permissions = s.PrimaryKeyRelatedField(source='user_permissions', many=True)

    email = s.CharField(source='email', required=True)
    password = s.CharField(source='password', required=False, write_only=True)

    first_name = s.CharField(source='first_name', required=False)
    middle_name = s.CharField(source='middle_name', required=False)
    last_name = s.CharField(source='last_name', required=False)

    phone = s.CharField(source='phone', required=False)

    passport_series = s.CharField(source='passport_series', required=False)
    passport_code = s.CharField(source='passport_code', required=False)
    passport_issued = s.CharField(source='passport_issued', required=False)

    send_sms = s.BooleanField(source='send_sms', required=False)
    send_email = s.BooleanField(source='send_email', required=False)
    is_active = s.BooleanField(source='is_active', default=True)

    last_login = s.DateTimeField(source='last_login', required=False)
    blocked_till = s.DateTimeField(source='blocked_till', required=False)
    updated_at = s.DateTimeField(source='updated_at', required=False)
    created_at = s.DateTimeField(source='created_at', required=False)


    class Meta:
        model = User
        
        # exclude = ('password',)

    def get_related_data(self, obj):

        return { 
            'groups': GroupListSerializer(obj.groups.all(), many=True).data,
            'permissions': PermissionListSerializer(obj.user_permissions.all(), many=True).data,
        }


    def get_related_data_all(self, obj):
        return {
            'groups': GroupListSerializer(Group.objects.all(), many=True).data,
            'permissions': PermissionListSerializer(Permission.objects.all(),
                many=True).data,\
        }


    def restore_object(self, attrs, instance=None):
        user = super(UserDetailSerializer, self).restore_object(attrs, instance)

        if('password' in attrs):
            user.set_password(attrs['password'])

        return user

    