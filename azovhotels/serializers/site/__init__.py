# -*- coding: utf-8 -*-

from .hotels import HotelListSerializer, HotelAtomSerializer
from .room_types import RoomTypeListSerializer
from .reservations import HotelListSerializer, RoomTypeListSerializer