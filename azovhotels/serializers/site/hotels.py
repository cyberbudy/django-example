#-*- coding: utf-8 -*-

from rest_framework import serializers as s, status

import azovhotels.models as m
import azovhotels.serializers.admin as ads


class HotelListSerializer(s.ModelSerializer):
    # city = s.Field('city_id')
    user = s.Field('user_id')

    related_data = s.SerializerMethodField('get_related_data')
    mark = s.Field('mark')
    min_price = s.Field('min_price')
    default_price = s.Field('default_price')
    # dates_price = s.Field('dates_price')


    class Meta:
        model = m.Hotel
        exclude = ('labels', 'files')
        # include = ('related_data', 'mark')


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
            'city': ads.CityListSerializer(obj.city).data
        }


class HotelAtomSerializer(s.ModelSerializer):
    city = s.Field('city_id')
    user = s.Field('user_id')

    related_data = s.SerializerMethodField('get_related_data')
    mark = s.Field('mark')
    min_price = s.Field('min_price')


    class Meta:
        model = m.Hotel
        exclude = ('labels',)
        include = ('related_data', 'mark')

    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
            'files': ads.FileListSerializer(obj.files.all(), many=True).data,
            'types': ads.LabelListSerializer(m.Label.objects\
                .filter(type='hotel_type').all(), many=True,
                ).data,\
            'conveniences': ads.LabelListSerializer(m.Label.objects\
                .filter(type='hotel_convenience').all(), many=True,
                ).data,\
            'city': ads.CityListSerializer(obj.city).data
        }
