#-*- coding: utf-8 -*-

from rest_framework import serializers as s, status

import azovhotels.models as m
import azovhotels.serializers.admin as ads


class HotelListSerializer(s.ModelSerializer):
    user = s.Field('user_id')
    mark = s.Field('mark')
    related_data = s.SerializerMethodField('get_related_data')


    class Meta:
        model = m.Hotel
        exclude = ('labels', 'files')


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
            'city': ads.CityListSerializer(obj.city).data
        }


class RoomTypeListSerializer(ads.RoomTypeListSerializer):
    related_data = s.SerializerMethodField('get_related_data')

    class Meta:
        model = m.RoomType
        exclude = ('labels', 'files')
    

    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
            'files': ads.FileListSerializer(obj.files.all(), many=True).data,
        }