# -*- coding: utf-8 -*-
from rest_framework import serializers as s, status

import azovhotels.models as m
import azovhotels.serializers.admin as ads


class RoomTypeListSerializer(ads.RoomTypeListSerializer):
    related_data = s.SerializerMethodField('get_related_data')
    dates_price = s.Field('dates_price')
    min_price = s.SerializerMethodField('get_sum')

    class Meta:
        model = m.RoomType
        exclude = ('labels', 'files')
    

    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
            'files': ads.FileListSerializer(obj.files.all(), many=True).data,
        }

    def get_sum(self, obj):
        if not obj.dates_price:
            return obj.def_price

        if not obj.def_price:
            return obj.dates_price

        return obj.dates_price+obj.def_price