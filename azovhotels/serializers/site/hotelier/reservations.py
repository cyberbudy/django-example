# -*- coding: utf-8 -*-

import azovhotels.serializers.admin as ads
import azovhotels.models as m

from rest_framework import serializers as s

class ReservationListSerializer(ads.ReservationListSerializer):
    related_data = s.SerializerMethodField('get_related_data')

    class Meta: 
        model = m.Reservation


    def get_related_data(self, obj):
        return {
            'user': ads.UserListSerializer(obj.user),
            'room_type': ads.UserListSerializer(obj.room_type)
        }