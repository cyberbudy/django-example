#-*- coding: utf-8 -*-

from rest_framework import serializers as s

import azovhotels.models as m
import azovhotels.serializers.admin as ads


class RoomTypeListSerializer(ads.RoomTypeListSerializer):

    class Meta:
        model = m.RoomType



class RoomTypeDetailSerializer(ads.RoomTypeDetailSerializer):
    class Meta:
        model = m.RoomType


    def get_related_data(self, obj):
        return { 
            # 'hotel': HotelListSerializer(obj.hotel).data,
            'labels': ads.LabelListSerializer(obj.labels.all(), many=True).data,
            'files': ads.FileListSerializer(obj.files.all(), many=True).data,
            'view_file': ads.FileDetailSerializer(obj.view_file, many=False).data,
        }


    def get_related_data_all(self, obj):
        return {
            # 'hotels': HotelListSerializer(m.Hotel.objects.all(),
            #     many=True).data,
            'categories': ads.LabelListSerializer(m.Label.objects\
                    .filter(type='roomtype_categories')
                    .all(),
                many=True).data,
        }
