# -*- coding: utf-8 -*-

from rest_framework import serializers as s

import azovhotels.models as m
import azovhotels.serializers.admin as ads


class HotelListSerializer(ads.HotelListSerializer):
    related_data = s.SerializerMethodField('get_related_data')

    class Meta:
        model = m.Hotel
        include = ('related_data',)


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
        }



class HotelDetailSerializer(ads.HotelDetailSerializer):
    room_types = ads.RoomTypeListSerializer(read_only=True, many=True)

    class Meta:
        model = m.Hotel
        read_only_fields = ('is_active', )


    def get_related_data(self, obj):
        return {
            # 'room_types': ads.RoomTypeListSerializer(m.RoomType.objects.filter(hotel=obj), many=True).data,
            'city': ads.CityListSerializer(obj.city).data,\
            'labels': ads.LabelListSerializer(obj.labels.all(), many=True).data,\
            'view_file': ads.FileDetailSerializer(obj.view_file).data,\
            'files': ads.FileListSerializer(obj.files.all(), many=True).data,\
        }


    def get_related_data_all(self, obj):
        return {
            'cities': ads.CityListSerializer(m.City.objects.all(),
                many=True).data,
            'types': ads.LabelListSerializer(m.Label.objects\
                .filter(type='hotel_type').all(),
                ).data,
            'conveniences': ads.LabelListSerializer(m.Label.objects\
                .filter(type='hotel_convenience').all(),
                ).data,
            }