# -*- coding: utf-8 -*-

from rest_framework import serializers as s

import azovhotels.models as m

import azovhotels.serializers.admin as ads


class ContactListSerializer(ads.ContactListSerializer):
    class Meta:
        model = m.Contact