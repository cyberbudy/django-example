# -*- coding: utf-8 -*-

from .hotels import HotelListSerializer, HotelDetailSerializer
from .contacts import ContactListSerializer
from .room_types import RoomTypeListSerializer, RoomTypeDetailSerializer
from .actions import ActionListSerializer, ActionDetailSerializer