#-*- coding: utf-8 -*-

from rest_framework import serializers as s

import azovhotels.models as m
import azovhotels.serializers.admin as ads


class ActionListSerializer(ads.ActionListSerializer):
    class Meta:
        model = m.Action



class ActionDetailSerializer(ads.ActionDetailSerializer):
    class Meta:
        model = m.Action


    def get_related_data(self, obj):
        return { 
            'labels': ads.LabelListSerializer(obj.labels.all()).data
        }


    def get_related_data_all(self, obj):
        return {
            'labels': ads.LabelListSerializer(m.Label.objects.all()
                ).data
        }

