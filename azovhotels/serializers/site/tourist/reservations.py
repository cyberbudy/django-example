# -*- coding: utf-8 -*-

from rest_framework import serializers as s

import azovhotels.serializers.admin as ads
import azovhotels.models as m


class CustomHotelSerializer(s.ModelSerializer):
    # view_file = ads.FileListSerializer(source='view_file', required=False)
    related_data = s.SerializerMethodField('get_related_data')

    class Meta:
        model = m.Hotel
        include = ('related_data', )


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
        }



class ReservationListSerializer(s.ModelSerializer):
    # hotel = CustomHotelSerializer(source='hotel', required=False)
    related_data = s.SerializerMethodField('get_related_data')

    class Meta:
        model = m.Reservation
        include = ('related_data', )


    def get_related_data(self, obj):
        return {
            'hotel': CustomHotelSerializer(obj.hotel).data
        }