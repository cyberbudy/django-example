# -*- coding: utf-8 -*-


from rest_framework import serializers as s
from azovhotels.models import User

import azovhotels.serializers.admin as ads



class UserProfileSerializer(ads.UserListSerializer):
    
    class Meta:
        model = User
        exclude = ('groups', 'permissions', 'user_permissions')




