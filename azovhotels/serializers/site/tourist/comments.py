# -*- coding: utf-8 -*-


from rest_framework import serializers as s

import azovhotels.models as m 
import azovhotels.serializers.admin as ads



class CustomHotelSerializer(s.ModelSerializer):
    related_data = s.SerializerMethodField('get_related_data')

    class Meta:
        model = m.Hotel
        include = ('related_data', )


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data
        }



class CommentListSerializer(ads.CommentListSerializer):
    related_data = s.SerializerMethodField('get_related_data')
    
    class Meta:
        model = m.Comment
        include = ('related_data', )


    def get_related_data(self, obj):

        return {
            'hotel': ads.HotelListSerializer(obj.hotel).data
        }


    def get_hotels(self, obj):
        reservations = m.Reservation.objects\
            .filter(user=obj.user, arrived=True)
        comments = m.Comment.objects\
            .filter(user=obj.user)

        # commented_hotels = [x['hotel_id'] for x in comments]
        reserved_hotels = [x['hotel_id'] for x in reservations]
        

        return reserved_hotels