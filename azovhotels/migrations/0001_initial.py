# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'State'
        db.create_table(u'azovhotels_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=150)),
        ))
        db.send_create_signal('azovhotels', ['State'])

        # Adding model 'City'
        db.create_table(u'azovhotels_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=150)),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.State'], null=True, blank=True)),
        ))
        db.send_create_signal('azovhotels', ['City'])

        # Adding model 'Label'
        db.create_table(u'azovhotels_label', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
            ('description', self.gf('django.db.models.fields.TextField')(default=None, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal('azovhotels', ['Label'])

        # Adding model 'File'
        db.create_table(u'azovhotels_file', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('deleted_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal('azovhotels', ['File'])

        # Adding model 'Hotel'
        db.create_table(u'azovhotels_hotel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=150)),
            ('about', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('about_short', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_location', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_driveway', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_guest_accommodation', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_tourist_tax', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_internet', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_feeding', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_parking', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_pets', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('conditions_medicine', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('gps_latitude', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('gps_longitude', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.NullBooleanField')(default=False, null=True, blank=True)),
            ('votes_weight', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('votes_count', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('promote_count', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('stars_category', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('rooms_count', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('postal_code', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('site', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_address', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_egrpou', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_payment_account', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_payment_bank_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_payment_bank_mfo', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_card_account', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_card_bank_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_card_bank_mfo', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('entity_corporate_inn', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('working_from', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 1, 1, 0, 0), blank=True)),
            ('working_till', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 12, 31, 0, 0), blank=True)),
            ('arrival_from', self.gf('django.db.models.fields.CharField')(default='12.00', max_length=10, blank=True)),
            ('arrival_till', self.gf('django.db.models.fields.CharField')(default='12.00', max_length=10, blank=True)),
            ('reservation_cancellation_days', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('reservation_cancellation_about', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('reservation_period', self.gf('django.db.models.fields.IntegerField')(default=31, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.User'])),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.City'])),
            ('view_file', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='view', null=True, to=orm['azovhotels.File'])),
        ))
        db.send_create_signal('azovhotels', ['Hotel'])

        # Adding M2M table for field labels on 'Hotel'
        m2m_table_name = db.shorten_name(u'azovhotels_hotel_labels')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('hotel', models.ForeignKey(orm['azovhotels.hotel'], null=False)),
            ('label', models.ForeignKey(orm['azovhotels.label'], null=False))
        ))
        db.create_unique(m2m_table_name, ['hotel_id', 'label_id'])

        # Adding M2M table for field files on 'Hotel'
        m2m_table_name = db.shorten_name(u'azovhotels_hotel_files')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('hotel', models.ForeignKey(orm['azovhotels.hotel'], null=False)),
            ('file', models.ForeignKey(orm['azovhotels.file'], null=False))
        ))
        db.create_unique(m2m_table_name, ['hotel_id', 'file_id'])

        # Adding model 'Action'
        db.create_table(u'azovhotels_action', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=150)),
            ('content', self.gf('django.db.models.fields.TextField')(max_length=250, null=True)),
            ('started_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('ended_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('hotel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.Hotel'])),
        ))
        db.send_create_signal('azovhotels', ['Action'])

        # Adding M2M table for field labels on 'Action'
        m2m_table_name = db.shorten_name(u'azovhotels_action_labels')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('action', models.ForeignKey(orm['azovhotels.action'], null=False)),
            ('label', models.ForeignKey(orm['azovhotels.label'], null=False))
        ))
        db.create_unique(m2m_table_name, ['action_id', 'label_id'])

        # Adding model 'Comment'
        db.create_table(u'azovhotels_comment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(max_length=550)),
            ('vote_criteria', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('vote_mark', self.gf('django.db.models.fields.DecimalField')(max_digits=9, decimal_places=3)),
            ('hotel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.Hotel'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.User'])),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.Comment'], null=True, blank=True)),
        ))
        db.send_create_signal('azovhotels', ['Comment'])

        # Adding model 'Contact'
        db.create_table(u'azovhotels_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('sex', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=150, null=True)),
            ('hotel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.Hotel'])),
        ))
        db.send_create_signal('azovhotels', ['Contact'])

        # Adding model 'Page'
        db.create_table(u'azovhotels_page', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=150)),
            ('content', self.gf('django.db.models.fields.TextField')(max_length=250, null=True, blank=True)),
            ('template', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
        ))
        db.send_create_signal('azovhotels', ['Page'])

        # Adding M2M table for field labels on 'Page'
        m2m_table_name = db.shorten_name(u'azovhotels_page_labels')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('page', models.ForeignKey(orm['azovhotels.page'], null=False)),
            ('label', models.ForeignKey(orm['azovhotels.label'], null=False))
        ))
        db.create_unique(m2m_table_name, ['page_id', 'label_id'])

        # Adding model 'RoomType'
        db.create_table(u'azovhotels_roomtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=150)),
            ('about', self.gf('django.db.models.fields.TextField')(max_length=150, null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=9, decimal_places=2)),
            ('beds', self.gf('django.db.models.fields.IntegerField')()),
            ('rooms_available', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('rooms_total', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('hotel', self.gf('django.db.models.fields.related.ForeignKey')(related_name='room_types', to=orm['azovhotels.Hotel'])),
            ('view_file', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='view_file', null=True, to=orm['azovhotels.File'])),
        ))
        db.send_create_signal('azovhotels', ['RoomType'])

        # Adding M2M table for field files on 'RoomType'
        m2m_table_name = db.shorten_name(u'azovhotels_roomtype_files')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('roomtype', models.ForeignKey(orm['azovhotels.roomtype'], null=False)),
            ('file', models.ForeignKey(orm['azovhotels.file'], null=False))
        ))
        db.create_unique(m2m_table_name, ['roomtype_id', 'file_id'])

        # Adding M2M table for field labels on 'RoomType'
        m2m_table_name = db.shorten_name(u'azovhotels_roomtype_labels')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('roomtype', models.ForeignKey(orm['azovhotels.roomtype'], null=False)),
            ('label', models.ForeignKey(orm['azovhotels.label'], null=False))
        ))
        db.create_unique(m2m_table_name, ['roomtype_id', 'label_id'])

        # Adding model 'Room'
        db.create_table(u'azovhotels_room', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=150)),
            ('room_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.RoomType'])),
            ('hotel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.Hotel'])),
        ))
        db.send_create_signal('azovhotels', ['Room'])

        # Adding model 'Reservation'
        db.create_table(u'azovhotels_reservation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True)),
            ('passport_series', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('passport_code', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('passport_issued', self.gf('django.db.models.fields.CharField')(max_length=150, null=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=150, null=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=9, decimal_places=2)),
            ('starting_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('ending_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('room_index', self.gf('django.db.models.fields.IntegerField')()),
            ('hotel', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.Hotel'])),
            ('room_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.RoomType'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='user', null=True, to=orm['azovhotels.User'])),
            ('manager', self.gf('django.db.models.fields.related.ForeignKey')(related_name='manager', null=True, to=orm['azovhotels.User'])),
            ('payed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cancelled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('arrived', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('internal', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('reservation_status', self.gf('django.db.models.fields.CharField')(default=u'\u043d\u0435 \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u043e', max_length=2)),
        ))
        db.send_create_signal('azovhotels', ['Reservation'])

        # Adding model 'RoomTypeRate'
        db.create_table(u'azovhotels_roomtyperate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('about', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=9, decimal_places=2)),
            ('room_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.RoomType'])),
        ))
        db.send_create_signal('azovhotels', ['RoomTypeRate'])

        # Adding model 'RoomTypePrice'
        db.create_table(u'azovhotels_roomtypeprice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=9, decimal_places=2)),
            ('available_rooms', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('room_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.RoomType'])),
            ('rate', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['azovhotels.RoomTypeRate'])),
        ))
        db.send_create_signal('azovhotels', ['RoomTypePrice'])

        # Adding unique constraint on 'RoomTypePrice', fields ['room_type', 'date']
        db.create_unique(u'azovhotels_roomtypeprice', ['room_type_id', 'date'])

        # Adding model 'User'
        db.create_table(u'azovhotels_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=254, blank=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('passport_series', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('passport_code', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('passport_issued', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('send_sms', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('send_email', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('blocked_till', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal('azovhotels', ['User'])

        # Adding M2M table for field groups on 'User'
        m2m_table_name = db.shorten_name(u'azovhotels_user_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm['azovhotels.user'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'User'
        m2m_table_name = db.shorten_name(u'azovhotels_user_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm['azovhotels.user'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'permission_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'RoomTypePrice', fields ['room_type', 'date']
        db.delete_unique(u'azovhotels_roomtypeprice', ['room_type_id', 'date'])

        # Deleting model 'State'
        db.delete_table(u'azovhotels_state')

        # Deleting model 'City'
        db.delete_table(u'azovhotels_city')

        # Deleting model 'Label'
        db.delete_table(u'azovhotels_label')

        # Deleting model 'File'
        db.delete_table(u'azovhotels_file')

        # Deleting model 'Hotel'
        db.delete_table(u'azovhotels_hotel')

        # Removing M2M table for field labels on 'Hotel'
        db.delete_table(db.shorten_name(u'azovhotels_hotel_labels'))

        # Removing M2M table for field files on 'Hotel'
        db.delete_table(db.shorten_name(u'azovhotels_hotel_files'))

        # Deleting model 'Action'
        db.delete_table(u'azovhotels_action')

        # Removing M2M table for field labels on 'Action'
        db.delete_table(db.shorten_name(u'azovhotels_action_labels'))

        # Deleting model 'Comment'
        db.delete_table(u'azovhotels_comment')

        # Deleting model 'Contact'
        db.delete_table(u'azovhotels_contact')

        # Deleting model 'Page'
        db.delete_table(u'azovhotels_page')

        # Removing M2M table for field labels on 'Page'
        db.delete_table(db.shorten_name(u'azovhotels_page_labels'))

        # Deleting model 'RoomType'
        db.delete_table(u'azovhotels_roomtype')

        # Removing M2M table for field files on 'RoomType'
        db.delete_table(db.shorten_name(u'azovhotels_roomtype_files'))

        # Removing M2M table for field labels on 'RoomType'
        db.delete_table(db.shorten_name(u'azovhotels_roomtype_labels'))

        # Deleting model 'Room'
        db.delete_table(u'azovhotels_room')

        # Deleting model 'Reservation'
        db.delete_table(u'azovhotels_reservation')

        # Deleting model 'RoomTypeRate'
        db.delete_table(u'azovhotels_roomtyperate')

        # Deleting model 'RoomTypePrice'
        db.delete_table(u'azovhotels_roomtypeprice')

        # Deleting model 'User'
        db.delete_table(u'azovhotels_user')

        # Removing M2M table for field groups on 'User'
        db.delete_table(db.shorten_name(u'azovhotels_user_groups'))

        # Removing M2M table for field user_permissions on 'User'
        db.delete_table(db.shorten_name(u'azovhotels_user_user_permissions'))


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'azovhotels.action': {
            'Meta': {'object_name': 'Action'},
            'content': ('django.db.models.fields.TextField', [], {'max_length': '250', 'null': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ended_at': ('django.db.models.fields.DateTimeField', [], {}),
            'hotel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.Hotel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'labels': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['azovhotels.Label']", 'symmetrical': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '150'}),
            'started_at': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.city': {
            'Meta': {'object_name': 'City'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '150'}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.State']", 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.comment': {
            'Meta': {'object_name': 'Comment'},
            'content': ('django.db.models.fields.TextField', [], {'max_length': '550'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hotel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.Hotel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.Comment']", 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.User']"}),
            'vote_criteria': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'vote_mark': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '3'})
        },
        'azovhotels.contact': {
            'Meta': {'object_name': 'Contact'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'hotel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.Hotel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'sex': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.file': {
            'Meta': {'object_name': 'File'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'deleted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.hotel': {
            'Meta': {'object_name': 'Hotel'},
            'about': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'about_short': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'arrival_from': ('django.db.models.fields.CharField', [], {'default': "'12.00'", 'max_length': '10', 'blank': 'True'}),
            'arrival_till': ('django.db.models.fields.CharField', [], {'default': "'12.00'", 'max_length': '10', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.City']"}),
            'conditions_driveway': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'conditions_feeding': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'conditions_guest_accommodation': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'conditions_internet': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'conditions_location': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'conditions_medicine': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'conditions_parking': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'conditions_pets': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'conditions_tourist_tax': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'entity_address': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_card_account': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_card_bank_mfo': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_card_bank_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_corporate_inn': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_egrpou': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_payment_account': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_payment_bank_mfo': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'entity_payment_bank_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'files': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['azovhotels.File']", 'symmetrical': 'False'}),
            'gps_latitude': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'gps_longitude': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'labels': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['azovhotels.Label']", 'symmetrical': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'promote_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'reservation_cancellation_about': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'reservation_cancellation_days': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'reservation_period': ('django.db.models.fields.IntegerField', [], {'default': '31', 'blank': 'True'}),
            'rooms_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '150'}),
            'stars_category': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.User']"}),
            'view_file': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'view'", 'null': 'True', 'to': "orm['azovhotels.File']"}),
            'votes_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'votes_weight': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'working_from': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 1, 1, 0, 0)', 'blank': 'True'}),
            'working_till': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 12, 31, 0, 0)', 'blank': 'True'})
        },
        'azovhotels.label': {
            'Meta': {'object_name': 'Label'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.page': {
            'Meta': {'object_name': 'Page'},
            'content': ('django.db.models.fields.TextField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'labels': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['azovhotels.Label']", 'symmetrical': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '150'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.reservation': {
            'Meta': {'object_name': 'Reservation'},
            'arrived': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ending_at': ('django.db.models.fields.DateTimeField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'hotel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.Hotel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'manager': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'manager'", 'null': 'True', 'to': "orm['azovhotels.User']"}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'passport_code': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'passport_issued': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'passport_series': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'payed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '2'}),
            'reservation_status': ('django.db.models.fields.CharField', [], {'default': "u'\\u043d\\u0435 \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u043e'", 'max_length': '2'}),
            'room_index': ('django.db.models.fields.IntegerField', [], {}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.RoomType']"}),
            'starting_at': ('django.db.models.fields.DateTimeField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user'", 'null': 'True', 'to': "orm['azovhotels.User']"})
        },
        'azovhotels.room': {
            'Meta': {'object_name': 'Room'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hotel': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.Hotel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.RoomType']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'about': ('django.db.models.fields.TextField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'beds': ('django.db.models.fields.IntegerField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'files': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['azovhotels.File']", 'symmetrical': 'False'}),
            'hotel': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'room_types'", 'to': "orm['azovhotels.Hotel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'labels': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['azovhotels.Label']", 'symmetrical': 'False'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '2'}),
            'rooms_available': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rooms_total': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '150'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'view_file': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'view_file'", 'null': 'True', 'to': "orm['azovhotels.File']"})
        },
        'azovhotels.roomtypeprice': {
            'Meta': {'unique_together': "(('room_type', 'date'),)", 'object_name': 'RoomTypePrice'},
            'available_rooms': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '2'}),
            'rate': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.RoomTypeRate']"}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.RoomType']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.roomtyperate': {
            'Meta': {'object_name': 'RoomTypeRate'},
            'about': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '9', 'decimal_places': '2'}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['azovhotels.RoomType']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.state': {
            'Meta': {'object_name': 'State'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'azovhotels.user': {
            'Meta': {'object_name': 'User'},
            'blocked_till': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '254', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'passport_code': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'passport_issued': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'passport_series': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'send_email': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'send_sms': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['azovhotels']