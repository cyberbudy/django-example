# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import (authenticate,
    login as auth_login, logout as auth_logout)
from django.contrib.auth.forms import AuthenticationForm
import django.contrib.auth.hashers
from django.template.defaultfilters import slugify
from django.db.models import Q
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404
from django.contrib.auth.views import password_reset
from django.contrib.auth.views import password_reset, password_reset_confirm

from azovhotels.models import User, Label, Hotel
from azovhotels.serializers.admin import UserDetailSerializer, HotelDetailSerializer, ContactDetailSerializer
from azovhotels.utils.view import json
from azovhotels.utils.permissions import check_user_group



@csrf_exempt
def login(request, page=None):
    """
    Logging user in.
    """
    messages = []

    if (request.method == 'POST'):
        user = None

        try:
            user = User.objects.get(email=request.POST['email'])
        except:
            messages.append('Такого пользователя не существует.')

        print user

        if user:
            if user.check_password(request.POST['password']):
                auth_login(request, user)

                redirect_to = 'site'

                if(user.is_superuser):
                    redirect_to = 'admin'
                elif(user.has_perm('azovhotels.change_hotel')):
                    redirect_to = 'hotelier'

                print(redirect_to)

                redirect_to = request.GET.get('redirect', reverse(redirect_to))

                print(redirect_to)

                return redirect(redirect_to)
            else:
                return redirect(reverse('auth.login'))

    return render(request, 'auth/login.html', {
        'messages': messages,
    })


def logout(request):
    auth_logout(request)

    return redirect(reverse('site'))


def reset(request):
    if request.POST:
        user = User.objects.filter(email=request.POST.get('email'))

        if not user:
            return render(request, 'auth/error.html')


    return password_reset(
        request,
        template_name='auth/reset.html',
        email_template_name='auth/reset_email.html',
        subject_template_name='auth/reset_subject.txt',
        post_reset_redirect=reverse('auth.reset.success'))


def reset_confirm(request, uidb64=None, token=None):
    return password_reset_confirm(
        request,
        template_name='auth/reset-pass.html',
        uidb64=uidb64,
        token=token,
        post_reset_redirect=reverse('auth.reset.success'))


def reset_success(request):
  return render(request, "auth/reset-success.html")
