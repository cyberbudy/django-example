# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render, get_object_or_404
from django.core.urlresolvers import reverse
from django.db.models import Count, Q, Min, Sum
from django.http import HttpResponse, Http404
from django.forms.models import model_to_dict

from rest_framework.renderers import JSONRenderer, YAMLRenderer, TemplateHTMLRenderer
from rest_framework import serializers as s, status
from rest_framework.response import Response
from rest_framework.decorators import api_view, action

from azovhotels.utils.base import BaseView
from azovhotels.utils.reservations import dates_between_in_year
from azovhotels.utils.context_processors import config_pages
from azovhotels.serializers.site.hotels import HotelListSerializer, HotelAtomSerializer
from azovhotels.serializers.site.room_types import RoomTypeListSerializer

import azovhotels.models as m
import azovhotels.serializers.admin as ads

import datetime




class HotelView(BaseView):
    model = m.Hotel
    serializer_class = HotelListSerializer
    atom_serializer_class = HotelAtomSerializer
    # ordering_fields = ('mark', 'promote_count', 'id')

    # renderer_classes = (TemplateHTMLRenderer, )


    def create(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


    def list(self, request, *args, **kwargs):
        cookies_fields = ['date_arrival', 'date_leave', 'guests', 'destination', 'conveniences', 'hotel_type']

        order_by = request.GET.get('ordering', 'promote_count')

        hotel_types = m.Label.objects\
            .filter(type='hotel_type')

        hotel_conveniences = m.Label.objects\
            .filter(type='hotel_conveniences')

        # get search data, if post -> update cookies
        if not request.POST:
            data = request.COOKIES.copy()
        else:
            data = request.POST.copy()

            for key in cookies_fields:
                request.COOKIES[key] = data.get(key, '')

        date_start = data.get('date_arrival')
        date_end = data.get('date_leave')
        beds = data.get('guests', 1)
        string = data.get('destination', '')
        conveniences = data.get('conveniences', [])
        types = data.get('hotel_type', [])

        price_from = data.get('price_from', 0) 
        price_to = data.get('price_to', 100500)
        
        # checking if data valid
        try:
            beds = int(beds)
        except:
            beds = 1

        try:
            date_start = datetime.datetime.strptime(date_start, '%Y-%m-%d').date()
            date_end = datetime.datetime.strptime(date_end, '%Y-%m-%d').date()
        except:
            date_start = datetime.datetime.now().date()
            date_end = (datetime.datetime.now()+datetime.timedelta(days=1)).date()

        hotels = m.Hotel.objects\
            .extra(select={'default_price': 
                '''SELECT MIN(p.price) from azovhotels_roomtypeprice as p, azovhotels_roomtype as r
                    where  p.room_type_id = r.id and
                        r.hotel_id = azovhotels_hotel.id and
                        p.date='%s' and p.price BETWEEN %s AND %s and r.beds >= %s and
                        (SELECT IFNULL ((SELECT MIN(rtp.available_rooms) from azovhotels_roomtypeprice as rtp where (date >= '%s' and date <= '%s') and rtp.room_type_id=r.id)
                            , r.rooms_available))
                            >
                        (select COUNT(*) as c 
                            from (select COUNT(*), res.room_type_id from azovhotels_reservation as res 
                                where 
                                (((res.starting_at BETWEEN '%s' and '%s') or (res.ending_at BETWEEN '%s' AND '%s')) or
                                    (('%s' BETWEEN res.starting_at and res.ending_at) or ('%s' BETWEEN res.starting_at and res.ending_at)))
                                GROUP BY res.room_type_id, res.room_index
                            ) as d where d.room_type_id=r.id)
                 '''%(date_start,
                    price_from,
                    price_to,
                    beds,
                    date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_end
                    )})\
            .extra(select={'mark':
             'votes_weight / votes_count'})\
            .extra(select={'c_id':
                'azovhotels_hotel.id'
                })\
            .extra(select={'min_price':
                '''SELECT MIN(
                        (rt.price*(%s-
                            IFNULL(
                                (SELECT COUNT(*) 
                                    from azovhotels_roomtypeprice as rtp 
                                    where rtp.room_type_id=rt.id and rtp.date between '%s' and '%s'),
                             0))
                            +
                           IFNULL(
                                (select SUM(rtp1.price)
                                from azovhotels_roomtypeprice as rtp1
                                where rtp1.room_type_id=rt.id 
                                and rtp1.date between '%s' and '%s'),
                             0)
                        )/%s) from azovhotels_roomtype as rt

                    where rt.hotel_id = azovhotels_hotel.id 
                        and (rt.price*(%s-IFNULL(
                                (SELECT COUNT(*) 
                                    from azovhotels_roomtypeprice as rtp 
                                    where rtp.room_type_id=rt.id 
                                    and rtp.date between '%s' 
                                    and '%s')
                                , 0))
                            + 
                            IFNULL((select SUM(rtp1.price) 
                                from azovhotels_roomtypeprice as rtp1 
                                where rtp1.room_type_id=rt.id 
                                and rtp1.date between '%s' and '%s'), 
                            0)
                        /%s) between %s and %s 
                        and rt.rooms_available > 0
                        and rt.beds>=%s
                        and if (
                                (SELECT COUNT(rtp1.available_rooms) 
                                from azovhotels_roomtypeprice as rtp1 
                                where (rtp1.date >= '%s' and rtp1.date <= '%s') 
                                    and rtp1.room_type_id=rt.id) 
                                    <
                                %s,

                                if(
                                    (SELECT IFNULL((SELECT MIN(rtp1.available_rooms)
                                    from azovhotels_roomtypeprice as rtp1 
                                    where (rtp1.date >= '%s' and rtp1.date <= '%s') 
                                        and rtp1.room_type_id=rt.id), rt.rooms_available)) 
                                        <
                                     rt.rooms_available,

                                    (SELECT IFNULL((SELECT MIN(rtp1.available_rooms)
                                    from azovhotels_roomtypeprice as rtp1 
                                    where (rtp1.date >= '%s' and rtp1.date <= '%s') 
                                        and rtp1.room_type_id=rt.id), rt.rooms_available))

                                    ,rt.rooms_available)

                                , (SELECT IFNULL((SELECT MIN(rtp1.available_rooms)
                                from azovhotels_roomtypeprice as rtp1 
                                where (rtp1.date >= '%s' and rtp1.date <= '%s') 
                                    and rtp1.room_type_id=rt.id), rt.rooms_available)))
                                >
                            IFNULL((select COUNT(*) as c 
                                from (select COUNT(*), res.room_type_id 
                                    from azovhotels_reservation as res 
                                    where (((res.starting_at >= '%s' and res.starting_at <= '%s')
                                        or (res.ending_at >= '%s' and res.ending_at <= '%s')) 
                                        or (('%s' >= res.starting_at and '%s' <= res.ending_at)
                                        or ('%s' >= res.starting_at and '%s' <= res.ending_at))
                                    ) and res.internal = True and res.cancelled = False
                                    and not (res.starting_at = '%s' or res.ending_at = '%s')
                                    GROUP BY res.room_type_id, res.room_index
                                ) as d where d.room_type_id=rt.id), 
                            0)

                        and rt.rooms_available
                                >   
                            IFNULL((select COUNT(*) as c 
                                from (select COUNT(*), res.room_type_id 
                                    from azovhotels_reservation as res 
                                    where (((res.starting_at >= '%s' and res.starting_at <= '%s')
                                        or (res.ending_at >= '%s' and res.ending_at <= '%s')) 
                                        or (('%s' >= res.starting_at and '%s' <= res.ending_at)
                                        or ('%s' >= res.starting_at and '%s' <= res.ending_at))
                                    ) and res.cancelled = False
                                    and not (res.starting_at = '%s' or res.ending_at = '%s')
                                    GROUP BY res.room_type_id, res.room_index
                                ) as d where d.room_type_id=rt.id), 
                            0) 

                '''%((date_end-date_start).days,
                    date_start,
                    date_end-datetime.timedelta(days=1),
                    date_start,
                    date_end-datetime.timedelta(days=1),
                    (date_end-date_start).days,
                    (date_end-date_start).days,
                    date_start,
                    date_end-datetime.timedelta(days=1),
                    date_start,
                    date_end-datetime.timedelta(days=1),
                    (date_end-date_start).days,
                    price_from,
                    price_to,
                    beds,
                    date_start,
                    date_end,
                    date_end-datetime.timedelta(days=1),
                    date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_start,
                    date_end,
                    date_end,
                    date_end,
                    date_start,
                    date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_start,
                    date_end,
                    date_end,
                    date_end,
                    date_start,
                )})

        hotels = hotels\
            .filter(Q(reservation_period__lte=(date_end-date_start).days)&
                (Q(title__icontains=string)|Q(city__name__icontains=string)|Q(slug__icontains=string)|Q(address__icontains=string))&
                Q(user__is_active=True)&
                Q(is_active=True)
            )
  
        if conveniences:
            hotels = hotels.filter(labels__in=conveniences)

        if types:
            hotels = hotels.filter(labels__in=types)

        valid_hotels = []

        for hotel in hotels:
            if hotel.min_price and dates_between_in_year(hotel.working_from, date_start, hotel.working_till, date_end):
                valid_hotels.append(hotel)

        self.min_price = 10005000
        self.max_price = 0

        for hotel in valid_hotels:
            if hotel.min_price <= self.min_price:
                self.min_price = hotel.min_price

            if hotel.min_price >= self.max_price:
                self.max_price = hotel.min_price

        if not valid_hotels:
            self.min_price = self.max_price = 0



        self.object_list = valid_hotels

        if not self.allow_empty and not self.object_list:
            warnings.warn(
                'The `allow_empty` parameter is due to be deprecated. '
                'To use `allow_empty=False` style behavior, You should override '
                '`get_queryset()` and explicitly raise a 404 on empty querysets.',
                PendingDeprecationWarning
            )
            class_name = self.__class__.__name__
            error_msg = self.empty_error % {'class_name': class_name}
            raise Http404(error_msg)

        # Switch between paginated or standard style responses
        page = self.paginate_queryset(self.object_list)
        if page is not None:
            serializer = self.get_pagination_serializer(page)
        else:
            serializer = self.get_serializer(self.object_list, many=True)

        r_data = serializer.data
        r_data['types'] = ads.LabelListSerializer(hotel_types, many=True).data
        r_data['conveniences'] = ads.LabelListSerializer(hotel_conveniences, many=True).data
        r_data['menus'] = config_pages(request)
        r_data['min_price'] = self.min_price
        r_data['max_price'] = self.max_price

        for key, value in data.items():
            request.COOKIES[key] = value

        response = render(request, 'site/list-hotels.html', r_data)

        for key in cookies_fields:
            try: vvv = data.get(key, '').encode('utf-8')
            except: vvv = data.get(key, '')
            response.set_cookie(key, vvv)

        return response

    

    def retrieve(self, request, id=None, slug=None):
        cookies_fields = ['date_arrival', 'date_leave', 'guests']

        if not request.POST:
            search_data = request.COOKIES
        else:
            search_data = request.POST

            for key in cookies_fields:
                request.COOKIES[key] = search_data.get(key, '')


        beds = search_data.get('guests', 1)
        date_start = search_data.get('date_arrival')
        date_end = search_data.get('date_leave')
        
        hotel = get_object_or_404(m.Hotel, id=id, slug=slug)

        try:
            beds = int(beds)
        except:
            beds = 1

        try:
            date_start = datetime.datetime.strptime(date_start, '%Y-%m-%d').date()
            date_end = datetime.datetime.strptime(date_end, '%Y-%m-%d').date()
        except:
            date_start = datetime.datetime.now().date()
            date_end = (datetime.datetime.now()+datetime.timedelta(days=1)).date()

        d_e = (date_end-date_start).days

        comments = m.Comment.objects\
            .filter(hotel=id)

        room_types = m.RoomType.objects\
            .extra(select={'c_id':
                '''id'''})\
            .extra(select={'d_price':
                '''price'''
                })\
            .extra(select={'c_beds':
                'beds'
                })\
            .extra(select={'all_available':
                '''SELECT 
                        rooms_available
                            >
                        IFNULL((SELECT COUNT(*) as c
                        from 
                            (SELECT COUNT(*), res.room_type_id as rt_id
                                from azovhotels_reservation as res
                                where (((res.starting_at > '%s' and res.starting_at < '%s')
                                        or (res.ending_at > '%s' and res.ending_at < '%s'))
                                        or(('%s' > res.starting_at and '%s' < res.ending_at)
                                            or ('%s' > res.starting_at and '%s' < res.ending_at)
                                        )
                                    ) and res.cancelled = False
                                    and not (res.starting_at = '%s' or res.ending_at = '%s')
                                GROUP BY res.room_type_id, res.room_index) as cc 
                        where cc.rt_id=c_id
                        ), 0)
                '''%(date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_start,
                    date_end,
                    date_end,
                    date_end,
                    date_start)
                })\
            .extra(select={'internal_available':
                '''SELECT 
                    (SELECT IF(
                        (SELECT COUNT(rtp.available_rooms)
                        from azovhotels_roomtypeprice as rtp
                        where rtp.date BETWEEN '%s' AND  '%s' 
                            and rtp.room_type_id=c_id and c_beds>=%s) < %s,

                        if(
                            (SELECT MIN(rtp.available_rooms)
                            from azovhotels_roomtypeprice as rtp
                            where rtp.date BETWEEN '%s' AND  '%s' 
                                and rtp.room_type_id=c_id and c_beds>=%s) < rooms_available,

                            (SELECT MIN(rtp.available_rooms)
                            from azovhotels_roomtypeprice as rtp
                            where rtp.date BETWEEN '%s' AND  '%s' 
                                and rtp.room_type_id=c_id and c_beds>=%s),

                            rooms_available
                        ),

                        rooms_available))
                            > 
                        IFNULL((SELECT COUNT(*) as c
                        from 
                            (SELECT COUNT(*), res.room_type_id as rt_id
                                from azovhotels_reservation as res
                                where (((res.starting_at >= '%s' and res.starting_at <= '%s')
                                        or (res.ending_at >= '%s' and res.ending_at <= '%s'))
                                        or(('%s' >= res.starting_at and '%s' <= res.ending_at)
                                            or ('%s' >= res.starting_at and '%s' <= res.ending_at)
                                        )
                                    ) and res.internal = True and res.cancelled = False
                                    and not (res.starting_at = '%s' or res.ending_at = '%s')
                                GROUP BY res.room_type_id, res.room_index) as cc 
                        where cc.rt_id=c_id
                        ), 0)  
                '''%(date_start,
                    date_end,
                    beds,
                    d_e,
                    date_start,
                    date_end,
                    beds,
                    date_start,
                    date_end,
                    beds,
                    date_start,
                    date_end,
                    date_start,
                    date_end,
                    date_start,
                    date_start,
                    date_end,
                    date_end,
                    date_end,
                    date_start,
                    )})\
            .extra(select={'dates_price':
                '''
                select CEIL(SUM(d.price)) from azovhotels_roomtypeprice as d
                where d.room_type_id=c_id and d.date BETWEEN '%s' and '%s' and internal_available = 1
                '''%(date_start, date_end-datetime.timedelta(days=1))})\
            .extra(select={'def_price':
                '''
                select CEIL(price*(%s-(SELECT COUNT(d.id) from azovhotels_roomtypeprice as d
                where d.room_type_id=c_id and d.date BETWEEN '%s' and '%s' and internal_available = 1)))
                '''%((date_end - date_start).days, date_start, date_end-datetime.timedelta(days=1))
                })\
            .filter(hotel=id)


        rs = []
        for room_type in room_types:
            print(room_type.internal_available, room_type.all_available)
            if room_type.internal_available and room_type.all_available:
                rs.append(room_type)

        ids = ', '.join([str(x.id) for x in rs]) if rs else -1

        hotel = m.Hotel.objects\
            .filter(id=id, slug=slug)\
            .extra(select={'min_price': 
                '''select CEIL
                    (
                        MIN(
                            IFNULL((
                                SELECT SUM(rp.price) from azovhotels_roomtypeprice as rp
                                where rp.room_type_id=r.id and date BETWEEN '%s' and '%s'
                            ), 0)
                            +
                            IFNULL((
                                SELECT r.price*(%s-(SELECT COUNT(*) from azovhotels_roomtypeprice as rp
                                where rp.room_type_id=r.id and date BETWEEN '%s' and '%s')) 
                            ), 0)
                        )
                   )
                    from azovhotels_roomtype as r 
                    where r.id in (%s)
                '''%(date_start, date_end-datetime.timedelta(days=1), (date_end-date_start).days, date_start, date_end-datetime.timedelta(days=1), ids)})\
            .extra(select={'mark': 'votes_weight / votes_count'})\
            .all()

        try:
            hotel = hotel[0]
        except:
            raise Http404

        if not (dates_between_in_year(hotel.working_from, date_start,
            hotel.working_till, date_end)):
            rs = []
            hotel.min_price = 0

        if((date_end - date_start).days < hotel.reservation_period):
            rs = []
            hotel.min_price = 0

        room_type_serializer = RoomTypeListSerializer(rs, many=True)
        
        response = render(request, 'site/hotel.html', {
            'hotel': self.get_serializer(hotel).data,
            'comments': ads.CommentListSerializer(comments, many=True).data,
            'room_types': room_type_serializer.data,
            'menus': config_pages(request),
            })


        for key in cookies_fields:
            try: vvv = search_data.get(key, '').encode('utf-8')
            except: vvv = search_data.get(key, '')
            response.set_cookie(key, vvv)

        return response



    @action(['POST'])
    def set_comment(self, request, id=None, slug=None):
        data = request.POST

        text = data.get('text', '')
        parent_id = data.get('parent', None)

        Hotel = get_object_or_404(m.Hotel, id=id)
        user = request.user

        if parent_id:
            parent = get_object_or_404(m.User, id=parent_id)

        if user.is_anonymous():
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        save_data = {
            'content': text,
            'parent': parent_id,
            'user': user.id,
            'hotel': Hotel.id
        }

        comment_serializer = ads.CommentDetailSerilazer(data=save_data)

        if comment_serializer.is_valid():
            comment_serializer.save()

            return Response({
                'comment': comment_serializer.data
                })

        return Response(comment_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
