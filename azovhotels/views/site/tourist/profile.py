# -*- coding: utf-8 -*-


from rest_framework import mixins, viewsets
from azovhotels.serializers.site.tourist import UserProfileSerializer
from azovhotels.utils.permissions import TouristPermissions


import azovhotels.models as m


class UserProfileView(mixins.UpdateModelMixin,
                    mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):


    serializer_class = UserProfileSerializer
    permission_classes = (TouristPermissions,)

    def get_queryset(self):
        if self.request.user.is_anonymous:
            return m.User.objects.all()

        return self.request.user
