# -*- coding: utf-8 -*-

from rest_framework import mixins, viewsets, status, pagination, serializers as s
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes

from django.core.paginator import Paginator
from django.conf import settings 
from django.shortcuts import get_object_or_404

from azovhotels.serializers.site.tourist import ReservationListSerializer
from azovhotels.utils.pagination import ModelPaginationSerializer, paginate
from azovhotels.utils.permissions import TouristPermissions

import azovhotels.models as m
import azovhotels.serializers.admin as ads


@api_view(['GET'])
@permission_classes((TouristPermissions, ))
def reservation_list(request):
    user = request.user

    if user.is_anonymous:
        reservations = m.Reservation.objects\
            .select_related('hotel')\
            .select_related('hotel__view_file')\
            .all() # should be redirect
    else:
        reservations = m.Reservation.objects\
            .filter(user=user)\
            .select_related(hotel)\
            .order_by(-created_at)


    reservation_serializer = ReservationListSerializer(reservations, many=True)

    return Response({
        'reservations': reservation_serializer.data,
    })