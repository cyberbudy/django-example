# -*- coding: utf-8 -*-


from rest_framework import mixins, viewsets, status, pagination, serializers as s
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes

from django.core.paginator import Paginator
from django.conf import settings 
from django.shortcuts import get_object_or_404

from azovhotels.serializers.site.tourist import CommentListSerializer
from azovhotels.utils.pagination import ModelPaginationSerializer, paginate
from azovhotels.utils.permissions import TouristPermissions

import azovhotels.models as m
import azovhotels.serializers.admin as ads



class ModelPaginationSerializer(ModelPaginationSerializer):
    class Meta:
        object_serializer_class = CommentListSerializer



class HotelSerializer(s.ModelSerializer):
    related_data = s.SerializerMethodField('get_related_data')

    class Meta:
        model = m.Hotel
        exclude = ('city', 'user', 'labels', 'files')
        include = ('related_data', )


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
        }



class ReservationSerializer(s.ModelSerializer):
    class Meta:
        model = m.Hotel
        fields = ('id', 'created_at')



@api_view(['GET'])
@permission_classes((TouristPermissions,))
def comment_list(request):
    user = request.user

    if not user.is_anonymous:
        comments = m.Commnet.objects\
            .filter(user=user)
    else:
        comments = m.Comment.objects.all()

    reservations = m.Reservation.objects\
        .filter(arrived=True, user=user)\
        .values()


    reserved_hotels = set([x['hotel_id'] for x in reservations])
    commented_hotels = set([x['hotel_id'] for x in comments.values()])
    uncommented_hotels = []

    uncommented_hotels = reserved_hotels - commented_hotels

    r_hotels = [x for x in reservations if x['hotel_id'] in uncommented_hotels]

    hotels = m.Hotel.objects\
        .filter(id__in=uncommented_hotels)

    page = request.QUERY_PARAMS.get('page', 1)

    comments_paginator = paginate(comments, page)

    serializer_context = {
        'request': request
    }

    comment_serializer = ModelPaginationSerializer(comments_paginator, context=serializer_context)
    hotel_serializer = HotelSerializer(hotels, many=True)
    r = ReservationSerializer(r_hotels, many=True)

    return Response({
            'comments': comment_serializer.data,
            'hotels': hotel_serializer.data,
            'reservations': r.data
        })


@api_view(['DELETE'])
def comment_detail(request, pk):
    comment = get_object_or_404(m.Comment, id=pk)

    comment.delete()

    return Response(status=status.HTTP_204_NO_CONTENT)