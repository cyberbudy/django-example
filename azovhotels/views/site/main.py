# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render
from django.core.urlresolvers import reverse
from django.http import HttpResponseForbidden
from django.db.models import Count

from rest_framework.decorators import api_view
from rest_framework import serializers as s
from rest_framework.response import Response

from azovhotels.utils.context_processors import config_pages

import azovhotels.models as m
import azovhotels.serializers.admin as ads

import datetime


class HotelSerializer(s.ModelSerializer):
    # city = s.Field('city_id')
    user = s.Field('user_id')

    related_data = s.SerializerMethodField('get_related_data')



    class Meta:
        model = m.Hotel
        exclude = ('labels', 'files')
        # include = ('related_data', 'mark')


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
            'city': ads.CityListSerializer(obj.city).data
        }


class HotelPriceSerializer(s.ModelSerializer):
    # city = s.Field('city_id')
    user = s.Field('user_id')

    related_data = s.SerializerMethodField('get_related_data')
    mark = s.Field('mark')
    min_price = s.Field('min_price')



    class Meta:
        model = m.Hotel
        exclude = ('labels', 'files')
        # include = ('related_data', 'mark')


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
            'city': ads.CityListSerializer(obj.city).data
        }

class CitySerializer(s.ModelSerializer):
    name = s.Field('name')
    slug = s.Field('slug')
    hotels_count = s.Field('hotels_count')

    state = s.Field('state_id')

    class Meta:
        model = m.State



def root(request):
    """
    Site root view.
    """
    return render(request, 'site/placeholder.html')



@api_view(['GET'])
def index(request):
    date_start = datetime.datetime.now()
    date_end = datetime.datetime.now() + datetime.timedelta(days=1)

    hotels = m.Hotel.objects\
        .filter(is_active=True)\
        .select_related('city')\


    hotels_rate = hotels\
        .extra(select={'mark': 'votes_weight / votes_count'})\
        .extra(select={'default_price': 
            '''select MIN(r.price) from azovhotels_roomtype as r
                where r.hotel_id = azovhotels_hotel.id and
                    r.rooms_available > (SELECT COUNT(*) as c from (SELECT COUNT(*) as cc from azovhotels_reservation as res 
                        INNER JOIN azovhotels_roomtype as ar on res.room_type_id = ar.id
                        where (res.starting_at BETWEEN '%s' and '%s') or (res.ending_at BETWEEN '%s' AND '%s')
                        GROUP BY res.room_type_id, res.room_index) as d) 
                '''%(date_start.date(), date_end.date(), date_start.date(), date_end.date())})\
        .extra(select={'dates_price': 
            '''select MIN(p.price) from azovhotels_roomtypeprice as p, azovhotels_roomtype as r
                where  p.room_type_id = r.id and
                r.hotel_id = azovhotels_hotel.id and
                p.date='%s' and
                r.rooms_available > (SELECT COUNT(*) as c from (SELECT COUNT(*) as cc from azovhotels_reservation as res 
                    INNER JOIN azovhotels_roomtype as ar on res.room_type_id = ar.id
                    where (res.starting_at BETWEEN '%s' and '%s') or (res.ending_at BETWEEN '%s' AND '%s')
                    GROUP BY res.room_type_id, res.room_index) as dd)
         '''%(date_start.date(), date_start.date(), date_end.date(), date_start.date(), date_end.date())})\
        .extra(select={'min_price': '''select if
                    (default_price < dates_price, default_price, IFNULL(dates_price, default_price))
                    '''})\
        .order_by('-mark').all()[:8]





    cities = m.City.objects\
        .values('id', 'name', 'slug')\
        .filter(hotel__is_active=True)\
        .annotate(hotels_count=Count('hotel'))\
        .order_by('-hotels_count')\
        .all()


    most_city = cities[0]
    most_cities = cities[:4]

    hotels_promoted = hotels\
        .filter(city=cities[0]['id'])\
        .order_by('-promote_count')\
        .all()[:8]


    articles_slug = request.azovhotels_config.get('slug_articles', None)
    article = None

    if articles_slug:
        article = m.Page.objects\
            .filter(labels__slug=articles_slug)\
            .all()[:1]

        try:
            article = article[0]
        except:
            pass



    response = render(request, 'site/index.html', {
        'hotels_rate': HotelPriceSerializer(hotels_rate).data,
        'hotels_promoted': HotelSerializer(hotels_promoted).data,
        'article': article,
        'cities': CitySerializer(cities).data,
        'most_city': CitySerializer(most_city).data,
        'most_cities': CitySerializer(most_cities).data,
        'menus': config_pages(request)
    })

    return response


def pg(request):
    """
    Site pg view.
    """
    return render(request, 'site/hotel.html')