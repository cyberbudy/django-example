# -*- cofing: utf-8 -*-
from rest_framework import viewsets, serializers, generics
from django.contrib.auth import get_user_model

from azovhotels.serializers.site.hotelier import ReservationListSerializer
from azovhotels.utils.base import ListView

import azovhotels.models as m


class ReservationFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=m.Hotel.objects.all())
    user = df.ModelChoiceFilter(name='user',
        queryset=get_user_model().objects.all())
    status = df.Filter(name="reservation_status")

    class Meta:
        model = m.Reservation
        fields = ('hotel', 'user', 'status')



class ReservationListView(ListView):
    queryset = m.Reservation.objects\
        .select_related('room_type')\
        .select_related('user')\
        .select_related('manager')\
        .all()
    serializer_class = ReservationListSerializer
    filter_class = ReservationFilter
    atom_serializer_class = ReservationListSerializer

    ordering_fields = ('reservation_status', 'user', 'last_name', 'id', 'created_at')


    # def get_queryset(self):
    #     month = self.request.GET.