# -*- coding: utf-8 -*-

from .hotels import HotelListView
from .contacts import ContactListView
from .room_types import RoomTypeListView
from .actions import ActionListView
from .files import FileListView
from .room_type_prices import RoomTypePriceView
from .room_type_rates import RoomTypeRateListView