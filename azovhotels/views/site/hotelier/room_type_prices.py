# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, status
from rest_framework.response import Response

import django_filters as df

from azovhotels.utils.permissions import HotelierPermissions

import azovhotels.models as m

from azovhotels.views.admin.room_type_prices import RoomTypePriceListView as ARoomTypePriceView


class RoomTypePriceView(ARoomTypePriceView):
    permission_classes = (HotelierPermissions,)
