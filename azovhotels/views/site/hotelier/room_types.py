from rest_framework import mixins, viewsets, permissions, serializers as s, status
from rest_framework.response import Response

import django_filters as df
import azovhotels.models as m

from azovhotels.serializers.site.hotelier import RoomTypeListSerializer, RoomTypeDetailSerializer
from azovhotels.serializers.admin import RoomTypeRateDetailSerializer
from azovhotels.utils.base import ListView
from azovhotels.utils.permissions import HotelierPermissions



class RoomTypeFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=m.Hotel.objects.all())

    class Meta:
        model = m.RoomType
        fields = ('hotel',)



class RoomTypeListView(ListView):
    model = m.RoomType
    serializer_class = RoomTypeListSerializer
    atom_serializer_class = RoomTypeDetailSerializer
    filter_class = RoomTypeFilter

    permission_classes = (HotelierPermissions,)
    paginate_by = 1000

    def create(self, request, *args, **kwargs):
        slug = request.DATA.get('slug', None)

        if slug:
            model = self.get_queryset().model
            count = model.objects\
                .filter(slug__istartswith=slug)\
                .count()

            if count > 0:
                slug += str(count)

            request.DATA['slug'] = slug
            
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)
            headers = self.get_success_headers(serializer.data)

            rate_serializer = RoomTypeRateDetailSerializer(data={
                'price': serializer.data.get('price'),
                'room_type': serializer.data.get('id')
                })

            if not rate_serializer.is_valid():
                return Response(rate_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            rate_serializer.save()


            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)