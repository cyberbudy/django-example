# -*- coding: utf-8 -*-

import os

from django.conf import settings
from rest_framework import viewsets, status
from rest_framework.response import Response
import django_filters as df

from azovhotels.models import File, RoomType, Hotel
from azovhotels.serializers.admin import FileListSerializer, FileDetailSerializer
from azovhotels.utils.text import uid
from azovhotels.utils.base import ListView
from azovhotels.views.admin.files import create_file
from azovhotels.utils.permissions import HotelierPermissions



class FileFilter(df.FilterSet):
    room_type = df.ModelChoiceFilter(name="roomtype", queryset=RoomType.objects.all())
    hotel = df.ModelChoiceFilter(name="hotel", queryset=Hotel.objects.all())

    class Meta:
        model = File



class FileListView(ListView):
    model = File
    serializer_class = FileListSerializer
    filter_class = FileFilter
    atom_serializer_class = FileDetailSerializer

    permission_classes = (HotelierPermissions,)


    def create(self, request, *args, **kwargs):
        return create_file(self, request, *args, **kwargs)
