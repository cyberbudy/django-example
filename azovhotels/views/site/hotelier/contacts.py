from rest_framework import mixins, viewsets, permissions, serializers as s, status
from rest_framework.response import Response

import django_filters as df
import azovhotels.models as m

from azovhotels.serializers.site.hotelier import ContactListSerializer
from azovhotels.utils.base import ListView
from azovhotels.utils.permissions import HotelierPermissions



class ContactFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=m.Hotel.objects.all())

    class Meta:
        model = m.Contact
        fields = ('hotel',)



class ContactListView(ListView):
    model = m.Contact
    serializer_class = ContactListSerializer
    atom_serializer_class = ContactListSerializer
    filter_class = ContactFilter

    permission_classes = (HotelierPermissions,)


##############################################################################
# need testing, now sasving only many records at once

    # def update(self, request, *args, **kwargs):
    #     partial = kwargs.pop('partial', False)
    #     self.object = self.get_object_or_none()

    #     serializer = ContactListSerializer(self.object, data=request.DATA,
    #                                      files=request.FILES, partial=partial, many=True)

    #     if not serializer.is_valid():
    #         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #     try:
    #         self.pre_save(serializer.object)
    #     except ValidationError as err:
    #         # full_clean on model instance may be called in pre_save,
    #         # so we have to handle eventual errors.
    #         return Response(err.message_dict, status=status.HTTP_400_BAD_REQUEST)

    #     if self.object is None:
    #         self.object = serializer.save(force_insert=True)
    #         self.post_save(self.object, created=True)
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)

    #     self.object = serializer.save(force_update=True)
    #     self.post_save(self.object, created=False)
    #     return Response(serializer.data, status=status.HTTP_200_OK)


    # def create(self, request, *args, **kwargs):
    #     serializer = ContactListSerializer(data=request.DATA, files=request.FILES)

    #     if serializer.is_valid():
    #         self.pre_save(serializer.object)
    #         self.object = serializer.save(force_insert=True)
    #         self.post_save(self.object, created=True)
    #         headers = self.get_success_headers(serializer.data)
    #         return Response(serializer.data, status=status.HTTP_201_CREATED,
    #                         headers=headers)

    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        