from rest_framework import mixins, viewsets, permissions, serializers as s, status
from rest_framework.response import Response

import django_filters as df
import azovhotels.models as m

from azovhotels.serializers.site.hotelier import ActionListSerializer, ActionDetailSerializer
from azovhotels.utils.base import ListView
from azovhotels.utils.permissions import HotelierPermissions


class ActionFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=m.Hotel.objects.all())

    class Meta:
        model = m.Action
        fields = ('hotel',)



class ActionListView(ListView):
    model = m.Action
    serializer_class = ActionListSerializer
    atom_serializer_class = ActionDetailSerializer
    filter_class = ActionFilter


    permission_classes = (HotelierPermissions,)
    