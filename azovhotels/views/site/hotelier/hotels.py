from rest_framework import mixins, viewsets, permissions, serializers as s, status
from rest_framework.response import Response
from rest_framework.decorators import action

import azovhotels.models as m

from azovhotels.serializers.site.hotelier import HotelDetailSerializer, HotelListSerializer
from azovhotels.utils.base import ListView
from azovhotels.utils.permissions import HotelierPermissions





class HotelListView(ListView):
    model = m.Hotel
    serializer_class = HotelListSerializer
    atom_serializer_class = HotelDetailSerializer

    permission_classes = (HotelierPermissions,)

    paginate_by = 100
    

    def get_queryset(self):
        user = self.request.user

        if not user.is_anonymous():
            hotels = m.Hotel.objects.filter(user=user)
        else:
            hotels = m.Hotel.objects.all()  # here will be a redirect on login page

        return hotels


    def create(self, request, *args, **kwargs):
        if self.request.user.id:
            request.DATA['user'] = self.request.user.id

        return super(HotelListView, self).create(request, *args, **kwargs)


    @action(['GET', ])
    def set_active(self, request, pk=None):
        self.object = self.get_object_or_none()
        self.object.is_active = None
        self.object.save()

        return Response('', status=status.HTTP_200_OK)