# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render
from django.core.urlresolvers import reverse
from django.http import HttpResponseForbidden

from azovhotels.utils.context_processors import config_pages
import azovhotels.models as m

def root(request):
    """
    Hoteiler root view.
    """
    if not request.user.has_perm('azovhotels.change_hotel'):
        return redirect('auth.login')
    else:
        return render(request, 'site/hotelier.html', {
            'menus': config_pages(request)
        })



def register(request, messages=[]):
    """
    Hoteiler register view.
    """
    hotel_types = m.Label.objects.filter(type='hotel_type')
    hotel_cities = m.City.objects.all()

    return render(request, 'site/hotelier-register.html', {
        'messages': messages,
        'hotel_types': hotel_types,
        'hotel_cities': hotel_cities,
        'menus': config_pages(request)
    })