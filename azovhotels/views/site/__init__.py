# -*- coding: utf-8 -*-

from .hotels import HotelView
from .articles import ArticleView
from .pages import PageView
