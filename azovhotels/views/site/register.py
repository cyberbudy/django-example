# -*- coding: utf-8 -*-

from django.views.generic import View
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.contrib.auth.models import Group

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status, serializers as s

import azovhotels.serializers.admin as ads

import azovhotels.models as m
from azovhotels.models import User, Hotel, Label, Contact
from azovhotels.utils.text import slugify
from azovhotels.utils.mail import send_email


# @api_view(['POST', 'GET'])
@api_view(['POST'])
def tourist_register(request):
    """
    Registering user/tourist.
    """
    if not request.POST:
        return {}

    data = request.POST.copy()

    try:
        tourist = Group.objects.get(name__icontains='Туристы')
    except:
        tourist = Group(name='Туристы')
        tourist.save()

    user = user_register(data, tourist)


    MailTemplate = m.MailTemplate.filter(slug='tourist-register').all()

    if not MailTemplate:
        title = u'Регистрация туриста'
        msg = u'''Dear {{first_name}} {{last_name}},
            you've been registered at azovhotels.ua as a tourist
        '''
    else:
        title = MailTemplate.title
        msg = MailTemplate.msg

    send_email(request, title, msg, user)

    return Response({
        'status': 'ok',
        'data': user
    })


# @api_view(['POST'])
@api_view(['POST', 'GET'])
def hotelier_register(request):
    """
    Registering hotelier/hotel/contact.
    """
    if not request.POST:
        return {}

        hotel_types = Label.objects.filter(type='hotel_type')

        return Response({
            'hotel_types': [x for x in hotel_types.values()]
        })

    try:
        otelier = Group.objects.get(name__icontains='Отельеры')
    except:
        otelier = Group(name='Отельеры')
        otelier.save()

    req = request.POST.copy()

    if not req.get('title'):
        raise s.ValidationError('need hotel title')

    # user = User.objects.get(email=req['email'])

    user = user_register(req, otelier)    


    hotel_data = req
    hotel_data['slug'] = slugify(hotel_data.get('title'))
    hotel_data['user'] = user.id

    hotel = hotel_register(hotel_data, user)

    contact_data = req
    contact_data['hotel'] = hotel['id']

    contact = contact_register(contact_data, hotel)


    MailTemplate = m.MailTemplate.objects.filter(slug='hotelier-register').all()

    if not MailTemplate:
        title = u'Регистрация отельера'
        msg = u'''Здравствуйте {{first_name}} {{last_name}},
            спасибо за вашу регистрацию на azovhotels.ua.
        '''
    else:
        title = MailTemplate.title
        msg = MailTemplate.msg

    send_email(request, title, msg, user)


    return render(request, 'auth/register-success.html', {})
    



def contact_register(contact_data, hotel):
    contact_serializer = ads.ContactDetailSerializer(data=contact_data)

    if contact_serializer.is_valid():
        contact_serializer.save()

        return contact_serializer
    else:
        return {
            'status': 'contact bad request',
            'errors': contact_serializer.errors
        }



def hotel_register(hotel_data, user):
    hotels_count = Hotel.objects.filter(slug__startswith=hotel_data.get('slug')).count()

    if hotels_count != 0:
        hotel_data['slug'] = hotel_data.get('slug')+str(hotels_count)

    hotel_serializer = ads.HotelDetailSerializer(data=hotel_data)
    # print(hotel_data)
    if hotel_serializer.is_valid():
        hotel_serializer.save()

        return hotel_serializer.data
    else:
        raise s.ValidationError(hotel_serializer.errors)


def user_register(req, group):
    email = req.get('email')
    password = req.get('password')
    password_confirm = req.get('password_confirm')

    user_check = not (User.objects.filter(
        email=email))

    if not user_check:
        raise s.ValidationError('email already exist')

    password_check = password == password_confirm

    if not password_check: 
        raise s.ValidationError("passwords don't match" )

    user_serializer = ads.UserDetailSerializer(data=req)

    if len(password) < 3: 
        raise s.ValidationError('Password is too easy')

    if user_serializer.is_valid():
        user_serializer.save()
        group.user_set.add(User.objects.get(id=user_serializer.data['id']))

        return user_serializer.object
    else: 
        raise s.ValidationError(user_serializer.errors)
