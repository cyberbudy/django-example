# -*- coding: utf-8 -*-

from rest_framework import serializers as s, status
from rest_framework.response import Response
from rest_framework.decorators import api_view

import azovhotels.serializers.admin as ads
import azovhotels.models as m

from django.shortcuts import get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt

from azovhotels.utils.reservations import reservation_set_status, reservation_prepare
from azovhotels.serializers.site.reservations import HotelListSerializer, RoomTypeListSerializer
from azovhotels.serializers.admin import ReservationDetailSerializer

import datetime




@api_view(['GET', 'POST'])
@csrf_exempt
def reservation_user(request, *args, **kwargs):
    user = request.user
    data = request.DATA.copy()

    ################
    dt = {
        'hotel': 4,
        'room_type': 5,
        'starting_at': '2014-8-24',
        'ending_at': '2014-8-26'
    }
    #################
    data.update(dt)
    data['internal'] = True

    result = reservation_prepare(data)

    if not user.is_anonymous:
        result['first_name'] = user.first_name
        result['last_name'] = user.last_name
        result['email'] = user.email
    else:
        result['first_name'] = ''
        result['last_name'] = ''
        result['email'] = ''


    hotel = m.Hotel.objects\
        .filter(id=result['hotel'])\
        .extra(select={'mark':
            'votes_weight / votes_count'})[:1]

    room_type = m.RoomType.objects.filter(id=result['room_type'])[:1]

    print(data)
    if request.method == 'GET':
        return Response({
            'data': result,
            'hotel': HotelListSerializer(hotel).data,
            'room_type': RoomTypeListSerializer(room_type).data
            })

    if request.method == 'POST':
        return reservation_details(request, data=data, hotel=hotel, room_type=room_type)


@api_view(['GET'])
@csrf_exempt
def reservation_details(request, *args, **kwargs):
    # print(kwargs)
    hotel = kwargs.get('hotel')
    room_type = kwargs.get('room_type')
    data = kwargs.get('data')

    # data.update(request.DATA)
    data.update({'first_name':'asda', 'last_name': 'asdasd'})

    if request.method == 'GET':
        return Response({
            'data': result,
            'hotel': HotelListSerializer(hotel).data,
            'room_type': RoomTypeListSerializer(room_type).data
        })


@api_view(['POST'])
@csrf_exempt
def save_reservation(request, *args, **kwargs):
    if request.method == 'POST':
        data = request.POST

        serializer = ReservationDetailSerializer(data=data)

        if serializer.is_valid():
            serializer.save()

            # would be redirect on voucher
            return Response({            
                'data': serializer.data,
                'status': 'ok'
            })
        else:
            return Response({
                'data': serializer.errors,
                'status': 'bad request'
                })
