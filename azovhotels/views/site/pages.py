# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render
from django.shortcuts import get_object_or_404

from rest_framework.response import Response

from azovhotels.serializers.admin import PageListSerializer, PageDetailSerializer
from azovhotels.utils.context_processors import config_pages
from azovhotels.utils.base import BaseView
import azovhotels.models as m



class PageView(BaseView):
    queryset = m.Page.objects.all()
    serializer_class = PageListSerializer
    atom_serializer_class = PageDetailSerializer


    def list(self, request, *args, **kwargs):
        response = super(PageView, self).list(request, *args, **kwargs)
        response.data['menus'] = config_pages(request)

        return response


    def retrieve(self, request, id=None, slug=None):
        page = get_object_or_404(m.Page, id=id, slug=slug)
        serializer = PageListSerializer(page)

        response = render(request, 'site/page.html', {
            'page': serializer.data,
            'menus': config_pages(request)
        })

        return response
