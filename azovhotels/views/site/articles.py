# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render
from django.shortcuts import get_object_or_404

from rest_framework.response import Response

from azovhotels.serializers.admin import PageListSerializer, PageDetailSerializer
from azovhotels.utils.context_processors import config_pages
from azovhotels.utils.base import BaseView
import azovhotels.models as m



class ArticleView(BaseView):
    model = m.Page
    serializer_class = PageListSerializer
    atom_serializer_class = PageDetailSerializer




    def list(self, request, *args, **kwargs):
        articles = m.Page.objects\
            .filter(labels__slug=request.azovhotels_config['slug_articles'])

        self.object_list = self.filter_queryset(articles)

        # Default is to allow empty querysets.  This can be altered by setting
        # `.allow_empty = False`, to raise 404 errors on empty querysets.
        if not self.allow_empty and not self.object_list:
            warnings.warn(
                'The `allow_empty` parameter is due to be deprecated. '
                'To use `allow_empty=False` style behavior, You should override '
                '`get_queryset()` and explicitly raise a 404 on empty querysets.',
                PendingDeprecationWarning
            )
            class_name = self.__class__.__name__
            error_msg = self.empty_error % {'class_name': class_name}
            raise Http404(error_msg)

        # Switch between paginated or standard style responses
        page = self.paginate_queryset(self.object_list)
        if page is not None:
            serializer = self.get_pagination_serializer(page)
        else:
            serializer = self.get_serializer(self.object_list, many=True)

        response = serializer.data
        response['menus'] = config_pages(request)

        response = render(request, 'site/list-articles.html', response)

        return response


    def retrieve(self, request, id=None, slug=None):
        article = get_object_or_404(m.Page, id=id, slug=slug)
        serializer = PageListSerializer(article)

        response = render(request, 'site/article.html', {
            'article': serializer.data,
            'menus': config_pages(request)
        })

        return response
