# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics
import django_filters as df

from azovhotels.models import Action, Hotel
from azovhotels.serializers.admin import ActionListSerializer, ActionDetailSerializer
from azovhotels.utils.base import ListView



class ActionsFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=Hotel.objects.all())

    class Meta:
        model = Action
        fields = ('hotel',)



class ActionListView(ListView):
    queryset = Action.objects\
        .select_related('hotel')\
        .prefetch_related('labels')\
        .all()
    serializer_class = ActionListSerializer
    filter_class = ActionsFilter
    atom_serializer_class = ActionDetailSerializer

    ordering_fields = ('slug', 'title', 'id', 'created_at')
    search_fields = ('slug', 'title')
