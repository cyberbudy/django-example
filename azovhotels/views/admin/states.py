# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, mixins

from azovhotels.models import State
from azovhotels.serializers.admin import StateListSerializer, StateDetailSerializer
from azovhotels.utils.base import ListView




class StateListView(ListView):
    queryset = State.objects.all()
    serializer_class = StateListSerializer
    atom_serializer_class = StateDetailSerializer

    ordering_fields = ('slug', 'title', 'id', 'created_at')
    search_fields = ('slug', 'title')


