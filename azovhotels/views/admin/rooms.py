# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics

from azovhotels.models import Room
from azovhotels.serializers.admin import RoomListSerializer, RoomDetailSerializer
from azovhotels.utils.base import ListView


class RoomListView(ListView):
    queryset = Room.objects.all()
    serializer_class = RoomListSerializer
    atom_serializer_class = RoomDetailSerializer

