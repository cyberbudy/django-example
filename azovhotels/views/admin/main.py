# -*- coding: utf-8 -*-

from django.shortcuts import redirect, render
from django.core.urlresolvers import reverse
from django.http import HttpResponseForbidden


def root(request):
    """
    Admin root view.
    """
    if not request.user.is_superuser:
        return redirect('auth.login')
    else:
        return render(request, 'admin/index.html')