# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics
import django_filters as df

import azovhotels.filters as f
from azovhotels.models import User
from azovhotels.serializers.admin import UserListSerializer, UserDetailSerializer
from azovhotels.utils.base import ListView


class UserFilter(df.FilterSet):
    is_active = f.NullBooleanFilter(name='is_active')

    class Meta:
        model = User
        fields = ('is_active',)


class UserListView(ListView):
    queryset = User.objects\
        .select_related('group')\
        .prefetch_related('user_permissions')\
        .all()
    serializer_class = UserListSerializer
    filter_class = UserFilter
    atom_serializer_class = UserDetailSerializer

    ordering_fields = ('email', 'last_name', 'first_name', 'middle_name', 'id', 'created_at')
    search_fields = ('email', 'last_name', 'first_name', 'middle_name')
