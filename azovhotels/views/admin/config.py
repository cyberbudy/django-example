# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import Http404

from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets, serializers, generics, status, views

import django_filters as df

from azovhotels.serializers.admin import MetaListSerializer
from azovhotels.utils.middleware import get_config, get_meta
from azovhotels.utils.base import ListView

import azovhotels.models as m

from datetime import datetime



class ConfigView(ListView):
    model = m.Meta
    serializer_class = MetaListSerializer
    atom_serializer_class = MetaListSerializer

    def list(self, request, *args, **kwargs):
        """
        Getting all the configs.
        """

        config = request.azovhotels_config

        return Response({
            'config': config,
        })



    def create(self, request, *args, **kwargs):
        data = {'key': 'config', 'description': request.DATA.get('config', {})}

        try:
            obj = m.Meta.objects.get(key='config')
        except ObjectDoesNotExist:
            obj = None

        self.object = obj

        serializer = self.get_serializer(self.object, data=data)

        if not serializer.is_valid():
            return Response(serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

        try:
            self.pre_save(serializer.object)
        except ValidationError as err:
            return Response(err.message_dict,
                status=status.HTTP_400_BAD_REQUEST)

        if self.object is None:
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)

            return Response({
                    'config': serializer.data.get('description')
                }, status=status.HTTP_201_CREATED)

        self.object = serializer.save(force_update=True)
        self.post_save(self.object, created=False)

        return Response({
                'config': serializer.data.get('description')
            }, status=status.HTTP_200_OK)



class MetaDataView(ConfigView):
    def list(self, request, *args, **kwargs):
        """
        Getting all the meta configs.
        """

        metadata = request.azovhotels_metadata

        return Response({
            'metadata': metadata,
        })



    def create(self, request, *args, **kwargs):
        data = {'key': 'metadata', 'description': request.DATA.get('metadata', {})}

        try:
            obj = m.Meta.objects.get(key='metadata')
        except ObjectDoesNotExist:
            obj = None

        self.object = obj

        serializer = self.get_serializer(self.object, data=data)

        if not serializer.is_valid():
            return Response(serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

        try:
            self.pre_save(serializer.object)
        except ValidationError as err:
            return Response(err.message_dict,
                status=status.HTTP_400_BAD_REQUEST)

        if self.object is None:
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)

            return Response({
                    'metadata': serializer.data.get('description')
                }, status=status.HTTP_201_CREATED)

        self.object = serializer.save(force_update=True)
        self.post_save(self.object, created=False)

        return Response({
                'metadata': serializer.data.get('description')
            }, status=status.HTTP_200_OK)
