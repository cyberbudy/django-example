# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, status
from rest_framework.decorators import action
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.db.models import Q, Count, Min
import django_filters as df

from azovhotels.serializers.admin import (ReservationListSerializer,
    ReservationDetailSerializer, RoomTypeListSerializer)
from azovhotels.utils.base import ListView
from azovhotels.utils.reservations import dates_between_in_year, reservation_set_status, reservation_prepare

import azovhotels.models as m
import azovhotels.filters as f

import datetime



class ReservationFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=m.Hotel.objects.all())
    manager = df.ModelChoiceFilter(name='manager',
        queryset=get_user_model().objects.all())
    status = df.Filter(name="reservation_status")
    type_status = f.NullBooleanFilter(name='internal')

    class Meta:
        model = m.Reservation
        fields = ('hotel', 'manager', 'status', 'type_status')



class ReservationListView(ListView):
    queryset = m.Reservation.objects\
        .select_related('room_type')\
        .select_related('user')\
        .select_related('manager')\
        .order_by('-reservation_status', '-created_at')\
        .all()
    serializer_class = ReservationListSerializer
    filter_class = ReservationFilter
    atom_serializer_class = ReservationDetailSerializer

    ordering_fields = ('reservation_status', 'user', 'last_name', 'id', 'created_at')
    search_fields = ('reservation_status', 'last_name', 'first_name',
        'middle_name', 'price', 'phone', 'passport_series',
        'passport_code', 'passport_issued')


    def list(self, request, *args, **kwargs):
        response = super(ListView, self).list(request, *args, **kwargs)
        statuses = m.Reservation.RESERVATION_STATUS_CHOICES
        response.data['reservation_status_choices'] = [statuses[key] for key in statuses]

        return response


    def update(self, request, *args, **kwargs):
        reservation_set_status(request.DATA, request.DATA['reservation_status'])
        
        result = reservation_prepare(request.DATA)

        if isinstance(result, Response):
            return result

        return super(ReservationListView, self).update(request, *args, **kwargs)


    def create(self, request, *args, **kwargs):
        result = reservation_prepare(request.DATA)

        if isinstance(result, Response):
            return result

        return super(ReservationListView, self).create(request, *args, **kwargs)


    @action(methods=['GET'])
    def related(self, request, pk=None):
        serializer = self.get_serializer(data=request.DATA)
        data = serializer.get_related_data_all(request.DATA)

        room_types = m.RoomType.objects.all()
        hotel = request.GET.get('hotel', None)

        if(hotel):
            room_types = room_types.filter(hotel=hotel)

        data['room_types'] = RoomTypeListSerializer(room_types,
            many=True).data,

        return Response({
                'related_data_all': data,
            }, status=status.HTTP_200_OK)

