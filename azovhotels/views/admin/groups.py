# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics
from django.contrib.auth.models import Group, Permission

from azovhotels.serializers.admin import GroupListSerializer, GroupDetailSerializer
from azovhotels.utils.base import ListView



class GroupListView(ListView):
    queryset = Group.objects.all()
    serializer_class = GroupListSerializer
    atom_serializer_class = GroupDetailSerializer

    ordering_fields = ('name', 'id')
    search_fields = ('name',)

