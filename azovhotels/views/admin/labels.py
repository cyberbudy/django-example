# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, mixins, filters
import django_filters as df

from azovhotels.models import Label
from azovhotels.serializers.admin import LabelListSerializer, LabelDetailSerializer
from azovhotels.utils.base import ListView, BaseFilter


class LabelFilter(BaseFilter):

    class Meta:
        model = Label
        fields = ('type',)


class LabelListView(ListView):
    queryset = Label.objects.all()
    serializer_class = LabelListSerializer
    atom_serializer_class = LabelDetailSerializer
    filter_class = LabelFilter

    ordering_fields = ('slug', 'title', 'id', 'created_at')
    search_fields = ('slug', 'title',)
