# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics
import django_filters as df

from azovhotels.models import Contact, Hotel
from azovhotels.serializers.admin import ContactListSerializer, ContactDetailSerializer
from azovhotels.utils.base import ListView


class ContactFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=Hotel.objects.all())

    class Meta:
        model = Contact
        fields = ('hotel',)


class ContactListView(ListView):
    queryset = Contact.objects\
        .select_related('hotel')\
        .all()
    serializer_class = ContactListSerializer
    filter_class = ContactFilter
    atom_serializer_class = ContactDetailSerializer

    ordering_fields = ('position', 'last_name', 'id', 'created_at')
    search_fields = ('position', 'last_name', 'first_name',
        'middle_name', 'email', 'phone')

