# -*- coding: utf-8 -*-

import django_filters as df

from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework.decorators import action

import azovhotels.models as m
from azovhotels.serializers.admin import MailTemplateListSerializer,\
    MailTemplateDetailSerializer
from azovhotels.utils.base import ListView, BaseFilter
from azovhotels.utils.mail import replace_mail_tags, send_email


class MailTemplateFilter(BaseFilter):

    class Meta:
        model = m.MailTemplate


class MailTemplateListView(ListView):
    queryset = m.MailTemplate.objects.all()
    serializer_class = MailTemplateListSerializer
    atom_serializer_class = MailTemplateDetailSerializer
    filter_class = MailTemplateFilter

    ordering_fields = ('slug', 'title', 'id', 'created_at')
    search_fields = ('slug', 'title')


    @action(['POST', 'GET', ])
    def send(self, request, pk=None):
        pk = self.request.DATA.get('id')
        mail_template = get_object_or_404(m.MailTemplate, id=pk)

        ids = request.DATA.get('users')

        # ids = m.User.objects.values('id').all()
        user_list = m.User.objects.filter(id__in=ids)
        # print(user_list)
        # print(2342)

        # MailSend.delay(mail_template, user_list, r, default_token_generator)
        
        # for i in range(150):
        for user in user_list:
            msg = mail_template.description
            
            send_email(request, mail_template.title, msg, user)

        return self.retrieve(request, pk)
        return Response({'status':'ok'})




