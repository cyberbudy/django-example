# -*- coding: utf-8 -*-

import os

from django.conf import settings
from rest_framework import viewsets, status
from rest_framework.response import Response
import django_filters as df

from azovhotels.models import File, RoomType, Hotel
from azovhotels.serializers.admin import FileListSerializer, FileDetailSerializer
from azovhotels.utils.text import uid
from azovhotels.utils.base import ListView

from PIL import Image, ImageChops as Chops, ImageOps
import os
from sorl.thumbnail import get_thumbnail




class FileFilter(df.FilterSet):
    room_type = df.ModelChoiceFilter(name="roomtype", queryset=RoomType.objects.all())
    hotel = df.ModelChoiceFilter(name="hotel", queryset=Hotel.objects.all())

    class Meta:
        model = File



class FileListView(ListView):
    model = File
    serializer_class = FileListSerializer
    filter_class = FileFilter
    atom_serializer_class = FileDetailSerializer



    def create(self, request, *args, **kwargs):
        return create_file(self, request, *args, **kwargs)





def create_file(self, request, *args, **kwargs):
    im = Image.open(request.FILES['file'])

    data = {
        'title': request.FILES['file'].name,
        'slug': uid(),
        'type': os.path.splitext(request.FILES['file'].name)[1][1:],
    }

    request.FILES['file']._set_name(data['slug'] + '.' + data['type'])

    serializer = self.get_serializer_class()(data=data, files=request.FILES)

    if serializer.is_valid():
        serializer.save()

        for alias in settings.IMAGE_SIZES:
            size = settings.IMAGE_SIZES[alias]
            new_im = Chops.duplicate(im)

            width, height = new_im.size

            img = ImageOps.fit(new_im, size)
            img.thumbnail(size, Image.ANTIALIAS)

            img.save(os.path.join(settings.MEDIA_ROOT,
                settings.IMAGE_SIZES_FORMAT.format(size[0], size[1],
                    request.FILES['file'].name)))

        return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)