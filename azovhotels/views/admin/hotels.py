# -*- coding: utf-8 -*-

from django import forms
from rest_framework import viewsets, serializers, generics
import django_filters as df

import azovhotels.filters as f
from azovhotels.models import Hotel
from azovhotels.serializers.admin import HotelListSerializer, HotelDetailSerializer
from azovhotels.utils.base import ListView


class HotelFilter(df.FilterSet):
    is_active = f.NullBooleanFilter(name='is_active')

    class Meta:
        model = Hotel
        fields = ('is_active',)


class HotelListView(ListView):
    queryset = Hotel.objects\
        .select_related('city')\
        .select_related('user')\
        .prefetch_related('files')\
        .prefetch_related('labels')\
        .all()
    serializer_class = HotelListSerializer
    atom_serializer_class = HotelDetailSerializer
    filter_class = HotelFilter

    ordering_fields = ('slug', 'title', 'id', 'created_at')
    search_fields = ('slug', 'title', 'address', 'site', 'phone')

