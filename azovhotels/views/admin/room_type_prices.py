# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, status
from rest_framework.response import Response

import django_filters as df

from django.shortcuts import get_object_or_404

from azovhotels.models import RoomTypePrice, RoomType
from azovhotels.serializers.admin import (RoomTypePriceListSerializer,
    RoomTypePriceDetailSerializer, RoomTypeRateListSerializer)
from azovhotels.utils.base import ListView
from azovhotels.utils.text import date_range

import azovhotels.models as m

from datetime import datetime




class RoomTypePriceFilter(df.FilterSet):
    roomtype = df.ModelChoiceFilter(name='room_type',
        query=RoomType.objects.all())

    class Meta:
        model = RoomTypePrice
        fields = ('roomtype',)



class RoomTypePriceListView(ListView):
    queryset = RoomTypePrice.objects\
        .select_related('room_type')\
        .select_related('rate')\
        .all()
    serializer_class = RoomTypePriceListSerializer
    atom_serializer_class = RoomTypePriceDetailSerializer
    # filter_class = RoomTypePriceFilter

    ordering_fields = ('price', 'id', 'created_at')
    search_fields = ('price',)


    def list(self, request, *args, **kwargs):
        month = int(request.GET.get('month', datetime.now().month))
        year = int(request.GET.get('year', datetime.now().year))

        room_type = request.GET.get('roomtype', None)

        if room_type != None:
            roomtype = RoomType.objects.get(id=int(room_type))
        else:
            roomtype = {}

        self.object_list = self.filter_queryset(
            RoomTypePrice.objects.filter(date__year=year,
                date__month=month, room_type=room_type)
        )

        if not self.allow_empty and not self.object_list:
            warnings.warn(
                'The `allow_empty` parameter is due to be deprecated. '
                'To use `allow_empty=False` style behavior, You should override '
                '`get_queryset()` and explicitly raise a 404 on empty querysets.',
                PendingDeprecationWarning
            )
            class_name = self.__class__.__name__
            error_msg = self.empty_error % {'class_name': class_name}
            raise Http404(error_msg)

        serializer = self.get_serializer(self.object_list, many=True)

        rates = []
        default = {}
        # print(room_type)
        rates = RoomTypeRateListSerializer(m.RoomTypeRate.objects\
            .filter(room_type=room_type)\
            .order_by('created_at')\
            .all(), many=True).data

        if len(rates) > 0 :
            default = rates[0]
            default['available_rooms'] = getattr(roomtype, 'rooms_available', 0)

        return Response({
                'pagination': {
                    'year': year,
                    'month': month,
                },
                'rates': rates,
                'default': default,
                'objects': serializer.data,
            })



    def create(self, request, *args, **kwargs):
        request_data = request.DATA
        data = []
        buf = {}
        model_data = []
        rate = request_data.get('rate', None)
        room_type = request_data.get('room_type', None)
        available_rooms = request_data.get('available_rooms', 0)

        date_from = request_data.get('date_from', None)
        date_to = request_data.get('date_to', None)


        if not date_from or not date_to:
            return Response({
                    'date_from': ['need date'],
                }, status.HTTP_400_BAD_REQUEST)

        try:
            date_from = datetime.strptime(date_from, '%Y-%m-%d')
            date_to = datetime.strptime(date_to, '%Y-%m-%d')
        except:
            return Response({
                    'date_from': ['invalid date format'],
                }, status.HTTP_400_BAD_REQUEST)

        Price = get_object_or_404(m.RoomTypeRate, id=rate)
        RoomType = get_object_or_404(m.RoomType, id=room_type)
        Rate = get_object_or_404(m.RoomTypeRate, id=rate)

        for i in date_range(date_from, date_to):
            buf = {
                'date': i,
                'price': Price.price,
                'available_rooms': available_rooms,
                'room_type': room_type,
                'rate': rate
            }

            model_data.append(
                m.RoomTypePrice(
                    date=i,
                    price=Price.price,
                    available_rooms=available_rooms,
                    room_type=RoomType,
                    rate=Rate
                )
            )

            data.append(buf)

        m.RoomTypePrice.objects.filter(room_type=room_type, date__range=(date_from, date_to)).delete()
        serializer = self.get_serializer(data=data[0])

        if serializer.is_valid():
            objects = m.RoomTypePrice.objects.bulk_create(model_data)
            
            return Response({}, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
