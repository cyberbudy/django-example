# -*- coding: utf-8 -*-

from .cities import CityListView
from .hotels import HotelListView
from .users import UserListView
from .files import FileListView
from .labels import LabelListView
from .pages import PageListView
from .contacts import ContactListView
from .comments import CommentListView
from .actions import ActionListView
from .room_types import RoomTypeListView
from .rooms import RoomListView
from .room_type_rates import RoomTypeRateListView
from .room_type_prices import RoomTypePriceListView
from .reservations import ReservationListView
from .groups import GroupListView
from .states import StateListView
from .permissions import PermissionListView

from .config import ConfigView, MetaDataView
from .mail_templates import MailTemplateListView
