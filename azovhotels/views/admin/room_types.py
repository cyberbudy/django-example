# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, status
from rest_framework.response import Response
import django_filters as df

from azovhotels.models import RoomType, Hotel
from azovhotels.serializers.admin import RoomTypeListSerializer, RoomTypeDetailSerializer, RoomTypeRateDetailSerializer
from azovhotels.utils.base import ListView


class RoomTypeFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=Hotel.objects.all())

    class Meta:
        model = RoomType
        fields = ('hotel',)


class RoomTypeListView(ListView):
    queryset = RoomType.objects\
        .select_related('hotel')\
        .select_related('view_file')\
        .prefetch_related('files')\
        .prefetch_related('labels')\
        .all()
    serializer_class = RoomTypeListSerializer
    filter_class = RoomTypeFilter
    atom_serializer_class = RoomTypeDetailSerializer

    ordering_fields = ('slug', 'title', 'beds', 'rooms_total', 'id', 'created_at')
    search_fields = ('slug', 'title')


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)

        if serializer.is_valid():
            self.pre_save(serializer.object)
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)
            headers = self.get_success_headers(serializer.data)

            rate_serializer = RoomTypeRateDetailSerializer(data={
                'price': serializer.data.get('price'),
                'room_type': serializer.data.get('id')
                })

            if not rate_serializer.is_valid():
                return Response(rate_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            rate_serializer.save()


            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
