# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, mixins
from rest_framework.response import Response

from azovhotels.models import Page
from azovhotels.serializers.admin import PageListSerializer, PageDetailSerializer
from azovhotels.utils.base import ListView



class PageListView(ListView):
    queryset = Page.objects\
        .prefetch_related('labels')\
        .all()
    serializer_class = PageListSerializer
    atom_serializer_class = PageDetailSerializer

    ordering_fields = ('slug', 'title', 'id', 'created_at')
    search_fields = ('slug', 'title')

