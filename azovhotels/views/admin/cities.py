# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, mixins

from azovhotels.models import City
from azovhotels.serializers.admin import CityListSerializer, CityDetailSerializer
from azovhotels.utils.base import ListView




class CityListView(ListView):
    queryset = City.objects.all()
    serializer_class = CityListSerializer
    atom_serializer_class = CityDetailSerializer

    ordering_fields = ('slug', 'title', 'id', 'created_at')
    search_fields = ('slug', 'title')

