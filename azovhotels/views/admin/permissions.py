# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics
from django.contrib.auth.models import Permission

from azovhotels.serializers.admin import PermissionListSerializer, PermissionDetailSerializer
from azovhotels.utils.base import ListView



class PermissionListView(ListView):
    queryset = Permission.objects.all()
    serializer_class = PermissionListSerializer
    atom_serializer_class = PermissionDetailSerializer

