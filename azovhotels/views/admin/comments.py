# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics
import django_filters as df

from azovhotels.models import Comment, Hotel
from azovhotels.serializers.admin import CommentListSerializer, CommentDetailSerializer
from azovhotels.utils.base import ListView



class CommentFilter(df.FilterSet):
    hotel = df.ModelChoiceFilter(name="hotel", queryset=Hotel.objects.all())

    class Meta:
        model = Comment
        fields = ('hotel',)



class CommentListView(ListView):
    queryset = Comment.objects\
        .select_related('hotel')\
        .select_related('user')\
        .all()
    serializer_class = CommentListSerializer
    filter_class = CommentFilter
    atom_serializer_class = CommentDetailSerializer

    ordering_fields = ('vote_mark', 'user', 'id', 'created_at')
    search_fields = ('content',)


    def create(self, request, *args, **kwargs):
        cr = request.DATA.get('vote_criteria', [])

        if len(cr) > 0:
            request.DATA['vote_mark'] = float(int(
                float(sum([int(x) for x in cr])) / len(cr) * 100)) / 100.0

        return super(CommentListView, self)\
            .create(request, *args, **kwargs)


    def update(self, request, *args, **kwargs):
        cr = request.DATA.get('vote_criteria', [])

        if len(cr) > 0:
            request.DATA['vote_mark'] = float(int(
                float(sum([int(x) for x in cr])) / len(cr) * 100)) / 100.0

        return super(CommentListView, self)\
            .update(request, *args, **kwargs)

