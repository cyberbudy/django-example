# -*- coding: utf-8 -*-

from rest_framework import viewsets, serializers, generics, status
from rest_framework.response import Response
import django_filters as df

from azovhotels.models import RoomTypeRate, RoomType, RoomTypePrice
from azovhotels.serializers.admin import RoomTypeRateListSerializer, RoomTypeRateDetailSerializer
from azovhotels.utils.base import ListView



class RooomTypeRateFilter(df.FilterSet):
    roomtype = df.ModelChoiceFilter(name='room_type', queryset=RoomType.objects.all())

    class Meta:
        model = RoomTypeRate
        fields = ('roomtype', )



class RoomTypeRateListView(ListView):
    queryset = RoomTypeRate.objects\
        .select_related('room_type')\
        .all()
    serializer_class = RoomTypeRateListSerializer
    filter_class = RooomTypeRateFilter
    atom_serializer_class = RoomTypeRateDetailSerializer

    ordering_fields = ('price', 'id', 'created_at')
    search_fields = ('price', 'about')


    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        self.object = self.get_object_or_none()

        serializer = self.get_serializer(self.object, data=request.DATA,
                                         files=request.FILES, partial=partial)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        try:
            self.pre_save(serializer.object)
        except ValidationError as err:
            # full_clean on model instance may be called in pre_save,
            # so we have to handle eventual errors.
            return Response(err.message_dict, status=status.HTTP_400_BAD_REQUEST)

        if self.object is None:
            self.object = serializer.save(force_insert=True)
            self.post_save(self.object, created=True)

            return Response(serializer.data, status=status.HTTP_201_CREATED)

        self.object = serializer.save(force_update=True)
        self.post_save(self.object, created=False)

        RoomTypePrice.objects\
                .filter(rate=serializer.data['id'])\
                .update(price=serializer.data['price'])

        return Response(serializer.data, status=status.HTTP_200_OK)
