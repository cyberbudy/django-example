# -*- coding: utf-8 -*-

from rest_framework import pagination
from rest_framework import serializers
from rest_framework.templatetags.rest_framework import replace_query_param
from django.conf import settings

from django.core.paginator import Paginator

# 
# Pagination Fields
# 

class FirstPageLink(serializers.Field):
    """
    Field that returns a link to the first page in paginated results.
    """
    page_field = 'page'

    def to_native(self, value):
        if value.number == 1:
            return None
        page = None
        request = self.context.get('request')
        url = request and request.build_absolute_uri() or ''
        return replace_query_param(url, self.page_field, page)



class LastPageLink(serializers.Field):
    """
    Field that returns a link to the last page in paginated results.
    """
    page_field = 'page'

    def to_native(self, value):
        if value.number == value.paginator.num_pages:
            return None
        page = value.paginator.num_pages
        request = self.context.get('request')
        url = request and request.build_absolute_uri() or ''
        return replace_query_param(url, self.page_field, page)



class FirstPageNum(serializers.Field):
    page_field = 'page'

    def to_native(self, value):
        if value.number == 1:
            return None
        else:
            return 1


class NextPageNum(serializers.Field):
    page_field = 'page'

    def to_native(self, value):
        if not value.has_next():
            return None
        else:
            return value.next_page_number()


class PreviousPageNum(serializers.Field):
    page_field = 'page'

    def to_native(self, value):
        if not value.has_previous():
            return None
        else:
            return value.previous_page_number()


class LastPageNum(serializers.Field):
    page_field = 'page'

    def to_native(self, value):
        if value.number == value.paginator.num_pages:
            return None
        else:
            return value.paginator.num_pages



# 
# Pagination class
# 

class LinksSerializer(serializers.Serializer):
    first_link = FirstPageLink(source='*')
    next_link = pagination.NextPageField(source='*')
    prev_link = pagination.PreviousPageField(source='*')
    last_link = LastPageLink(source='*')



class PositionsSerializer(serializers.Serializer):
    first = FirstPageNum(source='*')
    next = NextPageNum(source='*')
    prev = PreviousPageNum(source='*')
    last = LastPageNum(source='*')



class PaginationListSerializer(serializers.Serializer):
    links = LinksSerializer(source='*')
    positions = PositionsSerializer(source='*')
    pages = serializers.Field(source='paginator.num_pages')
    count = serializers.Field(source='paginator.count')
    page = serializers.Field(source='number')



class ModelPaginationSerializer(pagination.BasePaginationSerializer):
    pagination = PaginationListSerializer(source='*')

    results_field = 'objects'



def paginate(obj, pg = None, page_by=None):
    if not page_by:
        page_by = settings.REST_FRAMEWORK['PAGINATE_BY']

    paginator = Paginator(obj, page_by)

    if pg == None or pg < 0 or pg > paginator.num_pages:
        return paginator.page(1)
    else:
        return paginator.page(pg)