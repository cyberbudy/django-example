# -*- coding: utf-8 -*-

from django.conf import settings

from rest_framework import status, fields
from rest_framework import serializers as s, generics, viewsets, mixins, filters
from rest_framework.decorators import action, link
from rest_framework.response import Response
from rest_framework.settings import import_from_string
from rest_framework.compat import parse_datetime

import django_filters as df

from pagination import ModelPaginationSerializer

from BeautifulSoup import BeautifulSoup
import json



class DateField(fields.DateField):
    def from_native(self, value):
        parsed = None

        try:
            parsed = parse_datetime(value)
        except (ValueError, TypeError):
            pass

        if(parsed):
            value = parsed

        return super(DateField, self).from_native(value)



# Base Serializers

class ListSerializer(s.ModelSerializer):
    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(ListSerializer, self).__init__(*args, **kwargs)

        if fields:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)



class DetailSerializer(s.ModelSerializer):
    related_data = s.SerializerMethodField('get_related_data')
    related_data_all = s.SerializerMethodField('get_related_data_all')    


    class Meta:
        fields = ('related_data', 'related_data_all')
        
    def get_related_data(self, obj):
        return { 
        }


    def get_related_data_all(self, obj):
        return {
        }



# Base Filters

class BaseFilter(df.FilterSet):
    class Meta:
        pass


# Base Views


class BaseView(mixins.ListModelMixin,
                mixins.CreateModelMixin,
                mixins.RetrieveModelMixin,
                mixins.UpdateModelMixin,
                mixins.DestroyModelMixin,
                viewsets.GenericViewSet):
    pagination_serializer_class = ModelPaginationSerializer

    filter_backends = (filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter)


    def get_serializer(self, instance=None, data=None,
                       files=None, many=False, partial=False):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        
        if self.action == 'list':
            serializer_class = self.get_serializer_class()
        elif self.action:
            serializer_class = self.atom_serializer_class
        else:
            serializer_class = self.atom_serializer_class
            
        context = self.get_serializer_context()
        return serializer_class(instance, data=data, files=files,
                                many=many, partial=partial, context=context)


    def update(self, request, *args, **kwargs):
        response = super(BaseView, self).update(request, *args, **kwargs)

        if response.status_code == status.HTTP_200_OK:
            return self.retrieve(request, *args, **kwargs)

        return response

    def create(self, request, *args, **kwargs):
        slug = request.DATA.get('slug', None)

        if slug:
            model = self.get_queryset().model
            count = model.objects\
                .filter(slug__istartswith=slug)\
                .count()

            if count > 0:
                slug += str(count)

            request.DATA['slug'] = slug

        return super(BaseView, self).create(request, *args, **kwargs)



class ListView(BaseView):
    def response_embed_data(self, request, response, *args, **kwargs):
        for embed in settings.AZOVHOTELS_REST_VIEW_EMBEDDERS:
            embed_func = import_from_string(embed,
                'AZOVHOTELS_REST_VIEW_EMBEDDERS')
            embed_func(self, request, response, *args, **kwargs)

        return response


    def list(self, request, *args, **kwargs):
        response = super(ListView, self).list(request, *args, **kwargs) 
        return self.response_embed_data(request, response, *args, **kwargs)


    def retrieve(self, request, *args, **kwargs):
        response = super(ListView, self).retrieve(request, *args, **kwargs) 
        return self.response_embed_data(request, response, *args, **kwargs)


    @action(methods=['GET'])
    def related(self, request, pk=None):
        serializer = self.get_serializer(data=request.DATA)

        return Response({
                'related_data_all': serializer\
                    .get_related_data_all(request.DATA),
            }, status=status.HTTP_200_OK)


    # def get(self, request, *args, **kwargs):
    #     return self.list(request, *args, **kwargs)

    # def post(self, request, *args, **kwargs):
    #     return self.create(request, *args, **kwargs)


# Serializer Fields 

class JSONField(s.WritableField):
    
    def from_native(self, obj):
        return json.dumps(obj)
    
    def to_native(self, value):
        return json.loads(value)


class HTMLField(s.WritableField):
    
    def to_native(self, obj):
        if not obj:
            return obj
        soup = BeautifulSoup(obj)

        for tag in soup.findAll(True):
            if tag.name not in VALID_TAGS:
                tag.hidden = True
                # tag.extract()

        return soup.renderContents()


    
    # def from_native(self, value):
    #     return value


VALID_TAGS = [
    'p', 'b', 'i', 'em', 'strong', 'a', 'blockquote',
    'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
    'table', 'caption', 'tbody', 'tr', 'td', 'th',
    'ul', 'ol', 'li',
    ]
