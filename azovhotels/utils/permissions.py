# -*- coding: utf-8 -*-

from rest_framework import permissions

# here will be custom permissions class




class TouristPermissions(permissions.BasePermission):
    def has_permission(self, request, view, obj=None):
        user_is_tourist = check_user_group(request.user, u'tourist')
        # user_can = request.user.has_perms('azovhotels.change_')
        
        return (request.user and request.user.is_authenticated() and (user_is_tourist))


    def has_object_permissions(self, request, view, obj):
        user_is_tourist = check_user_group(request.user, u'tourist')
        # user_can = request.user.has_perms('azovhotels.change_hotel')
        
        return (request.user and request.user.is_authenticated() and user_is_tourist)
        


class HotelierPermissions(permissions.BasePermission):
    def has_permission(self, request, view, obj=None):
        user_is_hotelier = check_user_group(request.user, u'hotelier')
        user_can = request.user.has_perm('azovhotels.change_hotel')
        
        return (request.user and request.user.is_authenticated() and (user_is_hotelier or user_can))
        # return request.user and request.user.is_authenticated() 


    def has_object_permissions(self, request, view, obj):
        user_is_hotelier = check_user_group(request.user, u'hotelier')
        user_can = request.user.has_perm('azovhotels.change_hotel')
        
        return (request.user and request.user.is_authenticated() and (user_is_hotelier or user_can))
        # return (request.user and request.user.is_authenticated())



class AdminPermission(permissions.BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated() and request.user.is_superuser
        


def check_user_group(user, group):
    for i in user.groups.values_list('name'):
        if i[0] == group:
            return True

    return False