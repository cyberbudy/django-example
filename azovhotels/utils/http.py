# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.utils import simplejson

from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers


class JSONEncoder(DjangoJSONEncoder):
    """
    JSONEncoder that doesn't thowing errors.
    """

    def default(self, o):
        return super(JSONEncoder, self).default(o)

        

class JsonResponse(HttpResponse):
    """
    An HTTP response class that consumes data to be serialized to JSON.

    :param data: Data to be dumped into json. By default only ``dict`` objects
        are allowed to be passed due to a security flaw before EcmaScript 5. See
        the ``safe`` parameter for more information.
    :param encoder: Should be an json encoder class. Defaults to
        ``azovhotels.utils.serializers.JSONEncoder``.
    :param safe: Controls if only ``dict`` objects may be serialized. Defaults
        to ``True``.
    """

    def __init__(self, data, encoder=JSONEncoder, safe=True, **kwargs):
        if safe and not isinstance(data, dict):
            raise TypeError('In order to allow non-dict objects to be '
                'serialized set the safe parameter to False')
            
        kwargs.setdefault('content_type', 'application/json')
        data = simplejson.dumps(data, cls=encoder, sort_keys=True, indent=4)

        super(JsonResponse, self).__init__(content=data, **kwargs)