# -*- coding: utf-8 -*-

from django.conf import settings

from .middleware import get_config


# Config context_processor
def config(request):
    if(hasattr(request, 'azovhotels_config')):
        return {'config': request.azovhotels_config}
    else:
        return {'config': get_config()}



from azovhotels.models import Page
from azovhotels.serializers.admin import PageListSerializer

def config_pages(request):
    if hasattr(request, 'azovhotels_config'):
        fields = [
            'slug_azovhotels',
            'slug_confidential',
            'slug_public_offer',
            'slug_fuq',
            'slug_carrier',
            'slug_conditions',
            'slug_help',
            'slug_hoteliers',
            'slug_tourists',
            'slug_advertisement',
        ]
        slugs = []
        relation = {}

        for i in fields:
            slug = request.azovhotels_config.get(i, None)

            if slug:
                if not relation.get(slug, False):
                    relation[slug] = []

                relation[slug].append(i)
                slugs.append(slug)

        pages = Page.objects\
            .defer('template', 'content')\
            .filter(slug__in=slugs).all().values()

        data = {}
        for i in pages:
            for j in relation[i['slug']]:
                data[j] = i

        return data

    return {}