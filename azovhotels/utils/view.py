# -*- coding: utf-8 -*-

from azovhotels.utils.http import JsonResponse


def json(fun):
    """
    Decorator for view function.
    Converts wrapped view output to the json. And returns json response.
    """
    def wrapper(request, *args, **kwargs):
        return JsonResponse(fun(request, *args, **kwargs), safe=False)

    return wrapper