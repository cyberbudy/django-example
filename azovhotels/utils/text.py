# -*- coding: utf-8 -*-

import datetime
import uuid
import re

from django.utils import text


def uid():
    """
    Cerates unique id(string).
    """
    return '{:x}-{:s}'.format(int(datetime.datetime.utcnow()
        .strftime('%Y%m%d%H%M%S%f')), uuid.uuid4().hex)



def date_range(start_date, end_date):
    for n in range(int ((end_date - start_date).days) + 1):
        yield (start_date + datetime.timedelta(n)).date()



def slugify(string):
    d = {
    u'а': 'a',
    u'б': 'b',
    u'в': 'v',
    u'г': 'g',
    u'д': 'd',
    u'й': 'y',
    u'ц': 'ts',
    u'у': 'u',
    u'к': 'k',
    u'е': 'e',
    u'н': 'n',
    u'щ': 'shch',
    u'ш': 'sh',
    u'з': 'z',
    u'х': 'kh',
    u'ф': 'f',
    u'ы': 'y',
    u'п': 'p',
    u'р': 'r',
    u'о': 'o',
    u'л': 'l',
    u'э': 'e',
    u'ж': 'zh',
    u'я': 'ya',
    u'ч': 'ch',
    u'с': 's',
    u'м': 'm',
    u'и': 'i',
    u'т': 't',
    u'ю': 'yu',
    ' ': '-',
    u'ъ': '',
    u'ь': '',

    u'А': 'a',
    u'Б': 'b',
    u'В': 'v',
    u'Г': 'g',
    u'Д': 'd',
    u'Й': 'y',
    u'Ц': 'ts',
    u'У': 'u',
    u'К': 'k',
    u'Е': 'e',
    u'Н': 'n',
    u'Щ': 'zhch',
    u'Ш': 'sh',
    u'З': 'z',
    u'Х': 'kh',
    u'Ф': 'f',
    u'Ы': 'y',
    u'П': 'p',
    u'Р': 'r',
    u'С': 'o',
    u'Л': 'l',
    u'Э': 'e',
    u'Ж': 'zh',
    u'Я': 'ya',
    u'Ч': 'ch',
    u'С': 's',
    u'М': 'm',
    u'И': 'i',
    u'Т': 't',
    u'Ю': 'yu',
    # ' ': '-',
    u'Ъ': '',
    u'Ю': '',
    u'Ь': '',
    '-': '-',
    u'О': 'o',
    u',': ''
    }  

    for letter in xrange(ord('a'), ord('z')):
        d[chr(letter).upper()] = chr(letter)
        d[chr(letter)] = chr(letter)

    for i in range(10):
        d[str(i)] = str(i)

    res = ''
    string = string.strip()

    for i in string:
        try:
            res += d[i]
        except:
            pass

    res = re.sub("\ +", "-", res)
    res = re.sub('\-+', '-', res)

    return res
