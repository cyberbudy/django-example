# -*- coding: utf-8 -*-

from rest_framework import serializers as s

import azovhotels.models as m
import azovhotels.serializers.admin as ads


class HotelSerializer(s.ModelSerializer):
    related_data = s.SerializerMethodField('get_related_data')

    class Meta:
        model = m.Hotel
        exclude = ('city', 'user', 'labels', 'files')
        include = ('related_data', )


    def get_related_data(self, obj):
        return {
            'view_file': ads.FileListSerializer(obj.view_file).data,
        }
