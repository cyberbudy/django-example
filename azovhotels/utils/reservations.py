# -*- coding: utf-8 -*-


from rest_framework import status
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.db.models import Q, Count, Min

import azovhotels.models as m

import datetime



def reservation_set_status(data, status):
    statuses = m.Reservation.RESERVATION_STATUS_CHOICES

    if statuses.has_key(status):
        data['reservation_status'] = status
        triggers = m.Reservation.RESERVATION_STATUS_TRIGGERS[status]

        for i in triggers:
            data[i] = triggers[i]

    return data



def reservation_prepare(data):
    hotel_id = data.get('hotel', None)
    room_type_id = data.get('room_type', None)
    internal = data.get('internal', None)

    if hotel_id and room_type_id and (internal != None):
        hotel = get_object_or_404(m.Hotel, id=hotel_id)
        room_type = get_object_or_404(m.RoomType, id=room_type_id)
        date_start = data.get('starting_at', None)
        date_end = data.get('ending_at', None)

        if(date_start and date_end):
            date_start = datetime.datetime.strptime(date_start[:10],
                '%Y-%m-%d').date()
            date_end = datetime.datetime.strptime(date_end[:10],
                '%Y-%m-%d').date()

            request_dates_error = Response({
                    'starting_at, ending_at': [
                        'Бронирование в эти даты невозможно.',
                    ],
                }, status=status.HTTP_400_BAD_REQUEST)

            if not dates_between_in_year(hotel.working_from, date_start,
                hotel.working_till, date_end):
                return request_dates_error
                
            reservation_days = (date_end-date_start).days
            if reservation_days < 1:
                return request_dates_error

            ''' retieve reservations where current date range crosses reservation date range and 
                    date_start != reservation.date_end or date_end != reservation.date_start for given room_type and only not cancelled
            '''
            reservations = m.Reservation.objects\
                .filter(
                    ((Q(starting_at__range=[date_start, date_end])|
                        Q(ending_at__range=[date_start, date_end])|
                    ((Q(starting_at__lte=date_start)&Q(ending_at__gte=date_start))|
                        (Q(starting_at__lte=date_end)&Q(ending_at__gte=date_end))))&
                    ~(Q(starting_at=date_end) | Q(ending_at=date_start))
                    )
                    &
                    Q(room_type=room_type_id),
                    Q(cancelled=False))\

            # retrice count and min price from calendat without last day
            min_available_rooms = m.RoomTypePrice.objects\
                .filter(Q(date__range=[date_start, date_end-datetime.timedelta(days=1)]), Q(room_type=room_type))\
                .aggregate(count=Count('available_rooms'), min=Min('available_rooms'))

            rooms_available = 0

            # retrive min available rooms: if not all dates in range in calendar filled -> check default av_rooms
            if min_available_rooms['min']:
                if min_available_rooms['count'] < reservation_days:
                    rooms_available = min(min_available_rooms['min'], room_type.rooms_available)
                elif min_available_rooms['count'] == reservation_days:
                    rooms_available = min_available_rooms['min']
            else:
                rooms_available = room_type.rooms_available

            # all reservations 
            reservations_all = reservations.all().values('room_index').annotate(Count('room_index'))
            # only internal reservations
            reservations_internal = reservations.filter(internal=True)\
                .all().values('room_index').annotate(Count('room_index'))

            # print(date_start, date_end, reservations_all, reservations_internal, room_type_id, room_type.rooms_total, rooms_available)
            # If all rooms are reserved, and internal reservations less then available rooms.
            if(len(reservations_all) >= room_type.rooms_total or
                (len(reservations_internal) >= rooms_available if internal else False)):
                return request_dates_error

            rooms_indexes = [x['room_index'] for x in reservations_internal]
            room_index = None

            # print(rooms_available, rooms_indexes)

            # retrieve available room
            for i in xrange(rooms_available):
                if i not in rooms_indexes:
                    room_index = i
                    break

            # retrieve reservation price
            query = m.RoomType.objects\
                .filter(id=room_type_id)\
                .extra(select={'dates_price':
                    '''SELECT CEIL(SUM(d.price)) from azovhotels_roomtypeprice as d
                    where d.room_type_id=%s and d.date BETWEEN '%s' and '%s'
                    '''%(room_type_id, date_start, date_end-datetime.timedelta(days=1))})\
                .extra(select={'def_price':
                    '''SELECT CEIL(price*(%s-(SELECT COUNT(d.id) from azovhotels_roomtypeprice as d
                    where d.room_type_id=%s and d.date BETWEEN '%s' and '%s')))
                    '''%(reservation_days, room_type_id, date_start, date_end-datetime.timedelta(days=1))
                    })

            reservation_prices = query[0]

            if not reservation_prices.dates_price:
                reservation_prices.dates_price = 0
            if not reservation_prices.def_price:
                reservation_prices.def_price = 0
            
            reservation_sum = round(float(reservation_prices.def_price\
                + reservation_prices.dates_price), 2)

            if not hotel.reservation_prepayment_type:
                prepayment_sum = round(float(reservation_sum / reservation_days)\
                    * float(hotel.reservation_prepayment_daily), 2)
            else:
                prepayment_sum = round((float(reservation_sum)\
                    * float(hotel.reservation_prepayment_percent)) / 100.0, 2)

            if prepayment_sum == 0:
                reservation_set_status(data, 'FD')
            else:
                reservation_set_status(data, 'FF')

            data['price'] = reservation_sum
            data['prepayment_sum'] = prepayment_sum
            data['room_index'] = room_index

            return data


def dates_between_in_year(ffrom, start, till, end):
    less = False
    bigger = False

    if ffrom.month < start.month:
        less = True

    if ffrom.month == start.month:
        if ffrom.day <= start.day:
            less = True


    if till.month > end.month:
        bigger = True

    if till.month == end.month:
        if till.day >= end.day:
            bigger = True

    return less and bigger