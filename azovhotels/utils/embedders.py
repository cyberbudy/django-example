# -*- coding: utf-8 -*-

from django.conf import settings

from .middleware import get_config

# Config Embedder functions

def config_embedder(obj, request, response, *args, **kwargs):
    if(hasattr(request, 'azovhotels_config')):
        response.data['config'] = request.azovhotels_config
    else:
        response.data['config'] = get_config()
