# -*- coding: utf-8 -*-

from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.html import urlize
from django.utils.http import urlsafe_base64_encode
from django.core.mail import EmailMultiAlternatives, EmailMessage, send_mass_mail
from django.conf import settings



def replace_mail_tags(request, msg, user=None):
    for key, value in mail_tags.items():
        if isinstance(value, list):
            kk = eval(value[0])

            if hasattr(kk, value[1]):
                data = getattr(kk, value[1])
            else:
                try:
                    data = kk.get(value[1], '')
                except e:
                    data = ''
        else:
            if value == 'token':
                link = 'http://{0}/auth/reset-confirm/{1}/{2}/'.format(*make_token(request, user))
                data = urlize(link)

        if not data:
            data = ''

        msg = msg.replace('{{'+key+'}}', data)

    return msg


def send_email(request, title, msg, user):
    msg = replace_mail_tags(request, msg, user)

    letter = EmailMessage(title, msg, settings.DEFAULT_FROM_EMAIL, [user.email])
    letter.encoding = "utf-8"
    letter.content_subtype = 'html'
    letter.send()


def make_token(request, user):
    domain = request.get_host()
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    token = default_token_generator.make_token(user)

    return [domain, uid, token]



mail_tags = {
    'email': ['user', 'email'],
    'first_name': ['user', 'first_name'],
    'last_name': ['user', 'last_name'],
    'token': 'token',
}