# -*- coding: utf-8 -*-

from django.core.exceptions import ObjectDoesNotExist

from azovhotels.serializers.admin.meta import MetaListSerializer
import azovhotels.models as m


# Configuration middleware
def get_config():
    try:
        obj = m.Meta.objects.get(key='config')
    except ObjectDoesNotExist:
        obj = None

    if obj:
        data = MetaListSerializer(obj).data
        return data.get('description', {})
    else:
        return {}

def get_meta():
    try:
        obj = m.Meta.objects.get(key='metadata')
    except ObjectDoesNotExist:
        obj = None

    if obj:
        data = MetaListSerializer(obj).data
        return data.get('description', {})
    else:
        return {}


class ConfigMiddleware(object):
    def process_request(self, request):
        request.azovhotels_config = get_config()
        request.azovhotels_metadata = get_meta()