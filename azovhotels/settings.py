"""
Django settings for azovhotels project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# import djcelery
# djcelery.setup_loader()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ')388ngt7%8u_5x26$%zt@%hh@$e60w0oo@(z_j@r2!u9vr#$1='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'azovhotels.utils.context_processors.config',
)

ALLOWED_HOSTS = ['*']

IMAGE_SIZES_FORMAT = "{0}-{1}-{2}"
IMAGE_SIZES = {
    'small': (68, 68),
    'medium': (220, 125),
    'big': (700, 410)
}


EMAIL_BACKEND = "django_mailer.smtp_queue.EmailBackend"

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = '' # here valid gmail email
EMAIL_HOST_PASSWORD = '' # valid host user email password
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER
# EMAIL_BACKEND = 'django_mailer.smtp_queue.EmailBackend'

# CELERY_IMPORTS = ('azovhotels.tasks',)
# BROKER_BACKEND = 'djkombu.transport.DatabaseTransport'
# BROKER_URL = 'django://'
# BROKER_HOST = 'localhost'
# BROKER_PORT = 5672
# BRKER_USER = 'guest'
# BROKER_PASSWORD = 'guest'
# BROKER_VHOST = '/'

# Application definition
INSTALLED_APPS = (
    # 'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'azovhotels',
    'south',
    'rest_framework',
    # 'debug_toolbar',
    'django_mailer'
    # 'djcelery',
    # 'kombu.transport.django'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.RemoteUserMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'azovhotels.utils.middleware.ConfigMiddleware',
)

ROOT_URLCONF = 'azovhotels.urls'
APPEND_SLASH = True

WSGI_APPLICATION = 'azovhotels.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    
   # 'default': {
   #      'ENGINE': 'django.db.backends.mysql', 
   #      'NAME': 'db2',
   #      'USER': 'root',
   #      'PASSWORD': '111',
   #      'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
   #      'PORT': '3306',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(STATIC_ROOT, 'uploads')
MEDIA_URL = STATIC_URL + 'uploads/'

STATICFILES_DIRS = (
    # os.path.join(BASE_DIR, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


# Auth settings
AUTH_USER_MODEL = 'azovhotels.User'
AUTHENTICATION_BACKENDS = (
    # 'azovhotels.backends.auth.EmailAuthBackend',
    # 'django.contrib.auth.backends.RemoteUserBackend',
    'django.contrib.auth.backends.ModelBackend',
)

AZOVHOTELS_REST_VIEW_EMBEDDERS = (
    'azovhotels.utils.embedders.config_embedder',
)

REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',

    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        # 'azovhotels.utils.permissions.CustomPermission'
        # 'rest_framework.permissions.DjangoModelPermissions'
    ],

    'DATETIME_INPUT_FORMATS': [
        '%Y-%m-%d',
        'iso-8601',
    ],

    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.DjangoFilterBackend',
    ),

    # 'DEFAULT_PAGINATION_SERIALIZER_CLASS':
    #     'rest_framework.pagination.PaginationSerializer',

    'SEARCH_PARAM': 'search',
    'ORDERING_PARAM': 'ordering',

    'PAGINATE_BY': 10,
    'PAGINATE_BY_PARAM': 'page_size',
    'MAX_PAGINATE_BY': 100000000,
}
