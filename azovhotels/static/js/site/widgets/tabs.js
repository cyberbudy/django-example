angular.module('widgets.tabs', [])

.controller('WidgetTabsCtrl', function($scope) {
  $scope.currentTab = 0;

  $scope.changeTab = function(i) {
    $scope.currentTab = i;
  };
})

.directive('widgetTabs', function() {
  return  {
    restrict: 'A',
    scope: true,
    controller: 'WidgetTabsCtrl',
    link: function($scope, $element, $attrs) {},
  }
});