angular.module('services.exceptionHandler', ['services.notifications'])


.factory('ExceptionHandlerFactory', function($injector) {
  return function($delegate) {

    return function (exception, cause) {
      // Lazy load notifications to get around circular dependency
      // Circular dependency: $rootScope <- notifications
      var notifications = $injector.get('notifications');

      // Pass through to original handler
      $delegate(exception, cause);

      // Push a notification error
      notifications.push({
        type: 'error',
        title: 'Fatal Error:',
        content: exception,
        cause: cause
      });
    };
  };
})

.config(function($provide) {
  $provide.decorator('$exceptionHandler', function (
    $delegate,
    ExceptionHandlerFactory) {
    return ExceptionHandlerFactory($delegate);
  });
});