angular.module('services.crudRouteProvider', [
  'ui.router',
  'app.conf',
])

.provider('crudRoute', function($stateProvider, pathTemlate) {

  // This $get noop is because at the moment in AngularJS "providers" must provide something
  // via a $get method.
  // When AngularJS has "provider helpers" then this will go away!
  this.$get = angular.noop;

  // Then we would have a much cleaner syntax and not have to do stuff
  // like:
  //
  // ```
  // myMod.config(function(crudRouteProvider) {
  //   var routeProvider = crudRouteProvider.routesFor('MyBook', '/myApp');
  // });
  // ```
  //
  // but instead have something like:
  //
  //
  // ```
  // myMod.config(function(crudRouteProvider) {
  //   var routeProvider = crudRouteProvider('MyBook', '/myApp');
  // });
  // ```

  this.routesFor = function(resourceName, urlPfixes) {
    var ex = angular.extend, copy = angular.copy;
    var state = resourceName.toLowerCase();
    var baseUrl = '/' + state;
    var promises = {
      list: function(Api, $stateParams) {
        return Api[state].query($stateParams).$promise;
      },
      single: function(Api, $stateParams) {
        return Api[state].get($stateParams).$promise;
      },
      create: function(Api, $stateParams) {
        return Api[state].get(angular.extend({id: 0, controller: 'related'},
            $stateParams)).$promise;
      },
    };

    if(angular.isString(urlPfixes[0]) && urlPfixes[0] !== '') {
      baseUrl = urlPfixes[0] + '/' + baseUrl;
    }

    // Append the postfix if it was provided;
    if(angular.isString(urlPfixes[1]) && urlPfixes[1] !== '') {
      baseUrl = baseUrl + urlPfixes[1];
    }

    var templatePath = pathTemlate + 'views/' + state + '/';

    var templateUrl = function(operation) {
      return templatePath + operation.toLowerCase() + '.html';
    };

    var controllerName = function(operation) {
      return function() {
        return resourceName + operation +'Ctrl'
      };
    };

    var stateBuilder = {
      // Create a route that will handle showing a list of items
      stateList: function(resolveFns) {
        stateBuilder.state(state, {
          url: baseUrl,
          views: {
            'view@': {
              dependencies: [],

              templateUrl: templateUrl('List'),
              controller: 'CrudListCtrl',
              resolve: ex({
                resourceName: function() { return resourceName; },
                controllerName: controllerName('List'),
                list: promises.list,
              }, resolveFns),
              reload: true,
            }
          }
        });
        return stateBuilder;
      },
      // Create a route that will handle editing an existing item
      stateDetail: function(resolveFns) {
        stateBuilder.state(state + '.detail', {
          url: '/detail/{id}',
          views: {
            'view@': {
              dependencies: ['id'],

              templateUrl: templateUrl('Detail'),
              controller: 'CrudSingleCtrl',
              resolve: ex({
                resourceName: function() { return resourceName; },
                controllerName: controllerName('Detail'),
                single: promises.single,
              }, resolveFns),
            }
          }
        });
        return stateBuilder;
      },
      // Create a route that will handle creating a new item
      stateCreate: function(resolveFns) {
        stateBuilder.state(state + '.create', {
          url: '/create',
          views: {
            'view@': {
              templateUrl: templateUrl('Create'),
              controller: 'CrudSingleCtrl',
              resolve: ex({
                resourceName: function() { return resourceName; },
                controllerName: controllerName('Create'),
                create: promises.create,
              }, resolveFns),
            }
          }
        });
        return stateBuilder;
      },
      // Pass-through to `$stateProvider.state()`
      state: function(name, definition) {
        $stateProvider.state(name, definition);
        return stateBuilder;
      },
      // Access to the core $stateProvider.
      $stateProvider: $stateProvider
    };

    return stateBuilder;
  };
})

.factory('controllerExtend', function($controller) {
  return function(controller, params) {
    try {
      $controller(controller, params);
    } catch (e) {
      if(!(e.message.indexOf('[ng:areq]') == 0)) {
        throw e;
      }
    }
  };
})

.factory('CroudSearch', function($state) {
  var CroudSearch = function() {
    this.params = $state.params || {};
    var i, l, searchParams = this.searchParams = [
        'ordering',
        'search',
        'status',
        'type_status',
        'is_active',
      ],
        searchParamsClean = this.searchParamsClean = {};

    for(i = 0, l = searchParams.length; i < l; i++) {
      searchParamsClean[searchParams[i]] = undefined;
    }
  };

  CroudSearch.prototype.submit = function($event) {
    $event.preventDefault();
    this.params.page = undefined;

    $state.transitionTo($state.current, this.params, {
      reload: true,
      inherit: true,
      notify: true
    });
  };

  CroudSearch.prototype.clean = function() {
    $state.transitionTo($state.current, this.searchParamsClean, {
      reload: true,
      inherit: true,
      notify: true
    });
  };

  CroudSearch.prototype.inProgress = function() {
    var i, l,
        params = this.params,
        searchParams = this.searchParams,
        is = false;

    for(i = 0, l = searchParams.length; i < l; i++) {
      if(is) break;

      if(typeof params[searchParams[i]] !== 'undefined') {
        if(!(params[searchParams[i]] === null
          || params[searchParams[i]] === '')) {
          is = true;
        }
      }
    }

    return is;
  };

  return CroudSearch;
})

.factory('CroudListActions', function($state, $window) {
  var CroudListActions = function() {
    this.selectors = {
      ids: {},
      master: false,
    };
  };

  CroudListActions.prototype.getIds = function() {
    var ids = [], i, from = this.selectors.ids, isNumber = angular.isNumber;

    for(i in from) {
      if(!from.hasOwnProperty(i)) continue;

      if(isNumber(from[i]) && from[i] == 1) {
        ids.push(i);
      }

      return ids;
    }
  };

  CroudListActions.prototype.apply = function(action) {
    var actions = {

    };

    if(action in actions) {
      
    }
  };

  return CroudListActions;
})


/**
 * Crud controllers
 * ======================================================================== */

/**
 * Crud list controller.
 */
.controller('CrudListCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  controllerExtend,
  CroudSearch,
  CroudListActions,
  Api,
  resourceName,
  controllerName,
  list
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;
  $scope.data = $state.current.data;
  $scope.Api = Api;
  $scope.controllerName = controllerName;
  $scope.resourceName = resourceName;

  $scope.list = list;
  $scope.config = list.config;

  $scope.search = new CroudSearch($stateParams);
  $scope.actions = new CroudListActions();

  controllerExtend(controllerName, {$scope: $scope});
})


/**
 * Crud single controller
 */
.controller('CrudSingleCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  controllerExtend,
  Api,
  resourceName,
  controllerName,
  single
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;
  $scope.data = $state.current.data;
  $scope.Api = Api;
  $scope.controllerName = controllerName;
  $scope.resourceName = resourceName;

  $scope.single = single;
  $scope.config = single.config;

  $scope.delete = function() {
    $scope.single.$delete({}, function() {
      $state.go('^', {}, {
          reload: true,
          inherit: true,
          relative: $state.$current,
          notify: true
        });
    });
  };

  controllerExtend(controllerName, {$scope: $scope});
});
