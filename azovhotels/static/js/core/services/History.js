angular.module('services.history', [])

.factory('History', function($state, $window) {
  var History = function() {};

  History.prototype.back = function() {
    $window.history.back();
  };

  return new History();
})
