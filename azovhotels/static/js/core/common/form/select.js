angular.module('form.select', [])

.controller('FormSelectCtrl', function($scope, $filter, $parse) {
  $scope.formSelect = {
    param: 'id',
    model: [],
    selected: [],
    data: [],
    dataFiltered: [],
    filter: '',
    multiple: false,
    active: false,
    current: 0,

    watchers: {
      filter: function (newVal) {
        var fs = $scope.formSelect;

        fs.dataFiltered = $filter('filter')(
            $filter('filter')(fs.data, fs.filter),
            function(item) {
              var val = fs.model.$modelValue;
              if(angular.isArray(val)) {
                return val.indexOf(item[fs.param]) < 0;
              } else {
                return val !== item[fs.param];
              }
            }
          );
        fs.current = 0;
      },

      data: function(newVal) {
        var fs = $scope.formSelect;

        $scope.formSelect.data = angular.copy(newVal);
        fs.watchers.filter(fs.filter);
      },

      model: function(newVal) {
        $scope.formSelect.model = newVal;
      },
    },

    prevent: function($event) {
      $event.preventDefault();
    },

    select: function(i) {
      var item = this.dataFiltered[i];
      var m = this.model;
      var val = m.$modelValue;

      if(angular.isArray(val)) {
        if(val.indexOf(item[this.param]) < 0) {
          if(!this.multiple) {
            if(this.selected.length > 0) {
              this.remove(0);
            }
          }

          val.push(item[this.param]);
          m.$setViewValue(val);
        }
      } else {
        this.remove(0);
        m.$setViewValue(item[this.param]);
      }

      this.selected.push(item);
      this.watchers.filter(this.data);
    },

    remove: function(i) {
      var item = this.selected[i];
      var m = this.model;
      var val = m.$modelValue;
      
      if(angular.isArray(val)) {
        if(val.length > 0) {
          val.splice(val.indexOf(this.selected[i][this.param]), 1);
          m.$setViewValue(val);
        }
      } else {
        m.$setViewValue(null);
      }

      this.selected.splice(i, 1);
      this.watchers.filter(this.data);
    },
  };
})

.directive('formSelect', function() {
  var directiveObject = {
    restrict: 'A',
    scope: true,
    controller: 'FormSelectCtrl',
    require: ['ngModel'],

    link: function($scope, $element, $attrs, controllers) {
      var ngModelCtrl = controllers[0],
        fs = $scope.formSelect;

      if(typeof $attrs.multiple != 'undefined') {
        fs.multiple = true;
      }

      $scope.$watch('formSelect.filter', fs.watchers.filter);
      $scope.$watch($attrs.formSelectData, fs.watchers.data);
      // $scope.$watch($attrs.ngModel, fs.watchers.filter);

      fs.model = ngModelCtrl;
      fs.watchers.data($scope.$eval($attrs.formSelectData));
      // fs.watchers.model(utils.getObjectDots($scope, $attrs.ngModel));
      fs.dataFiltered = angular.copy(fs.data);

      var i, l,
          model = $scope.$eval($attrs.ngModel),
          data = fs.data;

      for(i = 0, l = data.length; i < l; i++) {
        if(angular.isArray(model)) {
          if(model.indexOf(data[i][fs.param]) >= 0) {
            fs.selected.push(data[i]);
          }
        } else {
          if(model == data[i][fs.param]) {
            fs.selected.push(data[i]);
          }
        }
      }

      fs.watchers.filter('');
    },
  };

  return directiveObject;
})

.directive('formSelectToggle', function($filter) {
  var directiveObject = {
    restrict: 'A',
    require: '^formSelect',

    link: function($scope, $element, $attrs, controller) {
      $element.on('focus', function() {
        $scope.$apply(function() {
          $scope.formSelect.active = true;
          $scope.formSelect.current = 0;
        });
      });

      $element.on('blur', function() {
        $scope.$apply(function() {
          var fs = $scope.formSelect;

          fs.active = false;
          fs.current = 0;
          fs.filter = '';
        });
      });

      $element.on('keydown', function($event) {
        var key = $event.which, fs = $scope.formSelect,
            keys = {
              ESC: 27,
              UP: 38,
              DOWN: 40,
              ENTER: 13,
            };

        if(key == keys.ESC) {
          $scope.$apply(function() {
            fs.current = 0;
            fs.filter = '';
          });
        }

        if(key == keys.ENTER) {
          $event.preventDefault();

          if(fs.dataFiltered.length > 0) {
            $scope.$apply(function() {
              fs.select(fs.current);
              fs.filter = '';
              fs.current = 0;
            });
          }
        }

        if(key == keys.UP) {
          $event.preventDefault();

          $scope.$apply(function() {
            if(fs.current > 0) {
              fs.current--;
            }
          });
        }

        if(key == keys.DOWN) {
          $event.preventDefault();

          $scope.$apply(function() {
            if(fs.current < fs.dataFiltered.length - 1) {
              fs.current++;
            }
          });
        }
      });
    },
  };

  return directiveObject;
});