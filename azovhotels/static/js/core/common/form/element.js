/**
 * Form Element module
 * ======================================================================== */

angular.module('form.element', [])

.controller('FormElementCtrl', function ($scope) {
  var o = ($scope.formElementObject = {});

  o.focused = false;

  this.focus = function(is) {
    $scope.$apply(function() {
      $scope.formElementObject.focused = is;
    });
  };
})

.directive('formElement', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    controller: 'FormElementCtrl',

    link: function link($scope, $element, $attrs, controller) {
      $scope.$watch('formElementObject.focused', function(valNew, valOld) {
        if(valNew) {
          $element.addClass('_focus');
        } else {
          $element.removeClass('_focus');
        }
      });
    }
  };

  return directiveObject;
})

.directive('formElementField', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '^formElement',
    scope: false,

    link: function link($scope, $element, $attrs, controller) {
      $element.on('focus', function($event) {
        controller.focus(true);
      }).on('blur', function($event) {
        controller.focus(false);
      });
    },
  };

  return directiveObject;
});