angular.module('form.validators', [])

.directive('formValidateEquals', function() {
  var directiveObject = {
    restrict: 'A',
    require: '?ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      if(!ngModelCtrl) return;

      $scope.$watch($attrs.ngModel, function() {
        validate();
      });

      $scope.$watch($attrs.formValidateEquals, function (val) {
        validate();
      });

      var validate = function() {
        var val1 = ngModelCtrl.$viewValue;
        var val2 = $scope.$eval($attrs.formValidateEquals);

        ngModelCtrl.$setValidity('equals', val1 === val2);
      };
    }
  };

  return directiveObject;
})

.directive('formFormatterDate', function ($filter) {
  return {
    require: '?ngModel',
    priority: 1,
    link: function($scope, $element, $attrs, ngModelCtrl) {
      if (!ngModelCtrl) return;

      ngModelCtrl.$formatters.unshift(function(value) {
        console.log('happens')
        if(value) {
          return $filter('date')(value, $attrs.formFormatterDate);
        }
      })
    }
  };
});