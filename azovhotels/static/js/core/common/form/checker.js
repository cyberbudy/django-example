/**
 * Form checker module
 * ======================================================================== */

angular.module('form.checker', [])

.directive('formRadio', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '?ngModel',

    link: function link($scope, $element, $attrs, ngModelCtrl) {
      if(!ngModelCtrl) return;

      //model -> UI
      ngModelCtrl.$render = function () {
        $element.toggleClass('_checked', (ngModelCtrl.$modelValue ==
          $attrs.value));
      };

      //ui->model
      $element.bind('click', function () {
        var isActive = $element.hasClass('_checked');

        if (!isActive || angular.isDefined($attrs.uncheckable)) {
          $scope.$apply(function () {
            ngModelCtrl.$setViewValue(isActive ? null : $attrs.value);
            ngModelCtrl.$render();
          });
        }
      });
    },
  };

  return directiveObject;
})


.directive('formCheckbox', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '?ngModel',

    link: function link($scope, $element, $attrs, ngModelCtrl) {
      if(!ngModelCtrl) return;

      function getTrueValue() {
        return getCheckboxValue($attrs.checked, true);
      }

      function getFalseValue() {
        return getCheckboxValue($attrs.checked, false);
      }

      function getCheckboxValue(attributeValue, defaultValue) {
        var val = $scope.$eval(attributeValue);
        return angular.isDefined(val) ? val : defaultValue;
      }

      //model -> UI
      ngModelCtrl.$render = function () {
        $element.toggleClass('_checked',
          angular.equals(ngModelCtrl.$modelValue, getTrueValue()));
      };

      //ui->model
      if($attrs.formCheckboxBind) {
        $element.bind('click', function () {
          $scope.$apply(function () {
            ngModelCtrl.$setViewValue($element.hasClass('_checked') ?
              getFalseValue() : getTrueValue());
            ngModelCtrl.$render();
          });
        });
      }

      if($attrs.ngChecked) {
        $scope.$watch($attrs.ngChecked, function(newVal) {
          ngModelCtrl.$setViewValue(newVal);
          ngModelCtrl.$render();
        });
      }
    },
  };

  return directiveObject;
});