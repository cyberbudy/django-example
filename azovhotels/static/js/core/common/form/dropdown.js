angular.module('form.dropdown', [])

.controller('FormDropdownCtrl', function ($scope) {
  var o = ($scope.formDropdownObject = {});

  o.active = false;
})

.directive('formDropdown', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    controller: 'FormDropdownCtrl',

    link: function link($scope, $element, $attrs, controller) {
      $scope.$watch('formDropdownObject.active', function(newVal) {
        $element.toggleClass('_active', newVal);
      });
    },
  };

  return directiveObject;
})

.directive('formDropdownActivate', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '^formDropdown',
    scope: false,

    link: function link($scope, $element, $attrs, controller) {
      $element.on($attrs.formDropdownActivate, function() {
        $scope.$apply(function() {
          $scope.formDropdownObject.active = true;
        });
      })
    },
  };

  return directiveObject;
})

.directive('formDropdownDeactivate', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '^formDropdown',
    scope: false,

    link: function link($scope, $element, $attrs, controller) {
      $element.on($attrs.formDropdownDeactivate, function() {
        $scope.$apply(function() {
          $scope.formDropdownObject.active = false;
        });
      })
    },
  };

  return directiveObject;
})

.directive('formDropdownList', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '^formDropdown',
    scope: false,

    link: function link($scope, $element, $attrs, controller) {
      $element.on('mousedown', function($event) {
        $event.preventDefault();
      });
    },
  };

  return directiveObject;
});