angular.module('form.price-picker', [])


.factory('PricePickerDataBuilder', function() {
  return function(data) {
    var i, l, d, year, month, day, r = {};
    
    for(i = 0, l = data.length; i < l; i++) {
      d = new Date(data[i].date);
      year = d.getFullYear();
      month = d.getMonth();
      day = d.getDate();
      
      if(typeof r[year] == 'undefined') {
        r[year] = {}
      }
      
      if(typeof r[year][month] == 'undefined') {
        r[year][month] = {}
      }
      
      r[year][month][day] = data[i];
    }
    
    return r;
  };
})


.factory('PricePickerDay', function() {
  var PricePickerDay = function(date, data) {
    this.date = new Date(date);
    this.active = false;
    this.data = data;
  }
  
  return PricePickerDay;
})


.factory('PricePicker', function(PricePickerDay, $document) {
  var PricePicker = function(data, month) {
    var self = this, today = new Date();
    
    this.selected = false;
    this.days = [];
    this.data = angular.copy(data);
    this.month = new Date(month);
    this.today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    this.offset = [6,0,1,2,3,4,5][this.month.getDay()];
    
    this.dayStart = null;
    this.dayEnd = null;
    
    this.posStart = null;
    this.posEnd = null;
    
    this.fill();
    
    $document.on('mouseup', function() {
      if(self.posStart === null) return;
      
      self.clear();
    });
  };
  
  PricePicker.prototype.fill = function() {
    var i, l, e, d = this.month, data,
        to = new Date(this.month.getYear(),
          this.month.getMonth() + 1, 0);
    this.days = [];
    
    for(i = 0, l = to.getDate(); i < l; i++) {
      data = null;
      year = d.getFullYear();
      month = d.getMonth();
      day = d.getDate();
      
      try {
        data = this.data.objects[year][month][day];
      } catch (e) {}

      if(typeof data === 'undefined' || data === null) {
        data = this.data.default;
      }
      
      this.days.push(new PricePickerDay(d, angular.copy(data)));
      d.setDate(d.getDate() + 1);
    }
  };
  
  PricePicker.prototype.dragStart = function(i) {
    this.posStart = i;
    this.dayStart = this.days[i];
    this.posEnd = i;
    this.setActive(this.posStart, this.posEnd);
  };
  
  PricePicker.prototype.dragOver = function(i) {
    if(this.posStart === null) return;
    
    this.setActive(this.posStart, this.posEnd, false);
    this.posEnd = i;
    this.setActive(this.posStart, this.posEnd);
  };
  
  PricePicker.prototype.dragEnd = function(i) {
    if(this.posStart === null) return;
    
    this.dayEnd = this.days[i];
    
    if(this.dayStart.date.getDate() >
      this.dayEnd.date.getDate()) {
      var change = this.dayStart;
      
      this.dayStart = this.dayEnd;
      this.dayEnd = change;
    }
    
    this.selected = true;
    
    this.clear();
  };
  
  PricePicker.prototype.clear = function($event) {
    var i, l, days = this.days;
    
    this.setActive(this.posStart, this.posEnd, false);
    this.posStart = null;
    this.posEnd = null;
  };
  
  PricePicker.prototype.setActive = function(from, to, active) {
    var i, days = this.days;
    
    if(typeof active == 'undefined') {
      active = true;
    }
    
    if(from > to) {
      for(i = from; i >= to; i--) {
        days[i].active = active;
      }
    } else {
      for(i = from; i <= to; i++) {
        days[i].active = active;
      }
    }
  };
  
  return PricePicker;
})


.controller('PricePickerCtrl', function($scope, PricePicker,
  PricePickerDataBuilder, $filter, $attrs, $parse) {
  var data = angular.copy($parse($attrs.pricePicker)($scope)),
      date = $parse($attrs.pricePickerDate)($scope);

  data.objects = new PricePickerDataBuilder(data.objects);

  $scope.pricePicker = new PricePicker(data, date);
})


.directive('pricePicker', function() {
  var directiveObject = {
    restrict: 'A',
    controller: 'PricePickerCtrl',
    scope: true,
    link: function($scope, $element, $attrs) {}
  };
  
  return directiveObject;
})