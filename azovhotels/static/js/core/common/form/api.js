/**
 * Form api module
 * ======================================================================== */

angular.module('form.api', [])

.directive('formApi', function (Api) {
  var directiveObject = {
    restrict: 'A',
    // controller: 'FormApiCtrl',

    link: function link($scope, $element, $attrs) {
      $element.on('submit', function($event) {
        var data = $attrs.formApi,
            method = $attrs.method,
            params = $scope.eval($attrs.formApiParams);

        if($scope[name].$valid) {
          var i, l, b,
              data = $scope.$eval()
              saveData = {},
              name = $attrs.name,
              fields = [],
              form = $scope[name];

          for(i in form) {
            if(!form.hasOwnProperty(i)) continue;

            saveData[i] = data[i];
          }

          $scope.$eval($attrs.formApi);
        }
      });
    },
  };

  return directiveObject;
});