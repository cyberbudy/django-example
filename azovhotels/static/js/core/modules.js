/**
 * Utility functions.
 * ======================================================================== */

var utils = window.utils ? window.utils : (window.utils = {});

/**
 * Cpitalizing the string:
 * 'Alex TRansf ulerioo'.capitalize() -> 'Alex Transf Ulerioo'
 *
 * @param {boolean} lower: If set string tolowercase before transform.
 *                         Default true.
 * @returns {string} Capitalized string.
 */
String.prototype.capitalize = function(lower) {
  if(typeof lower === 'undefined') {
    lower = true;
  }

  return (lower ? this.toLowerCase() : this)
    .replace(/(?:^|\s)\S/g, function(a) {
      return a.toUpperCase();
    });
};


/**
 * Injecting params to string.
 *
 * @param {string} template template string
 * @param {string} params params string
 * @returns {function} Injector
 */
utils.injectStateParams = function(template, params) {
  var i;

  for(i in params) {
    if(!params.hasOwnProperty(i)) continue;

    template = template.replace(new RegExp('\{' + i + '\}', 'g'), params[i]);
  }

  return template;
};


/**
 * Getting current params from the route and replace placeholders in state
 * attributes.
 *
 * @param {string} str template string
 * @returns {function} Injector
 */
utils.injectStateParams = function(str) {
  return function($stateParams) {
    return utils.injectStateParams(str, $stateParams);
  }
};


/**
 * Resolves multiple named promises into named list.
 *
 * @param {$q} $q promise factory
 * @param {object} promises promises named list(object)
 * @returns {$deffer.promise} promise(named list on resolve)
 */
utils.resolveMultiple = function($q, promises) {
  var deferred = $q.defer();
  var i, v, keys = [], values = [];

  for(i in promises) {
    if(!promises.hasOwnProperty(i)) continue;

    keys.push(i);
    values.push(promises[i]);
  }

  $q.all(values).then(function(response) {
    var i, l = keys.length, r = {};

    for(i = 0; i < l; i++) {
      r[keys[i]] = response[i];
    }

    deferred.resolve(r); 
  }, function(reason) {
    deferred.reject(reason);
  });

  return deferred.promise;
};


/**
 * Returns child element of object by dot notation path.
 *
 * @param {object} from
 * @param {string} path
 * @returns {mixed} resolved element
 */
utils.getObjectDots = function(from, path) {
  var i, l, path = path.split('.');

  for (i = 0, l = path.length; i < l; i++) {
    if(typeof from != 'undefined') {
      from = from[path[i]];
    } else {
      throw new Error('There is no such element "'
        + path[i] + '" in: ' + path.join('.'));
    }
  }

  return from;
}
angular.module('form', [
  'form.element',
  'form.input',
  'form.checker',
  'form.upload',
  'form.slugify',
  'form.submit',
  'form.select',
  'form.editor',
  'form.files-list',
  'form.validators',
  'form.datafill',
  'form.price-picker',
  'form.value',
  'form.datepicker',
  // 'form.templates',
  // 'form.files-list',

  // 'ui.codemirror',
  // 'ui.bootstrap.datepicker',
  'ui.bootstrap.tabs',
  'ngCkeditor',
]);
/**
 * Form Element module
 * ======================================================================== */

angular.module('form.element', [])

.controller('FormElementCtrl', function ($scope) {
  var o = ($scope.formElementObject = {});

  o.focused = false;

  this.focus = function(is) {
    $scope.$apply(function() {
      $scope.formElementObject.focused = is;
    });
  };
})

.directive('formElement', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    controller: 'FormElementCtrl',

    link: function link($scope, $element, $attrs, controller) {
      $scope.$watch('formElementObject.focused', function(valNew, valOld) {
        if(valNew) {
          $element.addClass('_focus');
        } else {
          $element.removeClass('_focus');
        }
      });
    }
  };

  return directiveObject;
})

.directive('formElementField', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '^formElement',
    scope: false,

    link: function link($scope, $element, $attrs, controller) {
      $element.on('focus', function($event) {
        controller.focus(true);
      }).on('blur', function($event) {
        controller.focus(false);
      });
    },
  };

  return directiveObject;
});
/**
 * Form input module
 * ======================================================================== */

angular.module('form.input', [])

.controller('FormInputCtrl', function ($scope) {
  var o = ($scope.formInputObject = {});
})

.directive('formInput', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    controller: 'FormInputCtrl',

    link: function link($scope, $element, $attrs, controller) {},
  };

  return directiveObject;
})

.directive('formInputField', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '^formInput',
    scope: false,

    link: function link($scope, $element, $attrs, controller) {},
  };

  return directiveObject;
});
angular.module('form.select', [])

.controller('FormSelectCtrl', function($scope, $filter, $parse) {
  $scope.formSelect = {
    param: 'id',
    model: [],
    selected: [],
    data: [],
    dataFiltered: [],
    filter: '',
    multiple: false,
    active: false,
    current: 0,

    watchers: {
      filter: function (newVal) {
        var fs = $scope.formSelect;

        fs.dataFiltered = $filter('filter')(
            $filter('filter')(fs.data, fs.filter),
            function(item) {
              var val = fs.model.$modelValue;
              if(angular.isArray(val)) {
                return val.indexOf(item[fs.param]) < 0;
              } else {
                return val !== item[fs.param];
              }
            }
          );
        fs.current = 0;
      },

      data: function(newVal) {
        var fs = $scope.formSelect;

        $scope.formSelect.data = angular.copy(newVal);
        fs.watchers.filter(fs.filter);
      },

      model: function(newVal) {
        $scope.formSelect.model = newVal;
      },
    },

    prevent: function($event) {
      $event.preventDefault();
    },

    select: function(i) {
      var item = this.dataFiltered[i];
      var m = this.model;
      var val = m.$modelValue;

      if(angular.isArray(val)) {
        if(val.indexOf(item[this.param]) < 0) {
          if(!this.multiple) {
            if(this.selected.length > 0) {
              this.remove(0);
            }
          }

          val.push(item[this.param]);
          m.$setViewValue(val);
        }
      } else {
        this.remove(0);
        m.$setViewValue(item[this.param]);
      }

      this.selected.push(item);
      this.watchers.filter(this.data);
    },

    remove: function(i) {
      var item = this.selected[i];
      var m = this.model;
      var val = m.$modelValue;
      
      if(angular.isArray(val)) {
        if(val.length > 0) {
          val.splice(val.indexOf(this.selected[i][this.param]), 1);
          m.$setViewValue(val);
        }
      } else {
        m.$setViewValue(null);
      }

      this.selected.splice(i, 1);
      this.watchers.filter(this.data);
    },
  };
})

.directive('formSelect', function() {
  var directiveObject = {
    restrict: 'A',
    scope: true,
    controller: 'FormSelectCtrl',
    require: ['ngModel'],

    link: function($scope, $element, $attrs, controllers) {
      var ngModelCtrl = controllers[0],
        fs = $scope.formSelect;

      if(typeof $attrs.multiple != 'undefined') {
        fs.multiple = true;
      }

      $scope.$watch('formSelect.filter', fs.watchers.filter);
      $scope.$watch($attrs.formSelectData, fs.watchers.data);
      // $scope.$watch($attrs.ngModel, fs.watchers.filter);

      fs.model = ngModelCtrl;
      fs.watchers.data($scope.$eval($attrs.formSelectData));
      // fs.watchers.model(utils.getObjectDots($scope, $attrs.ngModel));
      fs.dataFiltered = angular.copy(fs.data);

      var i, l,
          model = $scope.$eval($attrs.ngModel),
          data = fs.data;

      for(i = 0, l = data.length; i < l; i++) {
        if(angular.isArray(model)) {
          if(model.indexOf(data[i][fs.param]) >= 0) {
            fs.selected.push(data[i]);
          }
        } else {
          if(model == data[i][fs.param]) {
            fs.selected.push(data[i]);
          }
        }
      }

      fs.watchers.filter('');
    },
  };

  return directiveObject;
})

.directive('formSelectToggle', function($filter) {
  var directiveObject = {
    restrict: 'A',
    require: '^formSelect',

    link: function($scope, $element, $attrs, controller) {
      $element.on('focus', function() {
        $scope.$apply(function() {
          $scope.formSelect.active = true;
          $scope.formSelect.current = 0;
        });
      });

      $element.on('blur', function() {
        $scope.$apply(function() {
          var fs = $scope.formSelect;

          fs.active = false;
          fs.current = 0;
          fs.filter = '';
        });
      });

      $element.on('keydown', function($event) {
        var key = $event.which, fs = $scope.formSelect,
            keys = {
              ESC: 27,
              UP: 38,
              DOWN: 40,
              ENTER: 13,
            };

        if(key == keys.ESC) {
          $scope.$apply(function() {
            fs.current = 0;
            fs.filter = '';
          });
        }

        if(key == keys.ENTER) {
          $event.preventDefault();

          if(fs.dataFiltered.length > 0) {
            $scope.$apply(function() {
              fs.select(fs.current);
              fs.filter = '';
              fs.current = 0;
            });
          }
        }

        if(key == keys.UP) {
          $event.preventDefault();

          $scope.$apply(function() {
            if(fs.current > 0) {
              fs.current--;
            }
          });
        }

        if(key == keys.DOWN) {
          $event.preventDefault();

          $scope.$apply(function() {
            if(fs.current < fs.dataFiltered.length - 1) {
              fs.current++;
            }
          });
        }
      });
    },
  };

  return directiveObject;
});
/**
 * Form checker module
 * ======================================================================== */

angular.module('form.checker', [])

.directive('formRadio', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '?ngModel',

    link: function link($scope, $element, $attrs, ngModelCtrl) {
      if(!ngModelCtrl) return;

      //model -> UI
      ngModelCtrl.$render = function () {
        $element.toggleClass('_checked', (ngModelCtrl.$modelValue ==
          $attrs.value));
      };

      //ui->model
      $element.bind('click', function () {
        var isActive = $element.hasClass('_checked');

        if (!isActive || angular.isDefined($attrs.uncheckable)) {
          $scope.$apply(function () {
            ngModelCtrl.$setViewValue(isActive ? null : $attrs.value);
            ngModelCtrl.$render();
          });
        }
      });
    },
  };

  return directiveObject;
})


.directive('formCheckbox', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '?ngModel',

    link: function link($scope, $element, $attrs, ngModelCtrl) {
      if(!ngModelCtrl) return;

      function getTrueValue() {
        return getCheckboxValue($attrs.checked, true);
      }

      function getFalseValue() {
        return getCheckboxValue($attrs.checked, false);
      }

      function getCheckboxValue(attributeValue, defaultValue) {
        var val = $scope.$eval(attributeValue);
        return angular.isDefined(val) ? val : defaultValue;
      }

      //model -> UI
      ngModelCtrl.$render = function () {
        $element.toggleClass('_checked',
          angular.equals(ngModelCtrl.$modelValue, getTrueValue()));
      };

      //ui->model
      if($attrs.formCheckboxBind) {
        $element.bind('click', function () {
          $scope.$apply(function () {
            ngModelCtrl.$setViewValue($element.hasClass('_checked') ?
              getFalseValue() : getTrueValue());
            ngModelCtrl.$render();
          });
        });
      }

      if($attrs.ngChecked) {
        $scope.$watch($attrs.ngChecked, function(newVal) {
          ngModelCtrl.$setViewValue(newVal);
          ngModelCtrl.$render();
        });
      }
    },
  };

  return directiveObject;
});
/**
 * Form upload field
 * ======================================================================== */

angular.module('form.upload', [
  'angularFileUpload'
])

.constant('pathFormUpload', '/api/files/')

.controller('FormUploadCtrl', function(
  $scope,
  $attrs,
  $fileUploader,
  $parse,
  $cookies,
  pathFormUpload
  ) {
  var uploader = $scope.formUploader = {};

  uploader.uploader = $fileUploader.create({
      scope: $scope,
      url: pathFormUpload,
      method: 'POST',
      headers: {
        'X-CSRFToken': $cookies['csrftoken'],
      },
      filters: [
        function(item /*{File|HTMLInputElement}*/) {
          var type = uploader.uploader.isHTML5 ? item.type : '/'
            + item.value.slice(item.value.lastIndexOf('.') + 1);
          type = '|' + type.toLowerCase().slice(type.lastIndexOf('/') + 1) + '|';
          return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
      ]
  });

  uploader.uploader.autoUpload = true;
  uploader.uploaded = [];

  uploader.model = $parse($attrs.formUpload);
  uploader.files = $parse($attrs.formUploadFiles);

  uploader.uploader.bind('success', function (event, xhr, item, response) {
    $scope.$apply(function() {
      var modelData = uploader.model($scope),
          filesData = uploader.files($scope);

      console.log('become');

      if(angular.isArray(modelData)) {
        modelData.push(response.id);
        filesData.push(response);
        console.log('array')
      } else {
        modelData = response.id;
        filesData = response;
        console.log('single')
      }

      uploader.model.assign($scope, modelData);
      uploader.files.assign($scope, filesData);
    });
  });
})

.directive('formUpload', function($parse) {
  var directiveObject = {
    restrict: 'A',
    controller: 'FormUploadCtrl',
    scope: true,
    link: function($scope, $element, $attrs) {}
  };

  return directiveObject;
});
/**
 * Form slugify field module
 * ======================================================================== */

angular.module('form.slugify', [])

.factory('FormSlugify', function() {
  var FormSlugify = function(dash) {
    var dash = dash;

    var chars = {
      'а': 'a',
      'б': 'b',
      'в': 'v',
      'г': 'g',
      'д': 'd',
      'е': 'e',
      'ё': 'e',
      'ж': 'zh',
      'з': 'z',
      'и': 'i',
      'й': 'i',
      'к': 'k',
      'л': 'l',
      'м': 'm',
      'н': 'n',
      'о': 'o',
      'п': 'p',
      'р': 'r',
      'с': 's',
      'т': 't',
      'у': 'u',
      'ф': 'f',
      'х': 'h',
      'ц': 'c',
      'ч': 'ch',
      'ш': 'sh',
      'щ': 'shch',
      'ъ': '',
      'ы': 'y',
      'ь': '',
      'э': 'e',
      'ю': 'yu',
      'я': 'ya',
    }

    var from = [];

    for(i in chars) {
      if(!chars.hasOwnProperty(i)) continue;

      from.push(i);
    }

    this.chars = chars;
    this.dash = dash;
    this.from = from;
  };

  FormSlugify.prototype.slugify = function(str) {
    var str = str || '',
        dash = this.dash,
        from = this.from,
        chars = this.chars;

    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    for (var i = 0, l = from.length; i < l; i++) {
      str = str.replace(new RegExp(from[i], 'g'), chars[from[i]]);
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, dash) // collapse whitespace and replace by -
      .replace(new RegExp(dash + '+', 'g'), dash); // collapse dashes

    return str;
  };

  return FormSlugify;
})

.factory('formSlugify', function(FormSlugify) {
  return new FormSlugify('-');
})

.directive('formSlugify', function(formSlugify) {
  var directiveObject = {
    restrict: 'A',
    scope: {
      model: '=ngModel',
      slug: '=formSlugify',
    },

    link: function link($scope, $element, $attrs) {
      if(!$scope.slug) {
        $scope.slug = formSlugify.slugify($scope.model);
      }
      $scope.$watch('model', function(newValue, oldValue) {
        if($scope.slug == formSlugify.slugify(oldValue) || !$scope.slug) {
          $scope.slug = formSlugify.slugify(newValue);
        }
      });
    },
  };

  return directiveObject;
});
/**
 * Form submit module
 * ======================================================================== */

angular.module('form.submit', [])

.directive('formSubmit', function () {
  var directiveObject = {
    restrict: 'A',

    link: function link($scope, $element, $attrs) {
      $element.on('submit', function($event) {
        var name = $attrs.name, form = $scope[name];

        if(form.$valid) {
          $scope.$eval($attrs.formSubmit);
        } else {
          $scope.$apply(function() {
            form.$submitTry = true;
          });

          var i, firstInvalid = false;

          for(i in form) {
            if(!form.hasOwnProperty(i) ||
              typeof form[i].$valid == 'undefined') continue;

            if(!form[i].$valid) {
              firstInvalid = document.forms[name].elements.namedItem(i);
              break;
            }
          }

          if(firstInvalid) {
            firstInvalid.focus();
          }
        }
      });
    },
  };

  return directiveObject;
});
angular.module('form.editor', ['ngCkeditor'])

.directive('formEditorOptions', function() {
  var directiveObject = {
    restrict: 'A',
    scope: true,
    link: function($scope, $element, $attrs) {
      var attr = $attrs.formEditorOptions, e, options = {};

      if(typeof attr != 'undefined' && attr != null && attr != '') {
        try {
          // options = angular.fromJson(attr);
          options = $scope.$eval(attr);
        } catch(e) {
          throw e;
        }
      }

      $scope.editorOptions = options;
    },
  };

  return directiveObject;
});
/* ========================================================================
 * Form files list methods
 * ======================================================================== */

angular.module('form.files-list', [])

.controller('FormFilesListCtrl', function ($scope) {
  $scope.remove = function (index) {
    var s = $scope, fl = s.single.related_data.files;
    s.single.files.splice(s.single.files.indexOf(fl[index].id), 1);
    fl.splice(index, 1);
  };
})

.directive('formFilesList', function () {
  var directiveObject = {
      restrict: 'A',
      controller: 'FormFilesListCtrl',
      scope: true,
      link: function ($scope, $element, $attrs, controller) {}
    };
  return directiveObject;
});
angular.module('form.validators', [])

.directive('formValidateEquals', function() {
  var directiveObject = {
    restrict: 'A',
    require: '?ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      if(!ngModelCtrl) return;

      $scope.$watch($attrs.ngModel, function() {
        validate();
      });

      $scope.$watch($attrs.formValidateEquals, function (val) {
        validate();
      });

      var validate = function() {
        var val1 = ngModelCtrl.$viewValue;
        var val2 = $scope.$eval($attrs.formValidateEquals);

        ngModelCtrl.$setValidity('equals', val1 === val2);
      };
    }
  };

  return directiveObject;
})

.directive('formFormatterDate', function ($filter) {
  return {
    require: '?ngModel',
    priority: 1,
    link: function($scope, $element, $attrs, ngModelCtrl) {
      if (!ngModelCtrl) return;

      ngModelCtrl.$formatters.unshift(function(value) {
        console.log('happens')
        if(value) {
          return $filter('date')(value, $attrs.formFormatterDate);
        }
      })
    }
  };
});
angular.module('form.datafill', [])

.directive('formDatafillNumbers', function() {
  var directiveObject = {
    restrict: 'A',
    controller: function($scope, $attrs) {
      var i, attr = $attrs.formDatafillNumbers,
          count = $attrs.formDatafillNumbersCount;

      var datafill = $scope[attr] = [];

      for(i = 0; i <= count; i++) {
        datafill.push({id: i, name: i});
      }
    },
    scope: true,
    link: function($scope, $element, $attrs) {},
  };

  return directiveObject;
});
angular.module('form.price-picker', [])


.factory('PricePickerDataBuilder', function() {
  return function(data) {
    var i, l, d, year, month, day, r = {};
    
    for(i = 0, l = data.length; i < l; i++) {
      d = new Date(data[i].date);
      year = d.getFullYear();
      month = d.getMonth();
      day = d.getDate();
      
      if(typeof r[year] == 'undefined') {
        r[year] = {}
      }
      
      if(typeof r[year][month] == 'undefined') {
        r[year][month] = {}
      }
      
      r[year][month][day] = data[i];
    }
    
    return r;
  };
})


.factory('PricePickerDay', function() {
  var PricePickerDay = function(date, data) {
    this.date = new Date(date);
    this.active = false;
    this.data = data;
  }
  
  return PricePickerDay;
})


.factory('PricePicker', function(PricePickerDay, $document) {
  var PricePicker = function(data, month) {
    var self = this, today = new Date();
    
    this.selected = false;
    this.days = [];
    this.data = angular.copy(data);
    this.month = new Date(month);
    this.today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    this.offset = [6,0,1,2,3,4,5][this.month.getDay()];
    
    this.dayStart = null;
    this.dayEnd = null;
    
    this.posStart = null;
    this.posEnd = null;
    
    this.fill();
    
    $document.on('mouseup', function() {
      if(self.posStart === null) return;
      
      self.clear();
    });
  };
  
  PricePicker.prototype.fill = function() {
    var i, l, e, d = this.month, data,
        to = new Date(this.month.getYear(),
          this.month.getMonth() + 1, 0);
    this.days = [];
    
    for(i = 0, l = to.getDate(); i < l; i++) {
      data = null;
      year = d.getFullYear();
      month = d.getMonth();
      day = d.getDate();
      
      try {
        data = this.data.objects[year][month][day];
      } catch (e) {}

      if(typeof data === 'undefined' || data === null) {
        data = this.data.default;
      }
      
      this.days.push(new PricePickerDay(d, angular.copy(data)));
      d.setDate(d.getDate() + 1);
    }
  };
  
  PricePicker.prototype.dragStart = function(i) {
    this.posStart = i;
    this.dayStart = this.days[i];
    this.posEnd = i;
    this.setActive(this.posStart, this.posEnd);
  };
  
  PricePicker.prototype.dragOver = function(i) {
    if(this.posStart === null) return;
    
    this.setActive(this.posStart, this.posEnd, false);
    this.posEnd = i;
    this.setActive(this.posStart, this.posEnd);
  };
  
  PricePicker.prototype.dragEnd = function(i) {
    if(this.posStart === null) return;
    
    this.dayEnd = this.days[i];
    
    if(this.dayStart.date.getDate() >
      this.dayEnd.date.getDate()) {
      var change = this.dayStart;
      
      this.dayStart = this.dayEnd;
      this.dayEnd = change;
    }
    
    this.selected = true;
    
    this.clear();
  };
  
  PricePicker.prototype.clear = function($event) {
    var i, l, days = this.days;
    
    this.setActive(this.posStart, this.posEnd, false);
    this.posStart = null;
    this.posEnd = null;
  };
  
  PricePicker.prototype.setActive = function(from, to, active) {
    var i, days = this.days;
    
    if(typeof active == 'undefined') {
      active = true;
    }
    
    if(from > to) {
      for(i = from; i >= to; i--) {
        days[i].active = active;
      }
    } else {
      for(i = from; i <= to; i++) {
        days[i].active = active;
      }
    }
  };
  
  return PricePicker;
})


.controller('PricePickerCtrl', function($scope, PricePicker,
  PricePickerDataBuilder, $filter, $attrs, $parse) {
  var data = angular.copy($parse($attrs.pricePicker)($scope)),
      date = $parse($attrs.pricePickerDate)($scope);

  data.objects = new PricePickerDataBuilder(data.objects);

  $scope.pricePicker = new PricePicker(data, date);
})


.directive('pricePicker', function() {
  var directiveObject = {
    restrict: 'A',
    controller: 'PricePickerCtrl',
    scope: true,
    link: function($scope, $element, $attrs) {}
  };
  
  return directiveObject;
})
/**
 * Form datepicker module
 * ======================================================================== */

angular.module('form.datepicker', [])


.directive('formDatepicker', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    require: '?ngModel',

    link: function link($scope, $element, $attrs, ngModelCtrl) {
      if(!ngModelCtrl) return;
      var modelValue = $scope.$eval($attrs.ngModel);

      $element.pickadate({
        formatSubmit: 'yyyy-mm-dd',
        format: 'yyyy-mm-dd',
        hiddenName: false,
        editable: false,
        firstDay: 1,
        onStart: function() {
          this.set({
            'select': modelValue,
          });
        },
        // onSet: function(context) {
        //   var date = new Date(this.get('select').pick);
        //   console.log(this)

        //   // if(date)
        // }
      });

      var picker = $element.pickadate('picker');

      $scope.$watch($attrs.ngModel, function(value) {
        picker.set('select', value);
      });
    },
  };

  return directiveObject;
});
/**
 * Form value module
 * ======================================================================== */

angular.module('form.value', [])

.directive('formValue', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    require: '?ngModel',

    link: function link($scope, $element, $attrs, ngModelCtrl) {
      $scope.$watch($attrs.formValue, function(newVal) {
        ngModelCtrl.$setViewValue(newVal, angular.noop);
        ngModelCtrl.$render();
      });
    },
  };

  return directiveObject;
});
angular.module('filters', [])

.filter('toJson', function() {
  return function(data, pretty) {
    return angular.toJson(data, pretty);
  };
})

.filter('average', function() {
  return function(value) {
    if(angular.isArray(value)) {
      var i, l = value.length, sum = 0;

      for(i = 0; i < l; i++) {
        sum += value[i] | 0;
      }

      return sum / l;
    } else {
      return false;
    }
  };
})

.filter('startsWith', function() {
  return function(value, startsWith) {
    if(angular.isString(value)) {
      return value.indexOf(startsWith) === 0;
    } else {
      return false;
    }
  };
})

.filter('isFilled', function() {
  return function(value) {
    if(typeof value != 'undefined' && value !== null) {
      if(angular.isString(value) || angular.isArray(value)) {
        return value.length > 0;
      } else if(angular.isNumber(value)) {
        return true;
      }
    } else {
      return false;
    }
  };
});
angular.module('services.history', [])

.factory('History', function($state, $window) {
  var History = function() {};

  History.prototype.back = function() {
    $window.history.back();
  };

  return new History();
})

/* ========================================================================
 * Notification service
 * ======================================================================== */

angular.module('services.notifications', [])

.factory('notifications', function() {
  var Notification = function(parent) {
    /**
     * Notification item class.
     */
    this.parent = parent;
  };

  Notification.prototype.remove = function() {
    this.parent.remove(this);
  };

  var Notifications = function() {
    /**
     * Notifications list class.
     */
    this.list = [];
  };

  Notifications.prototype.add = function(notification) {
    /**
     * Adding notification to the notifications list.
     *
     * @param {object} notification : Notification object shoud look like this:
     *    {
     *      "type": "error|success|info|[custom]",
     *      "title": "Some title of the notification message",
     *      "content": "The notification message itself.",
     *      "cause": {Caused if exists}
     *    }
     * @returns void
     */

    if(!angular.isObject(notification)) {
      throw new Error("Only object can be added to the notification service");
    }

    this.list.push(angular.extend(new Notification(this), notification));
  };

  Notifications.prototype.remove = function(notification) {
    var n = this.list, pos = 0;

    if(angular.isObject(notification)) {
      pos = n.indexOf(notification);
    } else if(angular.isNumber(notification)) {
      pos = notification;
    }

    if(pos >= 0 && pos < n.length) {
      n.splice(pos, 1)
    }
  };

  Notifications.prototype.clean = function() {
    this.list.length = 0;
  };

  return new Notifications;
})

.controller('NotificationsCtrl', function($scope, notifications) {
  $scope.notifications = notifications;
})

.directive('notificationsList', function() {
  var directiveObject = {
    restrict: 'A',
    controller: 'NotificationsCtrl',
    link: function() {}
  };

  return directiveObject;
});
angular.module('services.formFactory', [
  'services.notifications',
  'services.api'
])

.factory('FormFactory', function(Api, notifications, $location, $parse) {
  function FormFactory(
    scope,
    formName,
    resourceName,
    callbacks
    ) {

    this.scope = scope;
    this.form = scope[formName];
    this.formName = formName;
    this.resourceName = resourceName;
    this.resource = $parse(resourceName);
    this.resourceCopy = angular.copy(this.resource(scope));


    this.errors = {};
    this.success = false;
    self.loading = false;
    this.callbacks = angular.extend({
      success: function() {},
      error: function() {},
    }, callbacks);
  };

  FormFactory.prototype.canSave = function () {
    return this.form.$valid && this.canRevert();
  };

  FormFactory.prototype.revertChanges = function () {
    if(this.canRevert()) {
      this.resource.assign(this.scope, angular.copy(this.resourceCopy));
    }
  };

  FormFactory.prototype.canRevert = function () {
    return !angular.equals(this.resource(this.scope), this.resourceCopy);
  };

  FormFactory.prototype.showError = function(fieldName, error) {
    return this.form[fieldName].$error[error];
  };


  /**
   * Data saving methods.
   */
  FormFactory.prototype.method = function(method, params) {
    var self = this,
        resource = this.resource(this.scope),
        attrs = [
          params,
          function() {
            var args = Array.prototype.slice.call(arguments);
            args.unshift(method);
            self.callbacks.success.apply(self, args);
            self.errors = {};
            self.success = true;
            self.loading = false;

            notifications.add({
              "type": "success",
              "content": "Данные успешно сохранены."
            });
          },
          function(data, status) {
            var args = Array.prototype.slice.call(arguments);
            args.unshift(method);
            self.callbacks.error.apply(self, args);
            self.errors = data;
            self.success = false;
            self.loading = false;

            var i, j, errors = data.data;
            if(angular.isObject(errors)) {
              for(i in errors) {
                if(!errors.hasOwnProperty(i)) continue;

                if(angular.isArray(errors[i])) {
                  for(j = 0; j < errors[i].length; j++) {
                    notifications.add({
                      "type": "error",
                      "title": "Поле " + i + ":",
                      "content": errors[i][j],
                    });
                  }
                } else if(angular.isString(errors[i])) {
                  notifications.add({
                    "type": "error",
                    "content": errors[i],
                  });
                }
              }

              notifications.add({
                "type": "error",
                "content": "Ошибки при заполнении формы:"
              });
            }
          }
        ];

    self.loading = true;

    if(typeof resource['$' + method] != 'undefined') {
      resource['$' + method].apply(resource, attrs);
    } else {
      throw new Error('There is no such method: "' 
        + method + '" in "' + this.resourceName + '" resource.');
    }
  };

  FormFactory.prototype.save = function(params) {
    if(this.resource(this.scope).id > 0) {
      this.method('update', params);
    } else {
      this.method('create', params);
    }
  };

  FormFactory.prototype.update = function(params) {
    this.method('update', params);
  };

  FormFactory.prototype.create = function(params) {
    this.method('create', params);
  };

  FormFactory.prototype.delete = function(params) {
    this.method('delete', params);
  };

  FormFactory.prototype.patch = function(params) {
    this.method('patch', params);
  };

  return FormFactory;
});
angular.module('services.httpRequestTracker', [])

.factory('httpRequestTracker', function($http){

  var HTTPRequestTracker = function() {};

  HTTPRequestTracker.prototype.hasPendingRequests = function() {
    return $http.pendingRequests.length > 0;
  };

  return new HTTPRequestTracker;
})

.controller('HTTPRequestTrackerCtrl', function($scope, httpRequestTracker) {
  $scope.httpRequestTracker = httpRequestTracker;
})

.directive('httpRequestTracker', function() {
  var directiveObject = {
    restrict: 'A',
    controller: 'HTTPRequestTrackerCtrl',
    link: function() {}
  };

  return directiveObject;
});
angular.module('services.httpErrorHandler', ['services.notifications'])

// register the ErrorHandler as a service
.factory('HTTPErrorHandler', function(
  $q,
  $log,
  notifications) {

  return {
   'responseError': function(rejection) {
      notifications.add({
        'title': 'HTTP ' + rejection.config.method + ' Error:',
        'type': 'error',
        'content': rejection.config.url + '\n'
          + rejection.status + ': ' + rejection.statusText,
      });

      return $q.reject(rejection);
    }
  };
})

.config(function($httpProvider) {
  $httpProvider.interceptors.push('HTTPErrorHandler');
});

angular.module('services.stateErrorHandler', ['services.exceptionHandler'])

.run(function($rootScope, $state) {
  $rootScope.$on(
    '$stateChangeError',
    function(event, toState, toParams, fromState, fromParams, error) {
      event.preventDefault();

      if('stack' in error) {
        throw error;
      }
      
      // throw new Error(error);
    });
})
angular.module('services.exceptionHandler', ['services.notifications'])

.factory('ExceptionHandlerFactory', function($injector) {
  return function($delegate) {

    return function (exception, cause) {
      // Lazy load notifications to get around circular dependency
      var notifications = $injector.get('notifications');

      // Pass through to original handler
      $delegate(exception, cause);

      // Push a notification error
      notifications.add({
        type: 'error',
        title: 'Error: ' + exception.message,
        content: exception.stack,
        cause: cause
      });
    };
  };
})

.config(function($provide) {
  $provide.decorator('$exceptionHandler', function (
    $delegate,
    ExceptionHandlerFactory) {
    return ExceptionHandlerFactory($delegate);
  });
});
angular.module('services.crudRouteProvider', [
  'ui.router',
  'app.conf',
])

.provider('crudRoute', function($stateProvider, pathTemlate) {

  // This $get noop is because at the moment in AngularJS "providers" must provide something
  // via a $get method.
  // When AngularJS has "provider helpers" then this will go away!
  this.$get = angular.noop;

  // Then we would have a much cleaner syntax and not have to do stuff
  // like:
  //
  // ```
  // myMod.config(function(crudRouteProvider) {
  //   var routeProvider = crudRouteProvider.routesFor('MyBook', '/myApp');
  // });
  // ```
  //
  // but instead have something like:
  //
  //
  // ```
  // myMod.config(function(crudRouteProvider) {
  //   var routeProvider = crudRouteProvider('MyBook', '/myApp');
  // });
  // ```

  this.routesFor = function(resourceName, urlPfixes) {
    var ex = angular.extend, copy = angular.copy;
    var state = resourceName.toLowerCase();
    var baseUrl = '/' + state;
    var promises = {
      list: function(Api, $stateParams) {
        return Api[state].query($stateParams).$promise;
      },
      single: function(Api, $stateParams) {
        return Api[state].get($stateParams).$promise;
      },
      create: function(Api, $stateParams) {
        return Api[state].get(angular.extend({id: 0, controller: 'related'},
            $stateParams)).$promise;
      },
    };

    if(angular.isString(urlPfixes[0]) && urlPfixes[0] !== '') {
      baseUrl = urlPfixes[0] + '/' + baseUrl;
    }

    // Append the postfix if it was provided;
    if(angular.isString(urlPfixes[1]) && urlPfixes[1] !== '') {
      baseUrl = baseUrl + urlPfixes[1];
    }

    var templatePath = pathTemlate + 'views/' + state + '/';

    var templateUrl = function(operation) {
      return templatePath + operation.toLowerCase() + '.html';
    };

    var controllerName = function(operation) {
      return function() {
        return resourceName + operation +'Ctrl'
      };
    };

    var stateBuilder = {
      // Create a route that will handle showing a list of items
      stateList: function(resolveFns) {
        stateBuilder.state(state, {
          url: baseUrl,
          views: {
            'view@': {
              dependencies: [],

              templateUrl: templateUrl('List'),
              controller: 'CrudListCtrl',
              resolve: ex({
                resourceName: function() { return resourceName; },
                controllerName: controllerName('List'),
                list: promises.list,
              }, resolveFns),
              reload: true,
            }
          }
        });
        return stateBuilder;
      },
      // Create a route that will handle editing an existing item
      stateDetail: function(resolveFns) {
        stateBuilder.state(state + '.detail', {
          url: '/detail/{id}',
          views: {
            'view@': {
              dependencies: ['id'],

              templateUrl: templateUrl('Detail'),
              controller: 'CrudSingleCtrl',
              resolve: ex({
                resourceName: function() { return resourceName; },
                controllerName: controllerName('Detail'),
                single: promises.single,
              }, resolveFns),
            }
          }
        });
        return stateBuilder;
      },
      // Create a route that will handle creating a new item
      stateCreate: function(resolveFns) {
        stateBuilder.state(state + '.create', {
          url: '/create',
          views: {
            'view@': {
              templateUrl: templateUrl('Create'),
              controller: 'CrudSingleCtrl',
              resolve: ex({
                resourceName: function() { return resourceName; },
                controllerName: controllerName('Create'),
                create: promises.create,
              }, resolveFns),
            }
          }
        });
        return stateBuilder;
      },
      // Pass-through to `$stateProvider.state()`
      state: function(name, definition) {
        $stateProvider.state(name, definition);
        return stateBuilder;
      },
      // Access to the core $stateProvider.
      $stateProvider: $stateProvider
    };

    return stateBuilder;
  };
})

.factory('controllerExtend', function($controller) {
  return function(controller, params) {
    try {
      $controller(controller, params);
    } catch (e) {
      if(!(e.message.indexOf('[ng:areq]') == 0)) {
        throw e;
      }
    }
  };
})

.factory('CroudSearch', function($state) {
  var CroudSearch = function() {
    this.params = $state.params || {};
    var i, l, searchParams = this.searchParams = [
        'ordering',
        'search',
        'status',
        'type_status',
        'is_active',
      ],
        searchParamsClean = this.searchParamsClean = {};

    for(i = 0, l = searchParams.length; i < l; i++) {
      searchParamsClean[searchParams[i]] = undefined;
    }
  };

  CroudSearch.prototype.submit = function($event) {
    $event.preventDefault();
    this.params.page = undefined;

    $state.transitionTo($state.current, this.params, {
      reload: true,
      inherit: true,
      notify: true
    });
  };

  CroudSearch.prototype.clean = function() {
    $state.transitionTo($state.current, this.searchParamsClean, {
      reload: true,
      inherit: true,
      notify: true
    });
  };

  CroudSearch.prototype.inProgress = function() {
    var i, l,
        params = this.params,
        searchParams = this.searchParams,
        is = false;

    for(i = 0, l = searchParams.length; i < l; i++) {
      if(is) break;

      if(typeof params[searchParams[i]] !== 'undefined') {
        if(!(params[searchParams[i]] === null
          || params[searchParams[i]] === '')) {
          is = true;
        }
      }
    }

    return is;
  };

  return CroudSearch;
})

.factory('CroudListActions', function($state, $window) {
  var CroudListActions = function() {
    this.selectors = {
      ids: {},
      master: false,
    };
  };

  CroudListActions.prototype.getIds = function() {
    var ids = [], i, from = this.selectors.ids, isNumber = angular.isNumber;

    for(i in from) {
      if(!from.hasOwnProperty(i)) continue;

      if(isNumber(from[i]) && from[i] == 1) {
        ids.push(i);
      }

      return ids;
    }
  };

  CroudListActions.prototype.apply = function(action) {
    var actions = {

    };

    if(action in actions) {
      
    }
  };

  return CroudListActions;
})


/**
 * Crud controllers
 * ======================================================================== */

/**
 * Crud list controller.
 */
.controller('CrudListCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  controllerExtend,
  CroudSearch,
  CroudListActions,
  Api,
  resourceName,
  controllerName,
  list
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;
  $scope.data = $state.current.data;
  $scope.Api = Api;
  $scope.controllerName = controllerName;
  $scope.resourceName = resourceName;

  $scope.list = list;
  $scope.config = list.config;

  $scope.search = new CroudSearch($stateParams);
  $scope.actions = new CroudListActions();

  controllerExtend(controllerName, {$scope: $scope});
})


/**
 * Crud single controller
 */
.controller('CrudSingleCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  controllerExtend,
  Api,
  resourceName,
  controllerName,
  single
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;
  $scope.data = $state.current.data;
  $scope.Api = Api;
  $scope.controllerName = controllerName;
  $scope.resourceName = resourceName;

  $scope.single = single;
  $scope.config = single.config;

  $scope.delete = function() {
    $scope.single.$delete({}, function() {
      $state.go('^', {}, {
          reload: true,
          inherit: true,
          relative: $state.$current,
          notify: true
        });
    });
  };

  controllerExtend(controllerName, {$scope: $scope});
});
