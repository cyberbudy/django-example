angular.module('app.conf', [])

// Path constants
.constant('pathTemlate', '/static/html/admin/')
.constant('pathResource', '/admin/ajax/v1/')
.constant('pathFormUpload', '/admin/ajax/v1/files/');
angular.module('app', [
  'ngRoute',
  'ngCookies',
  'ngResource',
  'ngSanitize',

  'ui.router',
  'ui.bootstrap.datepicker',
  'ui.bootstrap.tabs',

  'form',
  'filters',
  'services.api',
  'services.formFactory',
  'services.notifications',
  'services.history',
  'services.httpRequestTracker',
  'services.httpErrorHandler',
  'services.stateErrorHandler',
  'services.exceptionHandler',

  'app.conf',
  'admin.views',
  'admin.templates',
])

.constant('buttonConfig', {
  activeClass: '_checked',
  toggleEvent: 'click'
})

.constant('dropdownConfig', {
  openClass: '_active'
})

.config(function($interpolateProvider) {
  /**
   * Из-за django будем использовать <[ ]> вместо {{ }}.
   */
  $interpolateProvider.startSymbol('<[');
  $interpolateProvider.endSymbol(']>');
});
angular.module('services.api', [])

.service('Api', function($resource, $cookies, pathResource) {
  var self = this;

  var apiResource = function(url, paramDefaults, actions, options) {
    var ng = angular,
        methods = {
          query: {
            method: 'GET',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          all: {
            method: 'GET',
            params: {
              page_size: 9999999,
            },
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          delete: {
            method: 'DELETE',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          create: {
            method: 'POST',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          update: {
            method: 'PUT',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          patch: {
            method: 'PATCH',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
        };

    return $resource(
      pathResource + url,
      paramDefaults || {},
      ng.extend(ng.copy(methods), actions || {}),
      options
    );
  };

  var apiResourceDefault = function(name) {
    return apiResource(
      name + '/:id/:controller/?',
      {
        id: '@id',
        controller: '@controller',
      }
    );
  };

  /**
   * Resources definition.
   */
  this.hotels = apiResourceDefault('hotels');
    this.roomtypes = apiResourceDefault('roomtypes');
      this.roomtypes_rates = apiResourceDefault('roomtypes_rates');
      this.roomtypes_prices = apiResourceDefault('roomtypes_prices');
    this.contacts = apiResourceDefault('contacts');
    this.actions = apiResourceDefault('actions');
    this.reservations = apiResourceDefault('reservations');
    this.comments = apiResourceDefault('comments');

  this.pages = apiResourceDefault('pages');

  this.users = apiResourceDefault('users');
  this.groups = apiResourceDefault('groups');
  this.permissions = apiResourceDefault('permissions');

  this.labels = apiResourceDefault('labels');

  this.files = apiResourceDefault('files');

  this.cities = apiResourceDefault('cities');
  this.states = apiResourceDefault('states');

  this.config = apiResourceDefault('config');
  this.metadata = apiResourceDefault('metadata');

  this.mail_templates = apiResourceDefault('mail_templates');
});
angular.module('admin.templates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('template/datepicker/datepicker.html',
    "\n" +
    "<table class=\"datepicker\">\n" +
    "  <thead>\n" +
    "    <tr class=\"align-center\">\n" +
    "      <th>\n" +
    "        <button type=\"button\" ng-click=\"move(-1)\" class=\"button button_pull-right\"><span class=\"icon icon_fa-chevron-left\"></span></button><[ uniqueId ]>\n" +
    "      </th>\n" +
    "      <th colspan=\"<[ rows[0].length - 2 + showWeekNumbers ]>\">\n" +
    "        <button type=\"button\" ng-click=\"toggleMode()\" class=\"button button_block\"><strong><[ title ]></strong></button>\n" +
    "      </th>\n" +
    "      <th>\n" +
    "        <button type=\"button\" ng-click=\"move(1)\" class=\"button button_pull-left\"><span class=\"icon icon_fa-chevron-right\"></span></button>\n" +
    "      </th>\n" +
    "    </tr>\n" +
    "    <tr ng-show=\"labels.length > 0\" class=\"align-center\">\n" +
    "      <th ng-show=\"showWeekNumbers\">#</th>\n" +
    "      <th ng-repeat=\"label in labels\"><[ label ]></th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr ng-repeat=\"row in rows\">\n" +
    "      <td ng-show=\"showWeekNumbers\" class=\"align-center\"><em><[ getWeekNumber(row) ]></em></td>\n" +
    "      <td ng-repeat=\"dt in row\" class=\"align-center\">\n" +
    "        <button type=\"button\" style=\"width:100%;\" ng-class=\"{_active: dt.selected, _disabled: dt.disabled,            button_clean: dt.secondary, _focus: isActive(td)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" class=\"button\"><span ng-class=\"{muted: dt.secondary}\"><[ dt.label ]></span></button>\n" +
    "      </td>\n" +
    "    </tr>\n" +
    "  </tbody>\n" +
    "</table>"
  );


  $templateCache.put('template/datepicker/popup.html',
    "\n" +
    "<div ng-class=\"{_active: isOpen}\" class=\"dropdown-list\">\n" +
    "  <div class=\"dropdown-list__inner\">\n" +
    "    <div datepicker-dropdown=\"\" class=\"dropdown-list__body datepicker-popup\">\n" +
    "      <div ng-transclude=\"ng-transclude\"></div>\n" +
    "      <div class=\"datepicker-controls\">\n" +
    "        <div class=\"group\">\n" +
    "          <button ng-click=\"today()\" class=\"button button_primary button_small group__element group__element_first\">Сегодня</button>\n" +
    "          <button ng-click=\"clear()\" class=\"button button_red button_small group__element group__element_last\">Очистить</button>\n" +
    "        </div>\n" +
    "        <button ng-click=\"isOpen = false\" class=\"button button_clean button_small\">Закрыть</button>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>"
  );


  $templateCache.put('template/tabs/tab.html',
    "\n" +
    "<li ng-class=\"{_active: active, _disabled: disabled}\">\n" +
    "  <button type=\"button\" ng-click=\"select()\" ng-class=\"{_active: active, _disabled: disabled}\" tab-heading-transclude=\"\" class=\"tabs__item-toggler\"><[ heading ]></button>\n" +
    "</li>"
  );


  $templateCache.put('template/tabs/tabset-titles.html',
    "\n" +
    "<ul ng-class=\"{'nav-stacked': vertical}\" class=\"nav &lt;[ type &amp;&amp; 'nav-' + type ]&gt;\"></ul>"
  );


  $templateCache.put('template/tabs/tabset.html',
    "\n" +
    "<div class=\"tabs tabs_&lt;[ type ]&gt;\">\n" +
    "  <ul ng-class=\"{'_stacked': vertical, '_justified': justified}\" ng-transclude=\"\" class=\"tabs__togglers tabs__togglers_&lt;[ type ]&gt;\"></ul>\n" +
    "  <div class=\"tabs__contents tabs__contents_&lt;[ type ]&gt;\">\n" +
    "    <div ng-repeat=\"tab in tabs\" ng-class=\"{_active: tab.active}\" tab-content-transclude=\"tab\" class=\"tabs__item-content\"></div>\n" +
    "  </div>\n" +
    "</div>"
  );

}]);

angular.module('admin.views.conf', [])

.constant('viewListSearch', '?page&search&ordering');
angular.module('admin.views', [
  'admin.views.conf',

  'admin.views.pagination',

  'admin.views.labels',
  'admin.views.pages',
  'admin.views.cities',
  'admin.views.states',
  'admin.views.users',
  'admin.views.groups',
  'admin.views.hotels',
    'admin.views.roomtypes',
      'admin.views.roomtypes_rates',
      'admin.views.roomtypes_prices',
    'admin.views.contacts',
    'admin.views.comments',
  'admin.views.reservations',
  'admin.views.config',
  'admin.views.metadata',
  'admin.views.mail_templates',
  //   'admin.views.rooms',
  //   'admin.views.actions',


  'services.formFactory',
  'services.crudRouteProvider',
])

angular.module('admin.views')

/**
 * Root controller
 * ======================================================================== */
 
.controller('ViewRootCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  History
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;

  $scope.history = History;
})

/**
 * Sidebar controller
 * ======================================================================== */

.controller('ViewSidebarCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;

  $scope.setCurrentUser = function(id) {
    $rootScope.current_user_id = id;
  }
});
angular.module('admin.views.pagination', [])

.controller('PaginationCtrl', function($scope, $state, $stateParams) {
  // var pages = $scope.pages = new Array(8);

  // this.updatePagination = function() {
  //   if($scope.list) {
  //     if($scope.list.pagination) {
  //       var pg = $scope.list.pagination, i;

  //       if(pg.page > 1) {
  //         pages[0] = pg.page - 1;
  //         pages[1] = 1;
  //       }

  //       for(i = pg.page - 2; i < pg.page; i++) {
  //         if(i > 1) {
  //           pages[pg.page - 1 + 2] = i;
  //         }
  //       }

  //       if(pg.page > 1) {
  //         pages[0] = pg.page - 1;
  //         pages[1] = 1;
  //       }
  //     }
  //   }
  // }

  this.goToPage = function(page) {
    $scope.$emit('paginationChange', {page: page});

    $state.transitionTo($state.current,
      angular.extend(angular.copy($stateParams), {page: page}),
      {
        reload: true,
        inherit: true,
        notify: true
      });
  };
})

.directive('pagination', function() {
  var directiveObject = {
    restrict: 'A',
    controller: 'PaginationCtrl',
    scope: true,
    link: function() {},
  };

  return directiveObject;
})

.directive('paginationLink', function() {
  var directiveObject = {
    restrict: 'A',
    require: '^pagination',
    link: function($scope, $element, $attrs, controller) {
      if($scope.list) {
        if($scope.list.pagination) {
          $element.on('click', function paginationChange($event) {
            controller.goToPage($attrs.paginationLink);
            // if($scope.list.pagination.positions[$attrs.paginationLink]) {
            //   controller.goToPage(
            //     $scope.list.pagination.positions[$attrs.paginationLink]);
            // }
          });
        }
      }
    },
  };

  return directiveObject;
});

angular.module('admin.views.labels', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Labels', [, '/type/{type}' + viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams) {
        return new Api.labels({
          type: $stateParams.type,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('LabelsListCtrl', function(
  $scope
  ) {
})

.controller('LabelsDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('LabelsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('labels.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.pages', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Pages', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.pages({
          labels: [],
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('PagesListCtrl', function(
  $scope
  ) {
})

.controller('PagesDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('PagesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('pages.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.cities', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Cities', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.cities({
          state: null,
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('CitiesListCtrl', function(
  $scope
  ) {
})

.controller('CitiesDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('CitiesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('cities.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.states', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('States', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.states({});
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('StatesListCtrl', function(
  $scope
  ) {
})

.controller('StatesDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('StatesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('states.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.users', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Users', [, viewListSearch + '&is_active'])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.users({
          groups: [],
          permissions: [],
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('UsersListCtrl', function(
  $scope
  ) {
})

.controller('UsersDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('UsersCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('users.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.groups', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Groups', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.groups({
          permissions: [],
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('GroupsListCtrl', function(
  $scope
  ) {
})

.controller('GroupsDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('GroupsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('groups.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.hotels', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Hotels', [, viewListSearch + '&is_active'])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.hotels({
          labels: [],
          files: [],
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('HotelsListCtrl', function(
  $scope
  ) {
})

.controller('HotelsDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('HotelsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('hotels.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.roomtypes', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('RoomTypes', [, '/hotel/{hotel}'+ viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.roomtypes({
          hotel: $stateParams.hotel,
          labels: [],
          files: [],
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('RoomTypesListCtrl', function(
  $scope
  ) {
})

.controller('RoomTypesDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('RoomTypesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('roomtypes.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.roomtypes_rates', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('RoomTypes_Rates', [,
    '/hotel/{hotel}/roomtype/{roomtype}' + viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.roomtypes_rates({
          room_type: $stateParams.roomtype,
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('RoomTypes_RatesListCtrl', function(
  $scope
  ) {
})

.controller('RoomTypes_RatesDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('RoomTypes_RatesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('roomtypes_rates.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.roomtypes_prices', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('RoomTypes_Prices', [,
    '/hotel/{hotel}/roomtype/{roomtype}' + viewListSearch + '&year&month'])
    .stateList();
})


/**
 * Controllers
 * ======================================================================== */

.controller('RoomTypes_PricesListCtrl', function(
  $scope,
  $state,
  FormFactory,
  Api,
  $filter
  ) {

  $scope.monthNames = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
  ];

  var p = $scope.list.pagination,
      nextMonth = p.month == 12 ? 1 : (p.month + 1),
      prevMonth = p.month > 1 ? p.month - 1 : 12;

  p.next = {
    year: nextMonth > p.month ? p.year : p.year + 1,
    month: nextMonth,
  };
  p.prev = {
    year: prevMonth < p.month ? p.year : p.year - 1,
    month: prevMonth,
  };

  $scope.single = new Api.roomtypes_prices({
    date_from: null,
    date_to: null,
    rate: null,
    room_type: $scope.$stateParams.roomtype,
  });

  $scope.pricePickerDate = $filter('date')(
    new Date($scope.list.pagination.year, $scope.list.pagination.month - 1, 1),
    'yyyy-MM-dd');

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('roomtypes_prices', {}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.contacts', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Contacts', [, '/hotel/{hotel}'+ viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.contacts({
          hotel: $stateParams.hotel,
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('ContactsListCtrl', function(
  $scope
  ) {
})

.controller('ContactsDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('ContactsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('contacts.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.comments', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Comments', [, '/hotel/{hotel}'+ viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.comments({
          hotel: $stateParams.hotel,
          vote_criteria: [0,0,0,0,0,0],
          user: null,
          parent: null,
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('CommentsListCtrl', function(
  $scope
  ) {
})

.controller('CommentsDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('CommentsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('comments.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.reservations', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Reservations', [,
    viewListSearch + '&hotel&manager&status&type_status'])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.reservations({
          hotel: $stateParams.hotel,
          user: null,
          manager: null,
          internal: true,
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('ReservationsListCtrl', function(
  $scope
  ) {
})

.controller('ReservationsDetailCtrl', function(
  $scope,
  $rootScope,
  FormFactory
  ) {

  $scope.manage = function() {
    $scope.single.manager = $rootScope.current_user_id;
    $scope.single.$update({}, function() {
      $scope.Form.success = true;
    });
  };

  $scope.unManage = function() {
    $scope.single.manager = null;
    $scope.single.$update({}, function() {
      $scope.Form.success = true;
    });
  };

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('ReservationsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('reservations.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.config', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Config', [])
    .stateList();
})


/**
 * Controllers
 * ======================================================================== */

.controller('ConfigListCtrl', function(
  $scope,
  $state,
  FormFactory,
  Api,
  $filter
  ) {

  $scope.single = new Api.config({
    config: angular.extend({
        slug_articles: '',
      }, angular.copy($scope.list.config)),
  });

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('config', {}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.metadata', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Metadata', [])
    .stateList();
})


/**
 * Controllers
 * ======================================================================== */

.controller('MetadataListCtrl', function(
  $scope,
  $state,
  FormFactory,
  Api,
  $filter
  ) {

  $scope.single = new Api.metadata({
    metadata: angular.extend({
        slug_articles: '',
      }, angular.copy($scope.list.metadata)),
  });

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('metadata', {}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});
angular.module('admin.views.mail_templates', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Mail_Templates', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams) {
        return new Api.mail_templates({});
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('Mail_TemplatesListCtrl', function(
  $scope
  ) {
})

.controller('Mail_TemplatesDetailCtrl', function(
  $scope,
  $state,
  $filter,
  Api,
  FormFactory
  ) {

  var filterUsers = function(newVal) {
    $scope.sender.usersFiltered = $filter('filter')(
      $scope.single.related_data_all.users, 
      function(item) {
        if(typeof newVal === 'undefined' || newVal === null) {
          return true;
        }

        return item.groups.indexOf(newVal) >= 0;
      });
  };

  $scope.sender = {
    filter: null,
    usersFiltered: [],
  };

  $scope.$watch('sender.filter', filterUsers);
  filterUsers($scope.sender.filter);

  $scope.send = new Api.mail_templates({
    id: $scope.single.id,
    users: [],
  });

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
  $scope.FormSend = new FormFactory($scope, 'senderForm', 'send', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('mail_templates.detail', {id: $scope.single.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
})

.controller('Mail_TemplatesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('mail_templates.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});