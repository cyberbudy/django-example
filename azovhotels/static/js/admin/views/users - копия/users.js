angular.module('admin.views.users', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Users', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.users({
          groups: [],
          permissions: [],
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('UsersListCtrl', function(
  $scope
  ) {
})

.controller('UsersDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', $scope.single);
})

.controller('UsersCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', $scope.single, {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('users.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});