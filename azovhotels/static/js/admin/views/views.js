angular.module('admin.views', [
  'admin.views.conf',

  'admin.views.pagination',

  'admin.views.labels',
  'admin.views.pages',
  'admin.views.cities',
  'admin.views.states',
  'admin.views.users',
  'admin.views.groups',
  'admin.views.hotels',
    'admin.views.roomtypes',
      'admin.views.roomtypes_rates',
      'admin.views.roomtypes_prices',
    'admin.views.contacts',
    'admin.views.comments',
  'admin.views.reservations',
  'admin.views.config',
  'admin.views.metadata',
  'admin.views.mail_templates',
  //   'admin.views.rooms',
  //   'admin.views.actions',


  'services.formFactory',
  'services.crudRouteProvider',
])
