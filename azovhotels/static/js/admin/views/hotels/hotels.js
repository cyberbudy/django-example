angular.module('admin.views.hotels', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Hotels', [, viewListSearch + '&is_active'])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.hotels({
          labels: [],
          files: [],
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('HotelsListCtrl', function(
  $scope
  ) {
})

.controller('HotelsDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('HotelsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('hotels.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});