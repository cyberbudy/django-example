angular.module('app.conf', [])

// Path constants
.constant('pathTemlate', '/static/html/admin/')
.constant('pathResource', '/admin/ajax/v1/')
.constant('pathFormUpload', '/admin/ajax/v1/files/');