angular.module('services.formFactory', [
  'services.notifications',
  'services.api'
])

.factory('FormFactory', function(Api, notifications, $location, $parse) {
  function FormFactory(
    scope,
    formName,
    resourceName,
    callbacks
    ) {

    this.scope = scope;
    this.form = scope[formName];
    this.formName = formName;
    this.resourceName = resourceName;
    this.resource = $parse(resourceName);
    this.resourceCopy = angular.copy(this.resource(scope));


    this.errors = {};
    this.success = false;
    self.loading = false;
    this.callbacks = angular.extend({
      success: function() {},
      error: function() {},
    }, callbacks);
  };

  FormFactory.prototype.canSave = function () {
    return this.form.$valid && this.canRevert();
  };

  FormFactory.prototype.revertChanges = function () {
    if(this.canRevert()) {
      this.resource.assign(this.scope, angular.copy(this.resourceCopy));
    }
  };

  FormFactory.prototype.canRevert = function () {
    return !angular.equals(this.resource(this.scope), this.resourceCopy);
  };

  FormFactory.prototype.showError = function(fieldName, error) {
    return this.form[fieldName].$error[error];
  };


  /**
   * Data saving methods.
   */
  FormFactory.prototype.method = function(method, params) {
    var self = this,
        resource = this.resource(this.scope),
        attrs = [
          params,
          function() {
            var args = Array.prototype.slice.call(arguments);
            args.unshift(method);
            self.callbacks.success.apply(self, args);
            self.errors = {};
            self.success = true;
            self.loading = false;

            notifications.add({
              "type": "success",
              "content": "Данные успешно сохранены."
            });
          },
          function(data, status) {
            var args = Array.prototype.slice.call(arguments);
            args.unshift(method);
            self.callbacks.error.apply(self, args);
            self.errors = data;
            self.success = false;
            self.loading = false;

            var i, j, errors = data.data;
            if(angular.isObject(errors)) {
              for(i in errors) {
                if(!errors.hasOwnProperty(i)) continue;

                if(angular.isArray(errors[i])) {
                  for(j = 0; j < errors[i].length; j++) {
                    notifications.add({
                      "type": "error",
                      "title": "Поле " + i + ":",
                      "content": errors[i][j],
                    });
                  }
                } else if(angular.isString(errors[i])) {
                  notifications.add({
                    "type": "error",
                    "content": errors[i],
                  });
                }
              }
            }

            notifications.add({
              "type": "error",
              "content": "Ошибки при заполнении формы:"
            });
          }
        ];

    self.loading = true;

    console.log(this.resource)

    if(typeof resource['$' + method] != 'undefined') {
      resource['$' + method].apply(resource, attrs);
    } else {
      throw new Error('There is no such method: "' 
        + method + '" in "' + this.resourceName + '" resource.');
    }
  };

  FormFactory.prototype.save = function(params) {
    if(this.resource(this.scope).id > 0) {
      this.method('update', params);
    } else {
      this.method('create', params);
    }
  };

  FormFactory.prototype.update = function(params) {
    this.method('update', params);
  };

  FormFactory.prototype.create = function(params) {
    this.method('create', params);
  };

  FormFactory.prototype.delete = function(params) {
    this.method('delete', params);
  };

  FormFactory.prototype.patch = function(params) {
    this.method('patch', params);
  };

  return FormFactory;
});