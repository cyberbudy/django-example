# -*- coding: utf-8 -*-

from django import template

from azovhotels.serializers.admin.files import get_file_urls

register = template.Library()


@register.filter(name='access')
def access(value, arg):
    """
    Accessing dictionary by key in django templates
    """
    try:
        return value[arg]
    except:
        try:
            return getattr(value, arg)
        except:
            return ''


@register.filter(name='url_image')
def url_image(file, size):
    try:
        urls = get_file_urls(file)
        
        return urls[size]
    except:
        return ''


@register.filter(name='get_range')
def get_range(value):
  """
    Filter - returns a list containing range made from given value
    Usage (in template):

    <ul>{% for i in 3|get_range %}
      <li>{{ i }}. Do something</li>
    {% endfor %}</ul>

    Results with the HTML:
    <ul>
      <li>0. Do something</li>
      <li>1. Do something</li>
      <li>2. Do something</li>
    </ul>

    Instead of 3 one may use the variable set in the views
  """
  return range(int(value if value else 0))


@register.filter(name='upto')
def upto(value, arg):
  value = int(value)
  arg = value

  if value % arg > 0:
    return value + (arg - (value % arg))
  else:
    return value


@register.filter(name='subtract')
def subtract(value, arg):
    return int(value if value else 0) - int(arg if arg else 0)


@register.filter(name='divide')
def divide(value, arg):
  return int(value) / int(arg)


@register.filter(name='multiple')
def multiple(value, arg):
  return int(value) * int(arg)


@register.filter(name='concatenate')
def concatenate(value, arg):
  return ''.join((str(value), str(arg)))