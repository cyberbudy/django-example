# -*- coding: utf-8 -*-

from django.db import models as m

from .base import BaseModel


class State(BaseModel):
    name = m.CharField(max_length = 150, null=False)
    slug = m.SlugField(unique=True, max_length = 150, null=False)

    class Meta:
        app_label = 'azovhotels'