from django.db import models as m

from .base import BaseModel
from .files import File
from .hotels import Hotel
from .room_types import RoomType
from .room_type_rates import RoomTypeRate



class RoomTypePrice(BaseModel):    
    price = m.DecimalField(max_digits=9, decimal_places=2)
    available_rooms = m.IntegerField(null=True, blank=True)
    date = m.DateField()

    room_type = m.ForeignKey(RoomType)
    rate = m.ForeignKey(RoomTypeRate)       

    class Meta:
        app_label = 'azovhotels'
        unique_together = (('room_type', 'date'),)
