from django.db import models as m

from .base import BaseModel
from .room_types import RoomType
from .hotels import Hotel



class Room(BaseModel):    
    name = m.CharField(max_length = 150, null = False)
    slug = m.SlugField(max_length = 150, unique = True, null = False)
    
    room_type = m.ForeignKey(RoomType)
    hotel = m.ForeignKey(Hotel)

    class Meta:
        app_label = 'azovhotels'