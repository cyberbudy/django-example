# -*- coding: utf-8 -*-

from datetime import datetime

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin,\
    BaseUserManager
from django.core.mail import send_mail
from django.db import models as m

from .base import BaseModel


class UserManager(BaseUserManager):
    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = datetime.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, is_staff=is_staff, is_active=True,
            is_superuser=is_superuser, last_login=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user


    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)


    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)



class UserAbstract(BaseModel, AbstractBaseUser, PermissionsMixin):
    email = m.EmailField(blank=True, max_length=254, unique=True)

    first_name = m.CharField(max_length = 150, null=True, blank=True)
    middle_name = m.CharField(max_length = 150, null=True, blank=True)
    last_name = m.CharField(max_length = 150, null=True, blank=True)

    phone = m.CharField(max_length = 150, null=True, blank=True)
    type = m.CharField(max_length = 20, null=True, blank=True)

    passport_series = m.CharField(max_length = 150, null=True, blank=True)
    passport_code = m.CharField(max_length = 150, null=True, blank=True)
    passport_issued = m.CharField(max_length = 150, null=True, blank=True)

    send_sms = m.BooleanField(default=True, blank=True)
    send_email = m.BooleanField(default=True, blank=True)

    is_staff = m.BooleanField(default=False)
    is_active = m.BooleanField(default=True)

    blocked_till = m.DateTimeField(null=True, blank=True)

    objects = UserManager()
    backend = 'django.contrib.auth.backends.ModelBackend'

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []


    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        abstract = True


    def get_full_name(self):
        """
        Returns the email.
        """
        return self.email


    def get_short_name(self):
        """
        Returns the email.
        """
        return self.email


    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])


    def is_authenticated(self):
        return True


    def is_hotelier(self):
        return self.has_perm('azovhotels.change_hotel')


    def is_tourist(self):
        return self.is_authenticated()
    


class User(UserAbstract):
    """
    Concrete class of UserAbstract.

    Use this if you don't need to extend UserAbstract.
    """
    class Meta(UserAbstract.Meta):
        app_label= 'azovhotels'
        swappable = 'AUTH_USER_MODEL'