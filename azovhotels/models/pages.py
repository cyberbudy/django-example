from django.db import models as m

from .base import BaseModel
from .labels import Label


class Page(BaseModel):    
    title = m.CharField(max_length=150, null=False)
    slug = m.SlugField(max_length=150, unique=True, null=False)
    content = m.TextField(max_length=250, null=True, blank=True)

    template = m.CharField(max_length=150, null=True, blank=True)
    
    labels = m.ManyToManyField(Label)


    class Meta:
        app_label = 'azovhotels'