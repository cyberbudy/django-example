# -*- coding: utf-8 -*-
from django.db import models as m

from .base import BaseModel
from .hotels import Hotel



class Contact(BaseModel):    
    first_name = m.CharField(max_length = 150, null = False)
    middle_name = m.CharField(max_length = 150, null = True, blank=True)
    last_name = m.CharField(max_length = 150, null = True, blank=True)

    sex = m.NullBooleanField(null=True)
    position = m.CharField(max_length = 100, null = True, blank=True)

    phone = m.CharField(max_length = 150, null = True, blank=True)
    email = m.CharField(max_length = 150, null = True)

    hotel = m.ForeignKey(Hotel)


    class Meta:
        app_label = 'azovhotels'