# -*- coding: utf-8 -*-

from django.db import models as m

from .base import BaseModel


class Meta(BaseModel):
    key = m.SlugField(max_length = 150, null=False)
    description = m.TextField(null=True, default=None, blank=True)

    class Meta:
        app_label = 'azovhotels'