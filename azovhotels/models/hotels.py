# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models as m
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator

from .base import BaseModel
from .cities import City
from .labels import Label
from .files import File



class Hotel(BaseModel):    
    title = m.CharField(max_length=150, null=False)
    slug = m.SlugField(max_length=150, unique=True, null=False, blank=False)

    # Another two text fields for some other information.
    about = m.TextField(null=True, blank=True)
    about_short = m.TextField(null=True, blank=True)
    conditions_location = m.TextField(null=True, blank=True)
    conditions_driveway = m.TextField(null=True, blank=True)
    conditions_guest_accommodation = m.TextField(null=True, blank=True)
    conditions_tourist_tax = m.TextField(null=True, blank=True)
    conditions_internet = m.TextField(null=True, blank=True)
    conditions_feeding = m.TextField(null=True, blank=True)
    conditions_deposit = m.TextField(null=True, blank=True)
    conditions_parking = m.TextField(null=True, blank=True)
    conditions_pets = m.TextField(null=True, blank=True)
    conditions_medicine = m.TextField(null=True, blank=True)

    gps_latitude = m.CharField(max_length=25, null=True, blank=True)
    gps_longitude = m.CharField(max_length=25, null=True, blank=True)

    is_active = m.NullBooleanField(default=False, blank=True) # Three states field

    votes_weight = m.IntegerField(default=0)
    votes_count = m.IntegerField(default=0)
    promote_count = m.IntegerField(default=0) # Promotion counter
    stars_category = m.IntegerField(null=False, default=1, blank=True) # Hotel stars 5*, 4* etc.
    rooms_count = m.IntegerField(default=0)

    postal_code = m.CharField(max_length=150, null=True, blank=True)
    address = m.CharField(max_length=150, null=True, blank=True)
    phone = m.CharField(max_length=150, null=True, blank=True)
    site = m.CharField(max_length=150, null=True, blank=True)

    # Requisites
    entity_name = m.CharField(max_length=150, null=True, blank=True)    
    entity_address = m.CharField(max_length=150, null=True, blank=True)
    entity_egrpou = m.CharField(max_length=150, null=True, blank=True)
    entity_payment_account = m.CharField(max_length=150, null=True, blank=True)
    entity_payment_bank_name = m.CharField(max_length=150, null=True, blank=True)
    entity_payment_bank_mfo = m.CharField(max_length=150, null=True, blank=True)
    entity_card_account = m.CharField(max_length=150, null=True, blank=True)
    entity_card_bank_name = m.CharField(max_length=150, null=True, blank=True)
    entity_card_bank_mfo = m.CharField(max_length=150, null=True, blank=True)
    entity_corporate_inn = m.CharField(max_length=150, null=True, blank=True)

    working_from = m.DateField(default=datetime.strptime('01-01-2014',
        '%d-%m-%Y'), null=False, blank=True)
    working_till = m.DateField(default=datetime.strptime('31-12-2014',
        '%d-%m-%Y'), null=False, blank=True)

    arrival_from = m.CharField(max_length=10, default='12.00', null=False, blank=True)
    arrival_till = m.CharField(max_length=10, default='12.00', null=False, blank=True)

    reservation_cancellation_days = m.IntegerField(null=True, blank=True)
    reservation_cancellation_about = m.TextField(null=True, blank=True)
    reservation_period = m.IntegerField(null=False, default=1,
        validators=[MinValueValidator(0), MaxValueValidator(32)], blank=True)
    reservation_prepayment_type = m.BooleanField(default=False, blank=True) # 0 - daily, 1 - percent
    reservation_prepayment_daily = m.IntegerField(default=0)
    reservation_prepayment_percent = m.IntegerField(default=0)

    user = m.ForeignKey(settings.AUTH_USER_MODEL)
    city = m.ForeignKey(City)
    labels = m.ManyToManyField(Label)
    files = m.ManyToManyField(File)
    view_file = m.ForeignKey(File, related_name='view', null=True, blank=True)

       

    class Meta:
        app_label = 'azovhotels'