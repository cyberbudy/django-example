# -*- coding: utf-8 -*-

from django.db import models as m
from django.conf import settings

from .base import BaseModel


class File(BaseModel):
    title = m.CharField(max_length = 100, null=False)
    slug = m.SlugField(max_length = 100, unique=True, null=False)
    
    description = m.TextField(null=True, blank=True)

    type = m.CharField(max_length=120, null=True, blank=True)
    file = m.FileField(upload_to='.')

    deleted_at = m.DateTimeField(null=True, blank=True)


    class Meta:
        app_label = 'azovhotels'