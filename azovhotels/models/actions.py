from django.db import models as m

from .base import BaseModel

from .hotels import Hotel
from .labels import Label



class Action(BaseModel):    
    title = m.CharField(max_length = 150, null = False)
    slug = m.SlugField(max_length = 150, unique = True, null = False)
    content = m.TextField(max_length = 250, null = True)

    started_at = m.DateTimeField()
    ended_at = m.DateTimeField()

    hotel = m.ForeignKey(Hotel)
    labels = m.ManyToManyField(Label)


    class Meta:
        app_label = 'azovhotels'