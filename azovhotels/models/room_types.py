from django.db import models as m

from .base import BaseModel
from .files import File
from .hotels import Hotel
from .labels import Label


class RoomType(BaseModel):    
    title = m.CharField(max_length=150, blank=False)
    slug = m.SlugField(max_length=150, unique=True, blank=False)
    about = m.TextField(null=True, blank=True)
    included = m.TextField(null=True, blank=True)
    excluded = m.TextField(null=True, blank=True)

    price = m.DecimalField(max_digits=9, decimal_places=2)
    beds = m.IntegerField()
    
    rooms_available = m.IntegerField(default=0, null=False, blank=False)
    rooms_total = m.IntegerField(default=0, null=False, blank=False)

    hotel = m.ForeignKey(Hotel, related_name='room_types', blank=False)
    files = m.ManyToManyField(File)
    labels = m.ManyToManyField(Label)
    view_file = m.ForeignKey(File, related_name='view_file', null=True, blank=True)
    

    class Meta:
        app_label = 'azovhotels'
