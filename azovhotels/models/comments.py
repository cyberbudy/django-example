# -*- coding: utf-8 -*-
from django.db import models as m
from django.conf import settings

from .base import BaseModel
from .hotels import Hotel



class Comment(BaseModel):
    content = m.TextField(max_length = 550)

    vote_criteria = m.CharField(max_length = 100, blank=True)
    vote_mark = m.DecimalField(max_digits=9, decimal_places=3, null=False)
    
    hotel = m.ForeignKey(Hotel)
    user = m.ForeignKey(settings.AUTH_USER_MODEL)
    parent = m.ForeignKey('self', null=True, blank=True)

       

    class Meta:
        app_label = 'azovhotels'