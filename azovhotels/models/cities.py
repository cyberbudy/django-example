# -*- coding: utf-8 -*-

from django.db import models as m

from .states import State
from .base import BaseModel


class City(BaseModel):
    name = m.CharField(max_length = 150, null=False)
    slug = m.SlugField(unique=True, max_length = 150, null=False)

    state = m.ForeignKey(State, null=True, blank=True)

    class Meta:
        app_label = 'azovhotels'