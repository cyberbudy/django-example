# -*- coding: utf-8 -*-

from django.db import models as m

from .actions import Action
from .cities import City
from .comments import Comment
from .contacts import Contact
from .files import File
from .hotels import Hotel
from .labels import Label
from .pages import Page
from .reservations import Reservation
from .room_type_prices import RoomTypePrice
from .room_type_rates import RoomTypeRate
from .room_types import RoomType
from .rooms import Room
from .users import User
from .states import State

from .meta import Meta
from .mail_templates import MailTemplate
