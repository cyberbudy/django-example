# -*- coding: utf-8 -*-
from django.db import models as m
from django.conf import settings

from .base import BaseModel
from .hotels import Hotel
from .rooms import Room
from .room_types import RoomType



class Reservation(BaseModel):    
    first_name = m.CharField(max_length=150, null=False)
    middle_name = m.CharField(max_length=150, null=True)
    last_name = m.CharField(max_length=150, null=True)

    passport_series = m.CharField(max_length=50, null=True)
    passport_code = m.CharField(max_length=50, null=True)
    passport_issued = m.CharField(max_length=150, null=True)

    phone = m.CharField(max_length=150, null=True)
    price = m.DecimalField(max_digits=9, decimal_places=2)
    prepayment_sum = m.DecimalField(max_digits=9, decimal_places=2)

    starting_at = m.DateField()
    ending_at = m.DateField()

    room_index = m.IntegerField(null=False)

    hotel = m.ForeignKey(Hotel)
    # room = m.ForeignKey(Room)
    room_type = m.ForeignKey(RoomType)
    user = m.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True,
        related_name='user')
    manager = m.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True,
        related_name='manager')

    # arrived = m.BooleanField()
    # not_arrived = m.BooleanField()
    # cancelled = m.BooleanField()
    # payment_expected = m.BooleanField()
    # arrived_expected = m.BooleanField()

    # reservation types
    payed = m.BooleanField(default=False)
    cancelled = m.BooleanField(default=False)
    arrived = m.BooleanField(default=False)
    internal = m.BooleanField(default=True)


    RESERVATION_STATUS_CHOICES = {
        'FF': {'id': 'FF', 'label': u'ожидается проплата Гостя'},
        'FE': {'id': 'FE', 'label': u'отмена пользователем'},
        'FD': {'id': 'FD', 'label': u'ожидается заезд Гостя'},
        'FC': {'id': 'FC', 'label': u'отменено без штрафа'},
        'FB': {'id': 'FB', 'label': u'аннулировано системой'},
        'FA': {'id': 'FA', 'label': u'незаезд'},
        'F9': {'id': 'F9', 'label': u'проживание'},
    }

    RESERVATION_STATUS_TRIGGERS = {
        'FF': {
            'payed': False,
            'cancelled': False,
            'arrived': False,
        },
        'FC': {
            'cancelled': True,
        },
        'FD': {
            'payed': True,
            'cancelled': False,
            'arrived': False,
        },
        'FE': {
            'cancelled': True,
        },
        'FB': {
            'cancelled': True,
        },
        'FA': {
            'cancelled': True,
            'arrived': False,
        },
        'F9': {
            'arrived': True,
        },
    }

    reservation_status = m.CharField(max_length=2, null=False,
        choices=[(i, RESERVATION_STATUS_CHOICES[i]['label'])\
            for i in RESERVATION_STATUS_CHOICES],
        default=RESERVATION_STATUS_CHOICES['FF']['label'])
       

    class Meta:
        app_label = 'azovhotels'