from django.db import models as m

from .base import BaseModel
from .files import File
from .hotels import Hotel
from .room_types import RoomType



class RoomTypeRate(BaseModel):    
    about = m.CharField(max_length=150, null=True, blank=True)
    price = m.DecimalField(max_digits=9, decimal_places=2)
    
    room_type = m.ForeignKey(RoomType)       

    class Meta:
        app_label = 'azovhotels'
