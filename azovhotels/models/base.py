from django.db import models as m


class BaseModel(m.Model):
    created_at = m.DateTimeField(auto_now_add = True, blank = True)
    updated_at = m.DateTimeField(auto_now = True, blank = True)

    class Meta:
        abstract = True