# -*- coding: utf-8 -*-

from django.db import models as m

from .base import BaseModel

class Label(BaseModel):
    title = m.CharField(max_length = 150, null=False)
    slug = m.CharField(max_length = 150, unique=True, null=False)
    description = m.TextField(null=True, default=None, blank=True)

    type = m.CharField(max_length=150, null=False)

    class Meta:
        app_label = 'azovhotels'