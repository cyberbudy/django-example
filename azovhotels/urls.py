# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import patterns, include, url

from rest_framework import routers
import azovhotels.views.admin as admin
import azovhotels.views.site.hotelier as hotelier
import azovhotels.views.site.tourist as tourist
import azovhotels.views.site as site
import azovhotels.models as m



admin_router = routers.DefaultRouter()

admin_router.register(r'hotels', admin.HotelListView, base_name='admin-api.hotels')
admin_router.register(r'cities', admin.CityListView, base_name='admin-api.cities')
admin_router.register(r'states', admin.StateListView, base_name='admin-api.states')
admin_router.register(r'users', admin.UserListView, base_name='admin-api.users')
admin_router.register(r'files', admin.FileListView, base_name='admin-api.files')
admin_router.register(r'labels', admin.LabelListView, base_name='admin-api.labels')
admin_router.register(r'pages', admin.PageListView, base_name='admin-api.pages')
admin_router.register(r'contacts', admin.ContactListView, base_name='admin-api.contacts')
admin_router.register(r'comments', admin.CommentListView, base_name='admin-api.comments')
admin_router.register(r'actions', admin.ActionListView, base_name='admin-api.actions')
admin_router.register(r'roomtypes', admin.RoomTypeListView, base_name='admin-api.roomtypes')
admin_router.register(r'roomtypes_prices', admin.RoomTypePriceListView, base_name='admin-api.roomtypes_prices')
admin_router.register(r'roomtypes_rates', admin.RoomTypeRateListView, base_name='admin-api.roomtypes_rates')
admin_router.register(r'reservations', admin.ReservationListView, base_name='admin-api.reservations')
admin_router.register(r'groups', admin.GroupListView, base_name='admin-api.groups')
admin_router.register(r'permissions', admin.PermissionListView, base_name='admin-api.permissions')

admin_router.register(r'config', admin.ConfigView, base_name='admin-api.config')
admin_router.register(r'metadata', admin.MetaDataView, base_name='admin-api.metadata')
admin_router.register(r'mail_templates', admin.MailTemplateListView, base_name='admin.api-mail_templates')


hotelier_router = routers.DefaultRouter()

hotelier_router.register(r'hotels', hotelier.HotelListView, base_name='hotelier.api-hotels')
hotelier_router.register(r'contacts', hotelier.ContactListView, base_name='hotelier.api-contacts')
hotelier_router.register(r'roomtypes', hotelier.RoomTypeListView, base_name='hotelier.api-roomtypes')
hotelier_router.register(r'actions', hotelier.ActionListView, base_name='hotelier.api-actions')
hotelier_router.register(r'files', hotelier.FileListView, base_name='hotelier.api-files')
hotelier_router.register(r'roomtypes_prices', hotelier.RoomTypePriceView, base_name='hotelier.api-roomtypes_prices')
hotelier_router.register(r'roomtypes_rates', hotelier.RoomTypeRateListView, base_name='hotelier.api-roomtypes_rates')


tourist_router = routers.DefaultRouter()

# tourist_router.register(r'reservations', tourist.ReservationListView, base_name='reservations')
tourist_router.register(r'profile', tourist.UserProfileView, base_name='profile')
# tourist_router.register(r'comments', tourist.CommentListView, base_name='comments')

# router = routers.DefaultRouter()
# router.register(r'hotels', site.HotelView, base_name='site.hotels')

hotels_list = site.HotelView.as_view({
    'get': 'list',
    'post': 'list'
})

hotels_detail = site.HotelView.as_view({
    'get': 'retrieve',
    'post': 'retrieve',
    'patch': 'retrieve',
    'delete': 'destroy'
})

articles_list = site.ArticleView.as_view({
    'get': 'list',
})

articles_detail = site.ArticleView.as_view({
    'get': 'retrieve',
})

pages_list = site.PageView.as_view({
    'get': 'list',
   
})

pages_detail = site.PageView.as_view({
    'get': 'retrieve',
    })


urlpatterns = patterns('azovhotels.views',

    url('', include(patterns('azovhotels.views.auth',
        url(r'^login/$', 'login', name='auth.login'),
        url(r'^logout/$', 'logout', name='auth.logout'),

        url(r'^auth/reset/$', 'reset', name='auth.reset'),
        url(r'^auth/reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            'reset_confirm', name='password_reset_confirmp'),
        url(r'^auth/reset-success/$', 'reset_success', name='auth.reset.success'),
    ))),

    url(r'^make-reservation/', 'site.reservations.reservation_user'),
    url(r'^make-reservation-details/', 'site.reservations.reservation_details', name='reservation-details'),

    # All admin and admin ajax views goes here.
    url('', include(patterns('azovhotels.views.site',
        url(r'^$', 'main.index', name='site'),
        # url(r'^pg/$', 'main.pg'),
        # url(r'^main/$', 'main.index'),

        url(r'^hotels/(?P<id>\d+)-(?P<slug>[\w-]+)/$', hotels_detail, name='site.hotels-detail'),
        url(r'^hotels/$', hotels_list, name='site.hotels-list'),

        # url(r'^articles/(?P<id>\d+)-(?P<slug>[\w-]+)/$', articles_detail, name='site.articles-detail'),
        url(r'^articles/$', articles_list, name='site.articles-list'),

        url(r'^pages/(?P<id>\d+)-(?P<slug>[\w-]+)/$', pages_detail, name='site.pages-detail'),
        # url(r'^pages/$', pages_list, name='site.pages-list'),
        ))),

    url(r'^tourist-register/$', 'site.register.tourist_register'),
    url(r'^hotelier-register/$', 'site.register.hotelier_register'),

    # All site views goes here:
    url(r'^hotelier/', include(patterns('azovhotels.views.site.hotelier',
        url(r'^$', 'main.root', name='hotelier'),
        url(r'^register/$', 'main.register', name='hotelier-register'),

        url(r'^ajax/v1/', include(hotelier_router.urls)),
        ))),

    url(r'^tourist/', include(tourist_router.urls)),

    url(r'^tourist/', include(patterns('azovhotels.views.site.tourist',
        url(r'^comments/$', 'comments.comment_list'),
        url(r'^comments/(?P<pk>[0-9]+)$', 'comments.comment_detail'),

        url(r'^reservations/$', 'reservations.reservation_list'),
        ))),
        

    # All admin and admin ajax views goes here.
    url(r'^admin/', include(patterns('azovhotels.views.admin',
        url(r'^$', 'main.root', name='admin'),

        # Those urlconf is for admin ajax views(json api provided by django
        # rest framework)
        url(r'^ajax/v1/', include(admin_router.urls)),

        ))),

    )

# if settings.DEBUG:
#     import debug_toolbar
#     urlpatterns += patterns('',
#         url(r'^__debug__/', include(debug_toolbar.urls)),
#     )
