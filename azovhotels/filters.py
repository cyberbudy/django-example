# -*- coding: utf-8 -*-

from django import forms

import django_filters as df


class NullBooleanFilter(df.Filter):
    field_class = forms.Field

    def filter(self, qs, value):
        cases = {
            'null': None,
            'false': False,
            'true': True,
            '0': False,
            '1': True,
            0: False,
            1: True,
        }

        return qs.filter(**{self.name: cases.get(value, value)})