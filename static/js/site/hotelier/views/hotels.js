angular.module('hotelier.views.hotels', [
  'hotelier.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  $urlRouterProvider,
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Hotels', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.hotels({
          labels: [],
          files: [],
          related_data_all: create.related_data_all,
        });
      }
    });

  $urlRouterProvider.otherwise('/hotels');
})


/**
 * Controllers
 * ======================================================================== */

.controller('HotelsListCtrl', function(
  $scope
  ) {
})

.controller('HotelsDetailCtrl', function(
  $scope,
  $state,
  notifications,
  Api,
  FormFactory
  ) {

  $scope.delete = angular.noop;
  $scope.activate = function() {
    Api.hotels.get({
      id: $scope.single.id,
      controller: 'set_active',
    }, function(data) {
      notifications.add({
        'type': 'success',
        'content': 'Заявка принята на рассмотрение.',
      });

      $state.transitionTo('hotels.detail', {id: $scope.single.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    });
  };

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('HotelsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('hotels.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});