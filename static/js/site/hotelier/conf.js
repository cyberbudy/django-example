angular.module('app.conf', [])

// Path constants
.constant('pathTemlate', '/static/html/site/hotelier/')
.constant('pathResource', '/hotelier/ajax/v1/')
.constant('pathFormUpload', '/hotelier/ajax/v1/files/');