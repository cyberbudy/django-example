/* ========================================================================
 * Hoteiler application
 * ======================================================================== */

angular.module('app', [
  'ngRoute',
  'ngCookies',
  'ngResource',
  'ngSanitize',

  'ui.router',
  'ui.bootstrap.datepicker',
  'ui.bootstrap.tabs',

  'form',
  'filters',
  'services.api',
  'services.formFactory',
  'services.notifications',
  'services.history',
  'services.httpRequestTracker',
  'services.httpErrorHandler',
  'services.stateErrorHandler',
  'services.exceptionHandler',

  'app.conf',
  'hotelier.views',
  'admin.templates',
])

.constant('buttonConfig', {
  activeClass: '_checked',
  toggleEvent: 'click'
})

.constant('dropdownConfig', {
  openClass: '_active'
})

.config(function($interpolateProvider) {
  /**
   * Из-за django будем использовать <[ ]> вместо {{ }}.
   */
  $interpolateProvider.startSymbol('<[');
  $interpolateProvider.endSymbol(']>');
});