(function() {
  var datepickerStart = $('#datepicker_search_start');
  var datepickerEnd = $('#datepicker_search_end');
  var dt = {end: false};

  var datepickerStartPicker = datepickerStart.pickadate({
    formatSubmit: 'dd.mm.yyyy',
    hiddenName: true,
    editable: false,
    firstDay: 1,
    min: true,
    onSet: function(context) {
      var picker = datepickerEnd.pickadate('picker');
      var date = new Date(context.select);
      var cDateEnd = picker.get('select');
      var dateEnd = new Date(new Date(context.select)
        .setMonth(date.getMonth() + 1));

      picker.set({
        min: date,
        max: dateEnd
      });

      picker.render(true);
    }
  });

  var datepickerEndPicker = datepickerEnd.pickadate({
    formatSubmit: 'dd.mm.yyyy',
    hiddenName: true,
    editable: false,
    firstDay: 1,
    min: true,
    max: dt.end,
  });
})();