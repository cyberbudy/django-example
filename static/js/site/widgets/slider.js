/* ========================================================================
 * Angular css slider widget
 * ======================================================================== */

angular.module('widgets.slider', [])

.controller('WidgetSliderCtrl', function($scope) {
  $scope.count = 0;
  $scope.position = 0;
  $scope.next = 0;
  $scope.previous = 0;

  $scope.setPos = function(pos, direction) {
    if(direction) {
      if(pos >= $scope.count) {
        pos = 0;
      } else {
        pos++;
      }
    } else {
      if(pos > 0) {
        pos--;
      } else {
        pos = $scope.count;
      }
    }

    return pos;
  };
})

.directive('widgetsSlider', function() {
  return {
    restrict: 'A',
    controller: 'WidgetSliderCtrl',
    scope:true,
    link: function($scope, $element, $attrs) {
      $scope.count = $attrs.widgetsSlider | 0;
      $scope.next = $scope.setPos($scope.position, 1);
      $scope.previous = $scope.setPos($scope.position, 0);
    }
  };
})

.directive('widgetsSliderNext', function() {
  return {
    restrict: 'A',
    scope:true,
    link: function($scope, $element, $attrs) {
      $scope.direction = 1;
      $scope.position = $scope.setPos($scope.position, 1);
      $scope.next = $scope.setPos($scope.position, 1);
      $scope.previous = $scope.setPos($scope.position, 0);
    }
  };
})

.directive('widgetsSliderPrev', function() {
  return {
    restrict: 'A',
    scope:true,
    link: function($scope, $element, $attrs) {
      $scope.direction = 0;
      $scope.position = $scope.setPos($scope.position, 0);
      $scope.next = $scope.setPos($scope.position, 0);
      $scope.previous = $scope.setPos($scope.position, 1);
    },
  };
});