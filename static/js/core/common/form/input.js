/**
 * Form input module
 * ======================================================================== */

angular.module('form.input', [])

.controller('FormInputCtrl', function ($scope) {
  var o = ($scope.formInputObject = {});
})

.directive('formInput', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    controller: 'FormInputCtrl',

    link: function link($scope, $element, $attrs, controller) {},
  };

  return directiveObject;
})

.directive('formInputField', function() {
  var directiveObject = {
    restrict: 'EA',
    require: '^formInput',
    scope: false,

    link: function link($scope, $element, $attrs, controller) {},
  };

  return directiveObject;
});