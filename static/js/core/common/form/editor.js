angular.module('form.editor', ['ngCkeditor'])

.directive('formEditorOptions', function() {
  var directiveObject = {
    restrict: 'A',
    scope: true,
    link: function($scope, $element, $attrs) {
      var attr = $attrs.formEditorOptions, e, options = {};

      if(typeof attr != 'undefined' && attr != null && attr != '') {
        try {
          // options = angular.fromJson(attr);
          options = $scope.$eval(attr);
        } catch(e) {
          throw e;
        }
      }

      $scope.editorOptions = options;
    },
  };

  return directiveObject;
});