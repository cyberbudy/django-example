/**
 * Form slugify field module
 * ======================================================================== */

angular.module('form.slugify', [])

.factory('FormSlugify', function() {
  var FormSlugify = function(dash) {
    var dash = dash;

    var chars = {
      'а': 'a',
      'б': 'b',
      'в': 'v',
      'г': 'g',
      'д': 'd',
      'е': 'e',
      'ё': 'e',
      'ж': 'zh',
      'з': 'z',
      'и': 'i',
      'й': 'i',
      'к': 'k',
      'л': 'l',
      'м': 'm',
      'н': 'n',
      'о': 'o',
      'п': 'p',
      'р': 'r',
      'с': 's',
      'т': 't',
      'у': 'u',
      'ф': 'f',
      'х': 'h',
      'ц': 'c',
      'ч': 'ch',
      'ш': 'sh',
      'щ': 'shch',
      'ъ': '',
      'ы': 'y',
      'ь': '',
      'э': 'e',
      'ю': 'yu',
      'я': 'ya',
    }

    var from = [];

    for(i in chars) {
      if(!chars.hasOwnProperty(i)) continue;

      from.push(i);
    }

    this.chars = chars;
    this.dash = dash;
    this.from = from;
  };

  FormSlugify.prototype.slugify = function(str) {
    var str = str || '',
        dash = this.dash,
        from = this.from,
        chars = this.chars;

    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    for (var i = 0, l = from.length; i < l; i++) {
      str = str.replace(new RegExp(from[i], 'g'), chars[from[i]]);
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, dash) // collapse whitespace and replace by -
      .replace(new RegExp(dash + '+', 'g'), dash); // collapse dashes

    return str;
  };

  return FormSlugify;
})

.factory('formSlugify', function(FormSlugify) {
  return new FormSlugify('-');
})

.directive('formSlugify', function(formSlugify) {
  var directiveObject = {
    restrict: 'A',
    scope: {
      model: '=ngModel',
      slug: '=formSlugify',
    },

    link: function link($scope, $element, $attrs) {
      if(!$scope.slug) {
        $scope.slug = formSlugify.slugify($scope.model);
      }
      $scope.$watch('model', function(newValue, oldValue) {
        if($scope.slug == formSlugify.slugify(oldValue) || !$scope.slug) {
          $scope.slug = formSlugify.slugify(newValue);
        }
      });
    },
  };

  return directiveObject;
});