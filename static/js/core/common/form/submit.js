/**
 * Form submit module
 * ======================================================================== */

angular.module('form.submit', [])

.directive('formSubmit', function () {
  var directiveObject = {
    restrict: 'A',

    link: function link($scope, $element, $attrs) {
      $element.on('submit', function($event) {
        var name = $attrs.name, form = $scope[name];

        if(form.$valid) {
          $scope.$eval($attrs.formSubmit);
        } else {
          $scope.$apply(function() {
            form.$submitTry = true;
          });

          var i, firstInvalid = false;

          for(i in form) {
            if(!form.hasOwnProperty(i) ||
              typeof form[i].$valid == 'undefined') continue;

            if(!form[i].$valid) {
              firstInvalid = document.forms[name].elements.namedItem(i);
              break;
            }
          }

          if(firstInvalid) {
            firstInvalid.focus();
          }
        }
      });
    },
  };

  return directiveObject;
});