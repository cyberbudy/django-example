angular.module('services.httpErrorHandler', ['services.notifications'])

// register the ErrorHandler as a service
.factory('HTTPErrorHandler', function(
  $q,
  $log,
  notifications) {

  return {
   'responseError': function(rejection) {
      notifications.add({
        'title': 'HTTP ' + rejection.config.method + ' Error:',
        'type': 'error',
        'content': rejection.config.url + '\n'
          + rejection.status + ': ' + rejection.statusText,
      });

      return $q.reject(rejection);
    }
  };
})

.config(function($httpProvider) {
  $httpProvider.interceptors.push('HTTPErrorHandler');
});
