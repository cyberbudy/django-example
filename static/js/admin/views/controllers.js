angular.module('admin.views')

/**
 * Root controller
 * ======================================================================== */
 
.controller('ViewRootCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  History
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;

  $scope.history = History;
})

/**
 * Sidebar controller
 * ======================================================================== */

.controller('ViewSidebarCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;

  $scope.setCurrentUser = function(id) {
    $rootScope.current_user_id = id;
  }
});