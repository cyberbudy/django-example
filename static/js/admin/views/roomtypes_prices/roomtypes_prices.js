angular.module('admin.views.roomtypes_prices', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('RoomTypes_Prices', [,
    '/hotel/{hotel}/roomtype/{roomtype}' + viewListSearch + '&year&month'])
    .stateList();
})


/**
 * Controllers
 * ======================================================================== */

.controller('RoomTypes_PricesListCtrl', function(
  $scope,
  $state,
  FormFactory,
  Api,
  $filter
  ) {

  $scope.monthNames = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
  ];

  var p = $scope.list.pagination,
      nextMonth = p.month == 12 ? 1 : (p.month + 1),
      prevMonth = p.month > 1 ? p.month - 1 : 12;

  p.next = {
    year: nextMonth > p.month ? p.year : p.year + 1,
    month: nextMonth,
  };
  p.prev = {
    year: prevMonth < p.month ? p.year : p.year - 1,
    month: prevMonth,
  };

  $scope.single = new Api.roomtypes_prices({
    date_from: null,
    date_to: null,
    rate: null,
    room_type: $scope.$stateParams.roomtype,
  });

  $scope.pricePickerDate = $filter('date')(
    new Date($scope.list.pagination.year, $scope.list.pagination.month - 1, 1),
    'yyyy-MM-dd');

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('roomtypes_prices', {}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});