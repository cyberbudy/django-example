angular.module('admin.views.contacts', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Contacts', [, '/hotel/{hotel}'+ viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.contacts({
          hotel: $stateParams.hotel,
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('ContactsListCtrl', function(
  $scope
  ) {
})

.controller('ContactsDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('ContactsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('contacts.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});