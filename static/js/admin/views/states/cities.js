angular.module('admin.views.cities', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Cities', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.cities({});
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('CitiesListCtrl', function(
  $scope
  ) {
})

.controller('CitiesDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', $scope.single);
})

.controller('CitiesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', $scope.single, {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('cities.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});