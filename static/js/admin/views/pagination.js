angular.module('admin.views.pagination', [])

.controller('PaginationCtrl', function($scope, $state, $stateParams) {
  // var pages = $scope.pages = new Array(8);

  // this.updatePagination = function() {
  //   if($scope.list) {
  //     if($scope.list.pagination) {
  //       var pg = $scope.list.pagination, i;

  //       if(pg.page > 1) {
  //         pages[0] = pg.page - 1;
  //         pages[1] = 1;
  //       }

  //       for(i = pg.page - 2; i < pg.page; i++) {
  //         if(i > 1) {
  //           pages[pg.page - 1 + 2] = i;
  //         }
  //       }

  //       if(pg.page > 1) {
  //         pages[0] = pg.page - 1;
  //         pages[1] = 1;
  //       }
  //     }
  //   }
  // }

  this.goToPage = function(page) {
    $scope.$emit('paginationChange', {page: page});

    $state.transitionTo($state.current,
      angular.extend(angular.copy($stateParams), {page: page}),
      {
        reload: true,
        inherit: true,
        notify: true
      });
  };
})

.directive('pagination', function() {
  var directiveObject = {
    restrict: 'A',
    controller: 'PaginationCtrl',
    scope: true,
    link: function() {},
  };

  return directiveObject;
})

.directive('paginationLink', function() {
  var directiveObject = {
    restrict: 'A',
    require: '^pagination',
    link: function($scope, $element, $attrs, controller) {
      if($scope.list) {
        if($scope.list.pagination) {
          $element.on('click', function paginationChange($event) {
            controller.goToPage($attrs.paginationLink);
            // if($scope.list.pagination.positions[$attrs.paginationLink]) {
            //   controller.goToPage(
            //     $scope.list.pagination.positions[$attrs.paginationLink]);
            // }
          });
        }
      }
    },
  };

  return directiveObject;
});
