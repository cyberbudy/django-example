angular.module('admin.views.metadata', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Metadata', [])
    .stateList();
})


/**
 * Controllers
 * ======================================================================== */

.controller('MetadataListCtrl', function(
  $scope,
  $state,
  FormFactory,
  Api,
  $filter
  ) {

  $scope.single = new Api.metadata({
    metadata: angular.extend({
        slug_articles: '',
      }, angular.copy($scope.list.metadata)),
  });

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('metadata', {}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});