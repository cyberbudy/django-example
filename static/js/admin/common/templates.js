angular.module('admin.templates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('template/datepicker/datepicker.html',
    "\n" +
    "<table class=\"datepicker\">\n" +
    "  <thead>\n" +
    "    <tr class=\"align-center\">\n" +
    "      <th>\n" +
    "        <button type=\"button\" ng-click=\"move(-1)\" class=\"button button_pull-right\"><span class=\"icon icon_fa-chevron-left\"></span></button><[ uniqueId ]>\n" +
    "      </th>\n" +
    "      <th colspan=\"<[ rows[0].length - 2 + showWeekNumbers ]>\">\n" +
    "        <button type=\"button\" ng-click=\"toggleMode()\" class=\"button button_block\"><strong><[ title ]></strong></button>\n" +
    "      </th>\n" +
    "      <th>\n" +
    "        <button type=\"button\" ng-click=\"move(1)\" class=\"button button_pull-left\"><span class=\"icon icon_fa-chevron-right\"></span></button>\n" +
    "      </th>\n" +
    "    </tr>\n" +
    "    <tr ng-show=\"labels.length > 0\" class=\"align-center\">\n" +
    "      <th ng-show=\"showWeekNumbers\">#</th>\n" +
    "      <th ng-repeat=\"label in labels\"><[ label ]></th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr ng-repeat=\"row in rows\">\n" +
    "      <td ng-show=\"showWeekNumbers\" class=\"align-center\"><em><[ getWeekNumber(row) ]></em></td>\n" +
    "      <td ng-repeat=\"dt in row\" class=\"align-center\">\n" +
    "        <button type=\"button\" style=\"width:100%;\" ng-class=\"{_active: dt.selected, _disabled: dt.disabled,            button_clean: dt.secondary, _focus: isActive(td)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" class=\"button\"><span ng-class=\"{muted: dt.secondary}\"><[ dt.label ]></span></button>\n" +
    "      </td>\n" +
    "    </tr>\n" +
    "  </tbody>\n" +
    "</table>"
  );


  $templateCache.put('template/datepicker/popup.html',
    "\n" +
    "<div ng-class=\"{_active: isOpen}\" class=\"dropdown-list\">\n" +
    "  <div class=\"dropdown-list__inner\">\n" +
    "    <div datepicker-dropdown=\"\" class=\"dropdown-list__body datepicker-popup\">\n" +
    "      <div ng-transclude=\"ng-transclude\"></div>\n" +
    "      <div class=\"datepicker-controls\">\n" +
    "        <div class=\"group\">\n" +
    "          <button ng-click=\"today()\" class=\"button button_primary button_small group__element group__element_first\">Сегодня</button>\n" +
    "          <button ng-click=\"clear()\" class=\"button button_red button_small group__element group__element_last\">Очистить</button>\n" +
    "        </div>\n" +
    "        <button ng-click=\"isOpen = false\" class=\"button button_clean button_small\">Закрыть</button>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>"
  );


  $templateCache.put('template/tabs/tab.html',
    "\n" +
    "<li ng-class=\"{_active: active, _disabled: disabled}\">\n" +
    "  <button type=\"button\" ng-click=\"select()\" ng-class=\"{_active: active, _disabled: disabled}\" tab-heading-transclude=\"\" class=\"tabs__item-toggler\"><[ heading ]></button>\n" +
    "</li>"
  );


  $templateCache.put('template/tabs/tabset-titles.html',
    "\n" +
    "<ul ng-class=\"{'nav-stacked': vertical}\" class=\"nav &lt;[ type &amp;&amp; 'nav-' + type ]&gt;\"></ul>"
  );


  $templateCache.put('template/tabs/tabset.html',
    "\n" +
    "<div class=\"tabs tabs_&lt;[ type ]&gt;\">\n" +
    "  <ul ng-class=\"{'_stacked': vertical, '_justified': justified}\" ng-transclude=\"\" class=\"tabs__togglers tabs__togglers_&lt;[ type ]&gt;\"></ul>\n" +
    "  <div class=\"tabs__contents tabs__contents_&lt;[ type ]&gt;\">\n" +
    "    <div ng-repeat=\"tab in tabs\" ng-class=\"{_active: tab.active}\" tab-content-transclude=\"tab\" class=\"tabs__item-content\"></div>\n" +
    "  </div>\n" +
    "</div>"
  );

}]);
