var path = {
  styl: 'styl/',
  css: 'css/',
  jade: 'jade/',
  html: 'html/',
  ts: 'ts/',
  js: 'js/',
  img: 'img/',

  fonts: 'font/',

  django: 'azovhotels/templates/',
  dev: 'dev/static/',
  dist: 'azovhotels/static/',
};

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jade: {
      static: {
        options: {
          pretty: true,
        },
        files: [{
          expand: true,
          cwd: path.dev + path.jade + 'static/',
          src: '**/*.jade',
          dest: path.dist + path.html,
          ext: '.html',
        }],
      },

      django: {
        options: {
          pretty: true,
        },
        files: [{
          expand: true,
          cwd: path.dev + path.jade + 'django/',
          src: '**/*.jade',
          dest: path.django,
          ext: '.html',
        }],
      }
    },

    stylus: {
      dev: {
        options: {
          compress: false,
          linenos: false,
          'include css': true,
        },
        files: [{
          expand: true,
          cwd: path.dev + path.styl,
          src: ['admin.styl'],
          dest: path.dev + path.css,
          ext: '.css',
        }]
      },
      dist: {
        options: {
          compress: false,
          linenos: false,
          'include css': true,
        },
        files: [{
          expand: true,
          cwd: path.dev + path.styl,
          src: ['admin/style.styl', 'site/style.styl'],
          dest: path.dist + path.css,
          ext: '.css',
        }]
      }
    },

    ngtemplates:  {
      admin: {
        cwd: path.dist + path.html + 'admin/',
        src: 'template/**/*.html',
        dest: path.dist + path.js + 'admin/common/templates.js',
        options: {
          module: 'admin.templates',
          standalone: true,
          // htmlmin: {
          //   collapseWhitespace: true,
          //   collapseBooleanAttributes: true,
          // },
        },
      },
    },

    autoprefixer: {
      dist: {
        options: {
          diff: false,
          map: false,
        },
        expand: true,
        cwd: path.dist + path.css,
        src: '**/*.css',
        dest: path.dist + path.css,
      },
    },

    concat: {
      adminVendor: {
        src: (function() {
          var cwd = path.dev + path.js + 'vendor/';
          var src = [
            // 'es5-shim/es5-shim.js',
            // 'es6-shim/es6-shim.js',
            // 'jquery/dist/jquery.js',
            // 'angular/angular.js',
            // 'angular-cookies/angular-cookies.js',
            // 'angular-resource/angular-resource.js',
            // 'angular-route/angular-route.js',
            // 'angular-sanitize/angular-sanitize.js',
            // 'angular-ui-router/release/angular-ui-router.js',
            // 'angular-bootstrap/ui-bootstrap.js',
            // 'angular-ui-utils/angular-ui-utils.js',
            // 'angular-file-upload/angular-file-upload.js',
            // 'ng-ckeditor/ng-ckeditor.js',
            'es5-shim/es5-shim.min.js',
            'es6-shim/es6-shim.min.js',
            'jquery/dist/jquery.min.js',
            'pickadate/lib/compressed/legacy.js',
            'pickadate/lib/compressed/picker.js',
            'pickadate/lib/compressed/picker.date.js',
            'pickadate/lib/compressed/picker.time.js',
            'pickadate/lib/compressed/translations/ru_RU.js',
            'angular/angular.min.js',
            'angular-sanitize/angular-sanitize.min.js',
            'angular-cookies/angular-cookies.min.js',
            'angular-resource/angular-resource.min.js',
            'angular-route/angular-route.min.js',
            'angular-ui-router/release/angular-ui-router.min.js',
            'angular-bootstrap/ui-bootstrap.min.js',
            'angular-ui-utils/angular-ui-utils.min.js',
            'angular-file-upload/angular-file-upload.min.js',
            'ng-ckeditor/ng-ckeditor.min.js',
          ];

          return src.map(function(file) { return cwd + file; });
        }()),
        dest: path.dist + path.js + 'admin/vendor.js',
      },

      adminModules: {
        src: (function() {
          var cwd = path.dist + path.js + 'admin/';
          var src = [
            'conf.js',
            'app.js',

            'services/Api.js',

            'common/templates.js',

            'views/conf.js',
            'views/views.js',
            'views/controllers.js',
            'views/pagination.js',
            'views/labels/labels.js',
            'views/pages/pages.js',
            'views/cities/cities.js',
            'views/states/states.js',
            'views/users/users.js',
            'views/groups/groups.js',
            'views/hotels/hotels.js',
            'views/roomtypes/roomtypes.js',
            'views/roomtypes_rates/roomtypes_rates.js',
            'views/roomtypes_prices/roomtypes_prices.js',
            'views/contacts/contacts.js',
            'views/comments/comments.js',
            'views/reservations/reservations.js',
            'views/config/config.js',
            'views/metadata/metadata.js',
            'views/mail_templates/mail_templates.js',
          ];

          return src.map(function(file) { return cwd + file; });
        }()),
        dest: path.dist + path.js + 'admin/modules.js',
      },

    coreModules: {
        src: (function() {
          var cwd = path.dist + path.js + 'core/';
          var src = [
            'common/utils.js',
            'common/form/form.js',
            'common/form/element.js',
            'common/form/input.js',
            'common/form/select.js',
            'common/form/checker.js',
            'common/form/upload.js',
            'common/form/slugify.js',
            'common/form/submit.js',
            'common/form/editor.js',
            'common/form/files-list.js',
            'common/form/validators.js',
            'common/form/datafill.js',
            'common/form/price-picker.js',
            'common/form/datepicker.js',
            'common/form/value.js',
            'filters.js',
            'services/History.js',
            'services/Notifications.js',
            'services/FormFactory.js',
            'services/HTTPRequestTracker.js',
            'services/HTTPErrorHandler.js',
            'services/StateErrorHandler.js',
            'services/ExceptionHandler.js',
            'services/CrudRouteProvider.js',
          ];

          return src.map(function(file) { return cwd + file; });
        }()),
        dest: path.dist + path.js + 'core/modules.js',
      },

      siteVendor: {
        src: (function() {
          var cwd = path.dev + path.js + 'vendor/';
          var src = [
            'es5-shim/es5-shim.min.js',
            'es6-shim/es6-shim.min.js',
            'jquery/dist/jquery.min.js',
            'lightbox/js/lightbox.min.js',
            'pickadate/lib/compressed/legacy.js',
            'pickadate/lib/compressed/picker.js',
            'pickadate/lib/compressed/picker.date.js',
            'pickadate/lib/compressed/picker.time.js',
            'pickadate/lib/compressed/translations/ru_RU.js',
            'angular/angular.min.js',
            'angular-sanitize/angular-sanitize.min.js',
            'angular-cookies/angular-cookies.min.js',
            'angular-resource/angular-resource.min.js',
            'angular-route/angular-route.min.js',
            // 'angular-ui-router/release/angular-ui-router.min.js',
            'angular-bootstrap/ui-bootstrap.min.js',
            // 'angular-ui-utils/angular-ui-utils.min.js',
            // 'angular-file-upload/angular-file-upload.min.js',
            // 'ng-ckeditor/ng-ckeditor.min.js',
          ];

          return src.map(function(file) { return cwd + file; });
        }()),
        dest: path.dist + path.js + 'site/vendor.js',
      },
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: path.dist + path.img,
          src: '**/*.{png,jpg,gif}',
          dest: path.dist + path.img,
        }]
      }
    },

    copy: {
      css: {
        files: [{
          expand: true,
          cwd: path.dev + path.css,
          src: ['**/*.css'],
          dest: path.dist + path.css,
        }]
      },
      js: {
        files: [{
          expand: true,
          cwd: path.dev + path.js,
          src: ['**/*.js', '!vendor/**'],
          dest: path.dist + path.js,
        }]
      },
      fonts: {
        files: [{
          expand: true,
          cwd: path.dev + path.fonts,
          src: '**/*.{eot,otf,swg,ttf,woff}',
          dest: path.dist + path.fonts,
        }]
      },
      images: {
        files: [{
          expand: true,
          cwd: path.dev + path.img,
          src: '**/*.{png,jpg,gif}',
          dest: path.dist + path.img,
        }]
      },
    },

    watch: {
      options: {
        livereload: false,
      },
      stylus: {
        files: path.dev + path.styl + '**/*.styl',
        tasks: ['stylus:dist', 'autoprefixer:dist'],
        options: {
          spawn: false,
        },
      },
      jade: {
        files: path.dev + path.jade + 'static/**/*.jade',
        tasks: ['newer:jade:static', 'ngtemplates'],
        options: {
          spawn: false,
        },
      },
      jadeDjango: {
        files: path.dev + path.jade + 'django/**/*.jade',
        tasks: ['newer:jade:django'],
        options: {
          spawn: false,
        },
      },
      copy: {
        files: path.dev + path.js + '**/*.js',
        tasks: ['newer:copy:js'],
        options: {
          spawn: false,
        },
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-autoprefixer');
  
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('default', []);
  grunt.registerTask('dist', [
    'jade:static',
    'jade:django',

    'copy:css',
    'stylus:dist',
    'autoprefixer:dist',

    'copy:js',
    'concat:adminVendor',
    'concat:adminModules',
    'concat:coreModules',
    'ngtemplates:admin',

    'copy:images',
    'copy:fonts',
  ]);
};