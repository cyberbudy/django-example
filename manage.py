#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    dirname = os.path.dirname(__file__)

    if os.path.isfile(os.path.join(dirname, 'azovhotels', 'settings_production.py')):
        sys.path.append('/home/p/pavlovpr/public_html/_site-packages')
        sys.path.append('/home/p/pavlovpr/public_html')
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "azovhotels.settings_production")
    elif os.path.isfile(os.path.join(dirname, 'azovhotels', 'settings_local.py')):
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "azovhotels.settings_local")
    else:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "azovhotels.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
