#!/usr/bin/python

import os, sys

sys.path.append('/home/p/pavlovpr/public_html/_site-packages')
sys.path.append('/home/p/pavlovpr/public_html')
os.environ['DJANGO_SETTINGS_MODULE'] = 'azovhotels.settings_production' 

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()


# import os, sys 
# sys.path.append('/usr/local/django') 
# sys.path.append('/_site-packages/django') 
# os.environ['DJANGO_SETTINGS_MODULE'] = 'azovhotels.settings'

# import django.core.handlers.wsgi 

# application = django.core.handlers.wsgi.WSGIHandler()