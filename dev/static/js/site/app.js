var app = angular.module('app', [
  'ngRoute',
  'ngCookies',
  'ngResource',
  'widgets',

  'ui.bootstrap',
  'form.element',
  'form.input',
  'form.checker',
  // 'form.upload',
  // 'form.slugify',
  // 'form.submit',
  // 'form.select',
  // 'admin.form.range',
  // 'ui-rangeSlider',
  // 'admin.form.templates',
  // 'admin.form.files-list',

  // 'ui.codemirror',
  // 'ui.bootstrap.datepicker',
])

.config(function($interpolateProvider) {
  /**
   * Из-за django будем использовать <[ ]> вместо {{ }}.
   */
  $interpolateProvider.startSymbol('<[');
  $interpolateProvider.endSymbol(']>');
});