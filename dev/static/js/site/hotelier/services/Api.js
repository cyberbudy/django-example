angular.module('services.api', [])

.service('Api', function($resource, $cookies, pathResource) {
  var self = this;

  var apiResource = function(url, paramDefaults, actions, options) {
    var ng = angular,
        methods = {
          query: {
            method: 'GET',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          all: {
            method: 'GET',
            params: {
              page_size: 9999999,
            },
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          delete: {
            method: 'DELETE',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          create: {
            method: 'POST',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          update: {
            method: 'PUT',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
          patch: {
            method: 'PATCH',
            headers: {
              'X-CSRFToken': function() { return $cookies['csrftoken']; },
            },
          },
        };

    return $resource(
      pathResource + url,
      paramDefaults || {},
      ng.extend(ng.copy(methods), actions || {}),
      options
    );
  };

  var apiResourceDefault = function(name) {
    return apiResource(
      name + '/:id/:controller/?',
      {
        id: '@id',
        controller: '@controller',
      }
    );
  };

  /**
   * Resources definition.
   */
  this.hotels = apiResourceDefault('hotels');
    this.roomtypes = apiResourceDefault('roomtypes');
      this.roomtypes_prices = apiResourceDefault('roomtypes_prices');
      this.roomtypes_rates = apiResourceDefault('roomtypes_rates');
  //   this.contacts = apiResourceDefault('contacts');
  //   this.actions = apiResourceDefault('actions');
  //   this.reservations = apiResourceDefault('reservations');
  //   this.comments = apiResourceDefault('comments');

  // this.pages = apiResourceDefault('pages');

  // this.users = apiResourceDefault('users');
  // this.groups = apiResourceDefault('groups');
  // this.permissions = apiResourceDefault('permissions');

  // this.labels = apiResourceDefault('labels');

  // this.files = apiResourceDefault('files');

  // this.cities = apiResourceDefault('cities');
  // this.states = apiResourceDefault('states');

  // this.config = apiResourceDefault('config');

  // this.mail_templates = apiResourceDefault('mail_templates');
});