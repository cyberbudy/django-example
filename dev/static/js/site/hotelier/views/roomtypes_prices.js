angular.module('hotelier.views.roomtypes_prices', [
  'hotelier.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('RoomTypes_Prices', [,
    '/hotel/{hotel}/roomtype/{roomtype}' + viewListSearch + '&year&month'])
    .stateList();
})


/**
 * Controllers
 * ======================================================================== */

.controller('RoomTypes_PricesListCtrl', function(
  $scope,
  $state,
  FormFactory,
  notifications,
  Api,
  $filter
  ) {

  $scope.monthNames = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
  ];

  var p = $scope.list.pagination,
      nextMonth = p.month == 12 ? 1 : (p.month + 1),
      prevMonth = p.month > 1 ? p.month - 1 : 12;

  p.next = {
    year: nextMonth > p.month ? p.year : p.year + 1,
    month: nextMonth,
  };
  p.prev = {
    year: prevMonth < p.month ? p.year : p.year - 1,
    month: prevMonth,
  };

  $scope.single = new Api.roomtypes_prices({
    date_from: null,
    date_to: null,
    rate: null,
    room_type: $scope.$stateParams.roomtype,
  });

  $scope.rates = angular.copy($scope.list.rates);
  $scope.rate = new Api.roomtypes_rates({
    price: null,
    room_type: $scope.$stateParams.roomtype,
  });

  $scope.saveRate = function(data) {
    if(angular.isString(data.price)) {
      var price = parseFloat(data.price);
      if(price > 0) {
        data.price = price;
      }
    }

    if(angular.isNumber(data.price)) {
      var rate = new Api.roomtypes_rates(data);
      var method = angular.noop;

      if(rate.id > 0) {
        method = rate.$update;
      } else {
        method = rate.$create;
      }
      method.apply(rate, [{}, function() {
          notifications.add({
              "type": "success",
              "content": "Данные успешно сохранены."
            });

          $state.transitionTo('roomtypes_prices', {}, {
            reload: true,
            inherit: true,
            notify: true
          });
        }]);
    } else {
      notifications.add({
        "type": "error",
        "content": "Цена должна быть цифрой."
      });
    }
  };

  $scope.pricePickerDate = $filter('date')(
    new Date($scope.list.pagination.year, $scope.list.pagination.month - 1, 1),
    'yyyy-MM-dd');

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('roomtypes_prices', {}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});