angular.module('hotelier.views', [
  'hotelier.views.conf',

  'hotelier.views.pagination',

  'hotelier.views.hotels',
    'hotelier.views.roomtypes',
      'hotelier.views.roomtypes_prices',
  // 'hotelier.views.labels',
  // 'hotelier.views.pages',
  // 'hotelier.views.cities',
  // 'hotelier.views.states',
  // 'hotelier.views.users',
  // 'hotelier.views.groups',
      // 'hotelier.views.roomtypes_rates',
  //   'hotelier.views.contacts',
  //   'hotelier.views.comments',
  // 'hotelier.views.reservations',
  // 'hotelier.views.config',
  // 'hotelier.views.mail_templates',
  //   'hotelier.views.rooms',
  //   'hotelier.views.actions',


  'services.formFactory',
  'services.crudRouteProvider',
])

/**
 * Root controller
 * ======================================================================== */
 
.controller('ViewRootCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  History
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;

  $scope.history = History;
})

/**
 * Sidebar controller
 * ======================================================================== */

.controller('ViewSidebarCtrl', function(
  $rootScope,
  $scope,
  $state,
  $stateParams
  ) {

  $scope.rt = $rootScope;
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;

  $scope.setCurrentUser = function(id) {
    $rootScope.current_user_id = id;
  }
});