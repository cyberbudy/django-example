(function() {
  var datepickerStart = $('#datepicker_search_start');
  var datepickerEnd = $('#datepicker_search_end');
  var dt = {end: false};

  var datepickerStartInitValue = datepickerStart.val() || new Date().setHours( 0, 0, 0, 0 );
  var datepickerEndInitValue = datepickerEnd.val() || new Date().setHours( 0, 0, 0, 0 );

  var datepickerStartPicker = datepickerStart.pickadate({
    formatSubmit: 'yyyy-mm-dd',
    format: 'dd.mm.yyyy',
    hiddenName: true,
    editable: false,
    firstDay: 1,
    min: true,
    onStart: function() {
      this.set({
        'select': (new Date(datepickerStartInitValue)).getTime(),
      });
    },
    onSet: function(context) {
      var picker = datepickerEnd.pickadate('picker');
      var date = new Date(this.get('select').pick);

      if(picker) {
        var dateEnd = new Date(date);

        date.setDate(date.getDate() + 1);
        dateEnd.setMonth(dateEnd.getMonth() + 1);

        if(picker.get('select').pick < date.getTime()) {
          picker.set('select', date.getTime());
        }

        if(picker.get('select').pick > dateEnd.getTime()) {
          picker.set('select', dateEnd.getTime());
        }

        picker.set({
          'min': date,
          'max': dateEnd
        });

        picker.render(true);
      }
      
      this.render(true);
    },
  });

  var datepickerEndPicker = datepickerEnd.pickadate({
    formatSubmit: 'yyyy-mm-dd',
    format: 'dd.mm.yyyy',
    hiddenName: true,
    editable: false,
    firstDay: 1,
    min: true,
    max: dt.end,
    onStart: function() {
      var picker = datepickerStart.pickadate('picker');
      var dateEnd = new Date(datepickerEndInitValue);

      if(picker) {
        var dateStart = new Date(picker.get('select').pick);

        if(dateStart.getTime() >= dateEnd.getTime()) {
          dateEnd = new Date(dateStart);

          dateEnd.setDate(dateEnd.getDate() + 1);
        }

        var dateMax = new Date(dateStart);

        dateMax.setMonth(dateMax.getMonth() + 1);

        this.set({
          'max': dateMax,
        });

        picker.render(true);
      }

      this.set({
        'select': (dateEnd).getTime(),
      });
    },
    onSet: function(context) {
      var picker = datepickerStart.pickadate('picker');
      var select = this.get('select');

      if(select != null) {
        var dateStart = new Date(select.pick);

        if(picker) {
          var cDateStart = new Date(picker.get('select').pick);

          dateStart.setMonth(dateStart.getMonth() - 1);

          if(cDateStart.getTime() < dateStart.getTime()) {
            picker.set({
              'min': dateStart,
              'select': dateStart,
            });
          }

          picker.render(true);
        }
      }
      
      this.render(true);
    },
  });
})();