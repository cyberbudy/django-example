angular.module('admin.views.roomtypes', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('RoomTypes', [, '/hotel/{hotel}'+ viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.roomtypes({
          hotel: $stateParams.hotel,
          labels: [],
          files: [],
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('RoomTypesListCtrl', function(
  $scope
  ) {
})

.controller('RoomTypesDetailCtrl', function(
  $scope,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('RoomTypesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('roomtypes.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});