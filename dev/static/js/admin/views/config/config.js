angular.module('admin.views.config', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Config', [])
    .stateList();
})


/**
 * Controllers
 * ======================================================================== */

.controller('ConfigListCtrl', function(
  $scope,
  $state,
  FormFactory,
  Api,
  $filter
  ) {

  $scope.single = new Api.config({
    config: angular.extend({
        slug_articles: '',
      }, angular.copy($scope.list.config)),
  });

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('config', {}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});