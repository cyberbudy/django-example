angular.module('admin.views.mail_templates', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Mail_Templates', [, viewListSearch])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams) {
        return new Api.mail_templates({});
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('Mail_TemplatesListCtrl', function(
  $scope
  ) {
})

.controller('Mail_TemplatesDetailCtrl', function(
  $scope,
  $state,
  $filter,
  Api,
  FormFactory
  ) {

  var filterUsers = function(newVal) {
    $scope.sender.usersFiltered = $filter('filter')(
      $scope.single.related_data_all.users, 
      function(item) {
        if(typeof newVal === 'undefined' || newVal === null) {
          return true;
        }

        return item.groups.indexOf(newVal) >= 0;
      });
  };

  $scope.sender = {
    filter: null,
    usersFiltered: [],
  };

  $scope.$watch('sender.filter', filterUsers);
  filterUsers($scope.sender.filter);

  $scope.send = new Api.mail_templates({
    id: $scope.single.id,
    users: [],
  });

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
  $scope.FormSend = new FormFactory($scope, 'senderForm', 'send', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('mail_templates.detail', {id: $scope.single.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
})

.controller('Mail_TemplatesCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('mail_templates.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});