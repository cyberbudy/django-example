angular.module('admin.views.reservations', [
  'admin.views.conf',
  'services.crudRouteProvider'
])

.config(function(
  crudRouteProvider,
  viewListSearch) {

  crudRouteProvider.routesFor('Reservations', [,
    viewListSearch + '&hotel&manager&status&type_status'])
    .stateList()
    .stateDetail()
    .stateCreate({
      single: function(Api, $stateParams, create) {
        return new Api.reservations({
          hotel: $stateParams.hotel,
          user: null,
          manager: null,
          internal: true,
          related_data_all: create.related_data_all,
        });
      }
    });
})


/**
 * Controllers
 * ======================================================================== */

.controller('ReservationsListCtrl', function(
  $scope
  ) {
})

.controller('ReservationsDetailCtrl', function(
  $scope,
  $rootScope,
  FormFactory
  ) {

  $scope.manage = function() {
    $scope.single.manager = $rootScope.current_user_id;
    $scope.single.$update({}, function() {
      $scope.Form.success = true;
    });
  };

  $scope.unManage = function() {
    $scope.single.manager = null;
    $scope.single.$update({}, function() {
      $scope.Form.success = true;
    });
  };

  $scope.Form = new FormFactory($scope, 'singleForm', 'single');
})

.controller('ReservationsCreateCtrl', function(
  $scope,
  $state,
  FormFactory
  ) {

  $scope.Form = new FormFactory($scope, 'singleForm', 'single', {
    success: function onSuccess(method, data, status) {
      $state.transitionTo('reservations.detail', {id: data.id}, {
          reload: true,
          inherit: true,
          notify: true
        });
    },
  });
});