/* ========================================================================
 * Notification service
 * ======================================================================== */

angular.module('services.notifications', [])

.factory('notifications', function() {
  var Notification = function(parent) {
    /**
     * Notification item class.
     */
    this.parent = parent;
  };

  Notification.prototype.remove = function() {
    this.parent.remove(this);
  };

  var Notifications = function() {
    /**
     * Notifications list class.
     */
    this.list = [];
  };

  Notifications.prototype.add = function(notification) {
    /**
     * Adding notification to the notifications list.
     *
     * @param {object} notification : Notification object shoud look like this:
     *    {
     *      "type": "error|success|info|[custom]",
     *      "title": "Some title of the notification message",
     *      "content": "The notification message itself.",
     *      "cause": {Caused if exists}
     *    }
     * @returns void
     */

    if(!angular.isObject(notification)) {
      throw new Error("Only object can be added to the notification service");
    }

    this.list.push(angular.extend(new Notification(this), notification));
  };

  Notifications.prototype.remove = function(notification) {
    var n = this.list, pos = 0;

    if(angular.isObject(notification)) {
      pos = n.indexOf(notification);
    } else if(angular.isNumber(notification)) {
      pos = notification;
    }

    if(pos >= 0 && pos < n.length) {
      n.splice(pos, 1)
    }
  };

  Notifications.prototype.clean = function() {
    this.list.length = 0;
  };

  return new Notifications;
})

.controller('NotificationsCtrl', function($scope, notifications) {
  $scope.notifications = notifications;
})

.directive('notificationsList', function() {
  var directiveObject = {
    restrict: 'A',
    controller: 'NotificationsCtrl',
    link: function() {}
  };

  return directiveObject;
});