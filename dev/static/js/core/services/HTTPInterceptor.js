angular.module('services.httpInterceptor', ['services.notifications'])

// register the interceptor as a service
$provide.factory('HTTPInterceptor', function(
  $q,
  $log,
  notifications) {

  return {
    'response': function(response) {
      return response || $q.when(response);
    },

    // optional method
   'responseError': function(rejection) {
      // do something on error
      if (canRecover(rejection)) {
        return responseOrNewPromise
      }
      return $q.reject(rejection);
    };
  }
});

.config(function($httpProvider) {
  $httpProvider.interceptors.push('HTTPInterceptor');
});
