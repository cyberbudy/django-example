angular.module('services.stateErrorHandler', ['services.exceptionHandler'])

.run(function($rootScope, $state) {
  $rootScope.$on(
    '$stateChangeError',
    function(event, toState, toParams, fromState, fromParams, error) {
      event.preventDefault();

      if('stack' in error) {
        throw error;
      }
      
      // throw new Error(error);
    });
})