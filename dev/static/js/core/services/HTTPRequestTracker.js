angular.module('services.httpRequestTracker', [])

.factory('httpRequestTracker', function($http){

  var HTTPRequestTracker = function() {};

  HTTPRequestTracker.prototype.hasPendingRequests = function() {
    return $http.pendingRequests.length > 0;
  };

  return new HTTPRequestTracker;
})

.controller('HTTPRequestTrackerCtrl', function($scope, httpRequestTracker) {
  $scope.httpRequestTracker = httpRequestTracker;
})

.directive('httpRequestTracker', function() {
  var directiveObject = {
    restrict: 'A',
    controller: 'HTTPRequestTrackerCtrl',
    link: function() {}
  };

  return directiveObject;
});