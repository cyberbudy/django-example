/**
 * Form datepicker module
 * ======================================================================== */

angular.module('form.datepicker', [])


.directive('formDatepicker', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    require: '?ngModel',

    link: function link($scope, $element, $attrs, ngModelCtrl) {
      if(!ngModelCtrl) return;
      var modelValue = $scope.$eval($attrs.ngModel);

      $element.pickadate({
        formatSubmit: 'yyyy-mm-dd',
        format: 'yyyy-mm-dd',
        hiddenName: false,
        editable: false,
        firstDay: 1,
        onStart: function() {
          this.set({
            'select': modelValue,
          });
        },
        // onSet: function(context) {
        //   var date = new Date(this.get('select').pick);
        //   console.log(this)

        //   // if(date)
        // }
      });

      var picker = $element.pickadate('picker');

      $scope.$watch($attrs.ngModel, function(value) {
        picker.set('select', value);
      });
    },
  };

  return directiveObject;
});