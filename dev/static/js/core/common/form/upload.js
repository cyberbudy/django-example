/**
 * Form upload field
 * ======================================================================== */

angular.module('form.upload', [
  'angularFileUpload'
])

.constant('pathFormUpload', '/api/files/')

.controller('FormUploadCtrl', function(
  $scope,
  $attrs,
  $fileUploader,
  $parse,
  $cookies,
  pathFormUpload
  ) {
  var uploader = $scope.formUploader = {};

  uploader.uploader = $fileUploader.create({
      scope: $scope,
      url: pathFormUpload,
      method: 'POST',
      headers: {
        'X-CSRFToken': $cookies['csrftoken'],
      },
      filters: [
        function(item /*{File|HTMLInputElement}*/) {
          var type = uploader.uploader.isHTML5 ? item.type : '/'
            + item.value.slice(item.value.lastIndexOf('.') + 1);
          type = '|' + type.toLowerCase().slice(type.lastIndexOf('/') + 1) + '|';
          return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
      ]
  });

  uploader.uploader.autoUpload = true;
  uploader.uploaded = [];

  uploader.model = $parse($attrs.formUpload);
  uploader.files = $parse($attrs.formUploadFiles);

  uploader.uploader.bind('success', function (event, xhr, item, response) {
    $scope.$apply(function() {
      var modelData = uploader.model($scope),
          filesData = uploader.files($scope);

      console.log('become');

      if(angular.isArray(modelData)) {
        modelData.push(response.id);
        filesData.push(response);
        console.log('array')
      } else {
        modelData = response.id;
        filesData = response;
        console.log('single')
      }

      uploader.model.assign($scope, modelData);
      uploader.files.assign($scope, filesData);
    });
  });
})

.directive('formUpload', function($parse) {
  var directiveObject = {
    restrict: 'A',
    controller: 'FormUploadCtrl',
    scope: true,
    link: function($scope, $element, $attrs) {}
  };

  return directiveObject;
});