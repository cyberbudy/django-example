/**
 * Form value module
 * ======================================================================== */

angular.module('form.value', [])

.directive('formValue', function () {
  var directiveObject = {
    restrict: 'EA',
    scope: true,
    require: '?ngModel',

    link: function link($scope, $element, $attrs, ngModelCtrl) {
      $scope.$watch($attrs.formValue, function(newVal) {
        ngModelCtrl.$setViewValue(newVal, angular.noop);
        ngModelCtrl.$render();
      });
    },
  };

  return directiveObject;
});