angular.module('form.datafill', [])

.directive('formDatafillNumbers', function() {
  var directiveObject = {
    restrict: 'A',
    controller: function($scope, $attrs) {
      var i, attr = $attrs.formDatafillNumbers,
          count = $attrs.formDatafillNumbersCount;

      var datafill = $scope[attr] = [];

      for(i = 0; i <= count; i++) {
        datafill.push({id: i, name: i});
      }
    },
    scope: true,
    link: function($scope, $element, $attrs) {},
  };

  return directiveObject;
});