/* ========================================================================
 * Form files list methods
 * ======================================================================== */

angular.module('form.files-list', [])

.controller('FormFilesListCtrl', function ($scope) {
  $scope.remove = function (index) {
    var s = $scope, fl = s.single.related_data.files;
    s.single.files.splice(s.single.files.indexOf(fl[index].id), 1);
    fl.splice(index, 1);
  };
})

.directive('formFilesList', function () {
  var directiveObject = {
      restrict: 'A',
      controller: 'FormFilesListCtrl',
      scope: true,
      link: function ($scope, $element, $attrs, controller) {}
    };
  return directiveObject;
});