/**
 * Utility functions.
 * ======================================================================== */

var utils = window.utils ? window.utils : (window.utils = {});

/**
 * Cpitalizing the string:
 * 'Alex TRansf ulerioo'.capitalize() -> 'Alex Transf Ulerioo'
 *
 * @param {boolean} lower: If set string tolowercase before transform.
 *                         Default true.
 * @returns {string} Capitalized string.
 */
String.prototype.capitalize = function(lower) {
  if(typeof lower === 'undefined') {
    lower = true;
  }

  return (lower ? this.toLowerCase() : this)
    .replace(/(?:^|\s)\S/g, function(a) {
      return a.toUpperCase();
    });
};


/**
 * Injecting params to string.
 *
 * @param {string} template template string
 * @param {string} params params string
 * @returns {function} Injector
 */
utils.injectStateParams = function(template, params) {
  var i;

  for(i in params) {
    if(!params.hasOwnProperty(i)) continue;

    template = template.replace(new RegExp('\{' + i + '\}', 'g'), params[i]);
  }

  return template;
};


/**
 * Getting current params from the route and replace placeholders in state
 * attributes.
 *
 * @param {string} str template string
 * @returns {function} Injector
 */
utils.injectStateParams = function(str) {
  return function($stateParams) {
    return utils.injectStateParams(str, $stateParams);
  }
};


/**
 * Resolves multiple named promises into named list.
 *
 * @param {$q} $q promise factory
 * @param {object} promises promises named list(object)
 * @returns {$deffer.promise} promise(named list on resolve)
 */
utils.resolveMultiple = function($q, promises) {
  var deferred = $q.defer();
  var i, v, keys = [], values = [];

  for(i in promises) {
    if(!promises.hasOwnProperty(i)) continue;

    keys.push(i);
    values.push(promises[i]);
  }

  $q.all(values).then(function(response) {
    var i, l = keys.length, r = {};

    for(i = 0; i < l; i++) {
      r[keys[i]] = response[i];
    }

    deferred.resolve(r); 
  }, function(reason) {
    deferred.reject(reason);
  });

  return deferred.promise;
};


/**
 * Returns child element of object by dot notation path.
 *
 * @param {object} from
 * @param {string} path
 * @returns {mixed} resolved element
 */
utils.getObjectDots = function(from, path) {
  var i, l, path = path.split('.');

  for (i = 0, l = path.length; i < l; i++) {
    if(typeof from != 'undefined') {
      from = from[path[i]];
    } else {
      throw new Error('There is no such element "'
        + path[i] + '" in: ' + path.join('.'));
    }
  }

  return from;
}