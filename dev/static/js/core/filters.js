angular.module('filters', [])

.filter('toJson', function() {
  return function(data, pretty) {
    return angular.toJson(data, pretty);
  };
})

.filter('average', function() {
  return function(value) {
    if(angular.isArray(value)) {
      var i, l = value.length, sum = 0;

      for(i = 0; i < l; i++) {
        sum += value[i] | 0;
      }

      return sum / l;
    } else {
      return false;
    }
  };
})

.filter('startsWith', function() {
  return function(value, startsWith) {
    if(angular.isString(value)) {
      return value.indexOf(startsWith) === 0;
    } else {
      return false;
    }
  };
})

.filter('isFilled', function() {
  return function(value) {
    if(typeof value != 'undefined' && value !== null) {
      if(angular.isString(value) || angular.isArray(value)) {
        return value.length > 0;
      } else if(angular.isNumber(value)) {
        return true;
      }
    } else {
      return false;
    }
  };
});